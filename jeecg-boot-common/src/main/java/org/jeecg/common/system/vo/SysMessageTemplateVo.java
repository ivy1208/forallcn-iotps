package org.jeecg.common.system.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description: SysMessageTemplateVo
 * @author: zhouwr
 * @create: 2021-03-16 20:44
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysMessageTemplateVo {

    private java.lang.String id;
    /**
     * 模板code
     */
    private java.lang.String templateCode;
    /**
     * 模板标题
     */
    private java.lang.String templateName;
    /**
     * 模板内容
     */
    private java.lang.String templateContent;
    /**
     * 模板测试json
     */
    private java.lang.String templateTestJson;
    /**
     * 模板类型
     */
    private java.lang.String templateType;
    /**
     * 接收者
     */
    private java.lang.String receiver;

}
