package org.jeecg.common.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.common.aspect.annotation.Dict;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-05-12 16:00
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysSmsConfigVo {
    private java.lang.String id;
    /**
     * 名称
     */
    private java.lang.String name;
    /**
     * 消息类型
     */
    private java.lang.Integer smsType;
    /**
     * 配置信息
     */
    private java.lang.String config;
    /**
     * 网络服务
     */
    private java.lang.String networkService;
    /**
     * 发送人
     */
    private java.lang.String sender;
    /**
     * 接收人
     */
    private java.lang.String receiver;
    /**
     * 创建人
     */
    private java.lang.String createBy;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date createTime;
    /**
     * 更新人
     */
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    private java.lang.String updateBy;
    /**
     * 更新日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date updateTime;
    /**
     * 所属部门
     */
    @Dict(dictTable = "sys_depart", dicText = "depart_name", dicCode = "id")
    private java.lang.String sysOrgCode;
    /**
     * 备注
     */
    private java.lang.String remark;
}
