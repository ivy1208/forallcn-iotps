package org.jeecg.common.system.base.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description: 通用字典枚举
 * @author: zhouwr
 * @create: 2021-01-03 20:54
 * @version：1.0
 **/
public interface IDictEnum <T extends Serializable> extends IEnum<T> {

    /**
     * 数据库中存储的值
     * @return
     */
    @JsonValue
    @Override
    T getValue();


    /**
     * 从数据库保存的字典ID
     * @return
     */
    String getText();

}
