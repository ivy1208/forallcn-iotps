package com.zhouwr.protocol;

import com.alipay.jarslink.api.Action;
import com.zhouwr.common.network.DeviceMessageCodec;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-02-25 10:50
 * @version：1.0
 **/
public class ProtocolAction implements Action<Class<? extends DeviceMessageCodec>, DeviceMessageCodec> {

    @Override
    public DeviceMessageCodec execute(Class clazz) {
        try {
            return (DeviceMessageCodec) clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getActionName() {
        return "null";
    }
}
