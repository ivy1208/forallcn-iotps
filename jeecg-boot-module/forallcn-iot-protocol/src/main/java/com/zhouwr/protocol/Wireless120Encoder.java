package com.zhouwr.protocol;

import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: forallcn-iotps
 * @description: 编码器
 * @author: zhouwr
 * @create: 2020-12-08 16:22
 * @version：1.0
 **/
@Slf4j
@Sharable
public class Wireless120Encoder extends MessageToByteEncoder<InstanceFunctionVo> {

    @Override
    protected void encode(ChannelHandlerContext ctx, InstanceFunctionVo msg, ByteBuf out) throws Exception {
        final Object value = msg.getInputParams().get(0).getValue();
        log.info("编码器发送 >>>> {} -> {} {}", ctx.channel().remoteAddress(), ctx.channel().localAddress(), value);
        out.writeBytes((byte[]) value);
        out.writeByte((byte) 0x7e);
        ctx.flush();
    }
}
