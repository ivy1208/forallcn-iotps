package com.zhouwr.simulator;

import com.zhouwr.common.utils.HexConvertUtil;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @program: forallcn-iotps
 * @description: 模拟器
 * @author: zhouwr
 * @create: 2021-03-08 21:53
 * @version：1.0
 **/
public class SimulatorApplication {


    public static void main(String[] args) {

        String funcCode;
        Socket socket = null;
        int localPort = 23;
        int remotePort = 8888;
        String ip = "127.0.0.1";

        new NettyClient(ip, 8888, 26).connect();

        //创建一个流套接字并将其连接到指定主机上的指定端口号
        try {
            socket = new Socket();
            socket.bind(new InetSocketAddress(localPort));
            socket.connect(new InetSocketAddress(ip, remotePort));
        } catch (IOException e) {
            e.printStackTrace();
        }

        HeartSimulator heartSimulator = new HeartSimulator(socket);
        ResponseSimulator responsesimulator = new ResponseSimulator(socket);

        Thread thread = new Thread(heartSimulator);
        Thread thread1 = new Thread(responsesimulator);

        thread.start();
        thread1.start();
    }
}

class HeartSimulator implements Runnable {

    private Socket socket;
    public HeartSimulator(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            System.out.println("心跳线程。。。");
            while(true) {
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.writeBytes("heart");
                out.writeByte(0x7e);
                Thread.sleep(10000);

                String msg = "" +
                        "01 03 f4 00 c3 00 c1 00 c2 00 c0 00 bf 00 c2 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 00 c3 00 00 66 a3";

                // 0xFF 0x01 OX00
                byte[] bytes = HexConvertUtil.hexStringToBytes(msg);
                out.write(bytes);
                out.writeByte(0x7e);
                Thread.sleep(10000);
            }
        } catch (Exception e) {
            System.out.println("客户端异常:" + e.getMessage());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    socket = null;
                    System.out.println("客户端 finally 异常:" + e.getMessage());
                }
            }
        }
    }

}

class ResponseSimulator implements Runnable {

    private Socket socket;
    final byte[] output = {1, 3, 0, 0, 0, 122, (byte) 196, 41};

    public ResponseSimulator(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            System.out.println("响应数据。。。");

            while (true) {
                //读取服务器端数据
                DataInputStream input = new DataInputStream(socket.getInputStream());
                byte[] ret = input.readAllBytes();
                System.out.println("服务器端返回过来的是: " + ret);

                //向服务器端发送数据
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                out.write(output);
            }

        } catch (Exception e) {
            System.out.println("客户端异常:" + e.getMessage());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    socket = null;
                    System.out.println("客户端 finally 异常:" + e.getMessage());
                }
            }
        }
    }

}
