package com.zhouwr.common.device.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.DataRwAuthor;
import com.zhouwr.common.metadata.DataType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @program: forallcn-iotps
 * @description: 实例数据
 * @author: zhouwr
 * @create: 2020-11-23 23:17
 * @version：1.0
 **/
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InstanceDataStructure {

    private String id;
    private String instanceId;
    private String instanceName;
    /**
     * 名称
     */
    private String name;
    /**
     * 数据变量名
     */
    private String code;
    /**
     * 数据格式
     */
    private String type;
    /**
     * 数据读写权限
     */
    private DataRwAuthor rwAuthor;
    /**
     * 最新数据值
     */
    private ReportDataVo latestData;
    /**
     * 历史数据值
     */
    private List<ReportDataVo> historyDatas;
    /**
     * 数据格式结构
     */
    private DataType metadata;
    @JsonIgnore
    private String valueType;
    private Object value;

    public InstanceDataStructure(DeviceDataVo data) {
        this.id = data.getId();
        this.name = data.getName();
        this.code = data.getCode();
        this.type = data.getType();
        this.rwAuthor = data.getRwAuthor();
        this.metadata = data.getMetadata();

    }

    public InstanceDataStructure(DeviceDataVo data, ReportDataVo latestData) {
        this(data);
        this.latestData = latestData;
//        if (this.latestData.getValue() == null) {
//            this.latestData.setValue(this.metadata.getDefaultValue());
//        }
    }

    public InstanceDataStructure(DeviceDataVo data, List<ReportDataVo> historyDataReports) {
        this(data);
        this.historyDatas = historyDataReports;
//        this.historyDatas = historyDataReports.stream().map(reportDataVo -> {
//            if (reportDataVo.getValue() == null)
//                reportDataVo.setValue(this.metadata.getDefaultValue());
//            return reportDataVo;
//        }).collect(Collectors.toList());
    }

    public InstanceDataStructure(String instanceId, DeviceDataVo data, List<ReportDataVo> historyDataReports) {
        this(data, historyDataReports);
        this.instanceId = instanceId;
    }


    public InstanceDataStructure(String instanceId, DeviceDataVo data, ReportDataVo latestData) {
        this(data, latestData);
        this.instanceId = instanceId;
    }

    public InstanceDataStructure(String instanceId, String instanceName, DeviceDataVo data, ReportDataVo latestData) {
        this(instanceId, data, latestData);
        this.instanceName = instanceName;
    }

    public InstanceDataStructure(String instanceId, String instanceName, DeviceDataVo data, List<ReportDataVo> historyDataReports) {
        this(instanceId, data, historyDataReports);
        this.instanceName = instanceName;
    }
}
