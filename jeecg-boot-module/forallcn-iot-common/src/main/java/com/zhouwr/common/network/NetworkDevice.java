package com.zhouwr.common.network;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zhouwr.common.enums.DeviceInstanceState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
public class NetworkDevice {
    private String id;
    /**
     * 上级实例
     */
    private String parentBy;
    /**
     * 实例标识
     */
    private String code;
    /**
     * 实例名称
     */
    private String name;
    /**
     * 设备模型
     */
    private String modelBy;
    /**
     * 场景
     */
    private String sceneBy;
    /**
     * 场景方案
     */
    private String sceneSchemeBy;
    /**
     * 所属机构
     */
    private String sysOrgCode;
    /**
     * 设备实例状态
     */
    private DeviceInstanceState status;

    /**
     * 状态更新日期
     */
    private Date statusUpdateTime;

    @JsonIgnore
    private String modelAttribute;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建日期
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新日期
     */
    private Date updateTime;
    /**
     * 说明
     */
    private String description;
}
