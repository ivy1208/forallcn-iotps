package com.zhouwr.common.device.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: forallcn-iotps
 * @description: 设备实例扩展参数
 * @author: zhouwr
 * @create: 2020-11-21 17:18
 * @version：1.0
 **/
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InstanceExtentParams {
    String funcId;
    String funcName;
    String funcCode;
    String funcType;

    /**
     * 设备功能输入参数
     */
    List<InstanceFunctionInputParam> inputParams = new ArrayList<>();

    /**
     * 构造函数
     *
     * @param function            设备功能实体
     * @param functionInputParams 设备功能输入参数集合
     */
    public InstanceExtentParams(DeviceFunctionVo function, List<InstanceFunctionInputParam> functionInputParams) {
        this.funcId = function.getId();
        this.funcName = function.getName();
        this.funcCode = function.getCode();
        this.funcType = function.getType();
        this.inputParams = functionInputParams;
    }
}
