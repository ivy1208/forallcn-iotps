package com.zhouwr.common.device.vo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.zhouwr.common.enums.FunctionParamInputMode;
import com.zhouwr.common.metadata.ArrayData;
import com.zhouwr.common.metadata.Metadata;
import com.zhouwr.common.metadata.StringData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description: 设备功能输入参数
 * @author: zhouwr
 * @create: 2020-11-21 17:18
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstanceFunctionInputParam implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据节点信息
     */
    private String id;
    private String name;
    private String code;
    private String type;
    private Object value;
    private FunctionParamInputMode inputMode;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = ArrayData.class, name = "array"),
            @JsonSubTypes.Type(value = StringData.class, name = "string")
    })
    private Metadata metadata;

    /**
     * 构造函数，无有参数值
     *
     * @param data
     * @param inputMode
     */
    public InstanceFunctionInputParam(DeviceDataVo data, FunctionParamInputMode inputMode) {
        this.id = data.getId();
        this.name = data.getName();
        this.code = data.getCode();
        this.type = data.getType();
        this.inputMode = inputMode;
        this.metadata = data.getMetadata();
    }

    /**
     * 构造函数，有参数值
     *
     * @param data      数据节点实体
     * @param value     数据节点数值
     * @param inputMode 数据输入方式
     */
    public InstanceFunctionInputParam(DeviceDataVo data, Object value, FunctionParamInputMode inputMode) {
        this(data, inputMode);
        this.value = value;
    }
}
