package com.zhouwr.common.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-10 20:49
 * @version：1.0
 **/
public interface IotEnum<V> {

//    public abstract static V of(String code);

    static <T extends IotEnum> List<T> findList(Class<T> type, Predicate<T> predicate) {
        return type.isEnum() ? (List) Arrays.stream(type.getEnumConstants()).filter(predicate).collect(Collectors.toList()) : Collections.emptyList();
    }
}
