package com.zhouwr.common.network;

import com.zhouwr.common.enums.NetworkType;

/**
 * 网络组件，所有网络相关实例根接口
 *
 * @author zhouhao
 * @version 1.0
 * @since 1.0
 */
public interface Network {

    /**
     * ID唯一标识
     *
     * @return ID
     */
    String getId();

    /**
     * 网络类型
     * @return
     */
    NetworkType getType();

    /**
     * 关闭网络组件
     */
    void shutdown();

    /**
     * 是否存活
     * @return
     */
    boolean isAlive();

    /**
     * 是否重新加载
     * @return
     */
    boolean isAutoReload();
}
