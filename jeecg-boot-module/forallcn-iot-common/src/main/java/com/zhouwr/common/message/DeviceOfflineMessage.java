package com.zhouwr.common.message;

/**
 * 设备离线消息
 * @author zhouwenrong
 * @date 2022/03/19 23:33
 */
public class DeviceOfflineMessage extends DeviceMessage{
    public MessageType getMessageType() {
        return MessageType.OFFLINE;
    }
}
