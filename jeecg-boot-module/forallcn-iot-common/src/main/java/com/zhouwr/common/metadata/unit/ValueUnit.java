package com.zhouwr.common.metadata.unit;

import com.zhouwr.common.metadata.FormatSupport;
import com.zhouwr.common.metadata.Metadata;

import java.io.Serializable;
import java.util.Map;

/**
 * 值单位
 *
 * @author bsetfeng
 * @author zhouhao
 * @version 1.0
 **/
public interface ValueUnit extends Metadata, FormatSupport, Serializable {

    String getSymbol();
//    String getName();
//    String getType();
//    String getDescription();

    @Override
    default Map<String, Object> getExpands() {
        return null;
    }
}
