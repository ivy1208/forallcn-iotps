package com.zhouwr.common.metadata;

import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.unit.UnifyUnit;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LongData extends NumberData<Long> {

    private Long value = 0L;
    private ModelDataType type = ModelDataType.LONG;


    @Override
    public Long convert(Object value) {
        return super.convertNumber(value,Number::longValue);
    }

    public static void main(String[] args) {
        LongData longData = new LongData();
        longData.value = 89L;
        longData.setMax(100);
        longData.setMin(10);
        longData.setUnit(UnifyUnit.meter);
        System.out.println(longData);
    }
}
