package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/**
 * @author zhouwenrong
 */

@Getter
@AllArgsConstructor
public enum ReportMode implements IDictEnum<Integer> {
    /**
     *
     */
    REPORT("上报", 1),
    MANUAL("手动", 2);

    private final String text;
    @EnumValue
    private final Integer value;

    @JsonCreator
    public static ReportMode of(Integer value) {
        for (ReportMode reportMode : ReportMode.values()) {
            if (reportMode.value.equals(value)) {
                return reportMode;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

}
