package com.zhouwr.common.utils;

import com.alibaba.fastjson.JSON;
import com.zhouwr.common.metadata.Metadata;

/**
 * @program: forallcn-iotps
 * @description: 数据类型工具类
 * @author: zhouwr
 * @create: 2020-11-19 15:53
 * @version：1.0
 **/
public class DataTypeUtil {
    public static <T extends Metadata> T getDataStructure(String data, Class<T> clazz) {
        if (clazz == null) {
            return null;
        }
        return JSON.toJavaObject(JSON.parseObject(data), clazz);
    }
}
