package com.zhouwr.common.metadata;

import com.zhouwr.common.metadata.unit.ValueUnit;

public interface UnitSupported {
    ValueUnit getUnit();

    void setUnit(ValueUnit unit);
}
