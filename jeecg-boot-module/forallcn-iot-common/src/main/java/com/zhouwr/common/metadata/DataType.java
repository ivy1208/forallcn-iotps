package com.zhouwr.common.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.zhouwr.common.enums.ModelDataType;

import java.util.Map;

/**
 * 物模型数据类型
 *
 * @author zhouhao
 * @since 1.0.0
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes(value = {
        @JsonSubTypes.Type(value = ArrayData.class, name = "array"),
        @JsonSubTypes.Type(value = BoolData.class, name = "bool"),
        @JsonSubTypes.Type(value = ByteData.class, name = "byte"),
        @JsonSubTypes.Type(value = DateTimeData.class, name = "datetime"),
        @JsonSubTypes.Type(value = DoubleData.class, name = "double"),
        @JsonSubTypes.Type(value = EnumData.class, name = "enum"),
        @JsonSubTypes.Type(value = FloatData.class, name = "float"),
        @JsonSubTypes.Type(value = IntData.class, name = "int"),
        @JsonSubTypes.Type(value = IpData.class, name = "ip"),
        @JsonSubTypes.Type(value = IpPortData.class, name = "ipPort"),
        @JsonSubTypes.Type(value = LongData.class, name = "long"),
        @JsonSubTypes.Type(value = StringData.class, name = "string"),
        @JsonSubTypes.Type(value = Vector3Data.class, name = "vector3"),
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public interface DataType extends Metadata{

    /**
     * 验证是否合法
     *
     * @param value 值
     * @return ValidateResult
     */
    ValidateResult validate(Object value);

    /**
     * @return 类型标识
     */
    ModelDataType getType();
    void setType(ModelDataType type);

    /**
     * @return 拓展属性
     */
    @Override
    default Map<String, Object> getExpands() {
        return null;
    }

    @Override
    default Object getExpand(String name) {
        return Metadata.super.getExpand(name);
    }

    Object getValue();
}
