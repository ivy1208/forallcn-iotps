package com.zhouwr.common.exception;

/**
 * @program: forallcn-iotps
 * @description: 设备未激活异常
 * @author: zhouwr
 * @create: 2020-12-09 22:53
 * @version：1.0
 **/
public class IotDeviceNotActiveException extends RuntimeException{

    public IotDeviceNotActiveException(String msg){
        super(msg);
    }

    public IotDeviceNotActiveException(String instanceId, String instanceName){
        super("设备：" + instanceName + "[" + instanceId + "]" + "尚未激活！");
    }
}
