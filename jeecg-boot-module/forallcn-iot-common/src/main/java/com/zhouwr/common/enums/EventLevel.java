package com.zhouwr.common.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public enum EventLevel implements IotEnum<EventLevel> {

    /**
     *
     */
    ERROR("ERROR", "故障", 2),
    WARN("WARN", "警告", 1),
    INFO("INFO", "信息", 0),
    DEBUG("DEBUG", "调试", 3);

    private String code;
    private String name;
    private Integer value;

    @JsonCreator
    public EventLevel of(String code) {
        for (EventLevel level : values()) {
            if (level.code.equals(code)) {
                return level;
            }
        }
        throw new UnsupportedOperationException("不支持的事件等级:" + code);
    }

    public static EventLevel of(Integer value) {
        for (EventLevel level : values()) {
            if (Objects.equals(level.value, value)) {
                return level;
            }
        }
        throw new UnsupportedOperationException("不支持的事件等级:" + value);
    }

    public static List<EventLevel> toList(){
        return Arrays.stream(EventLevel.class.getEnumConstants()).collect(Collectors.toList());
    }

    public static List<Map<String, Object>> toJSONArray() {
        return Arrays.stream(EventLevel.class.getEnumConstants()).map(eventLevel -> {
            Map<String, Object> map = new HashMap<>();
            map.put("value", eventLevel.getValue());
            map.put("code", eventLevel.getCode());
            map.put("name", eventLevel.getName());
            return map;
        }).collect(Collectors.toList());
    }

}
