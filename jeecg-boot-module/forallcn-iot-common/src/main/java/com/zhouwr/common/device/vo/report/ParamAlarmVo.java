package com.zhouwr.common.device.vo.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.AlarmLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParamAlarmVo {
    private String id;
    /**
     * 警报配置 ID
     */
    private java.lang.String alarmConfigId;
    /**
     * 报警配置名称
     */
    private String alarmConfigName;

    private String ruleExpre;

    private String ruleType;

    private AlarmLevel alarmLevel;

    private java.lang.Boolean isNeedDeal;

    private java.lang.String dealContent;

}
