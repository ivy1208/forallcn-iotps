package com.zhouwr.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * enum工具
 *
 * @author zhouwenrong
 * @date 2022/01/07
 */
@Slf4j
public class EnumUtil {
    /**
     * 获取枚举属性
     *
     * @param sourceValue 源值
     * @param enumName    枚举名字
     * @param targetKey   目标关键
     * @param targetValue 目标价值
     * @return {@link Object}
     */
    public static Object getEnumProperty(String sourceValue, String enumName, String targetKey, String targetValue) {
        // 1.反射获取枚举类
        Class<IDictEnum<?>> enumClazz = null;
        try {
            enumClazz = (Class<IDictEnum<?>>) Class.forName(enumName);
            // 2.获取所有枚举实例
            IDictEnum[] enumConstants = enumClazz.getEnumConstants();
            //根据方法名获取方法
            Method getValue = enumClazz.getMethod("get" + targetKey.substring(0, 1).toUpperCase() + targetKey.substring(1));
            Method getText = enumClazz.getMethod("get" + targetValue.substring(0, 1).toUpperCase() + targetValue.substring(1));
            for (IDictEnum<?> e : enumConstants) {
                //执行枚举方法获得枚举实例对应的值
                String valueInvoke = getValue.invoke(e).toString();
                Object textInvoke = getText.invoke(e);
                log.info("{} == {} -->  {}", sourceValue, valueInvoke, textInvoke);
                if (sourceValue.equals(valueInvoke)) {
                    return textInvoke;
                }
            }
        } catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return sourceValue;
    }
}
