package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/**
 * @description: 设备实例状态
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum DeviceInstanceState implements IDictEnum<String> {
    /**
     *
     */
    NOT_ACTIVE("未激活", "notActive"),
    OFFLINE("离线", "offline"),
    ONLINE("在线", "online");

    private final String name;
    @EnumValue
    private final String code;

    @JsonCreator
    public static DeviceInstanceState of(String code) {
        for (DeviceInstanceState value : DeviceInstanceState.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + code);
    }

    @JsonValue
    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
