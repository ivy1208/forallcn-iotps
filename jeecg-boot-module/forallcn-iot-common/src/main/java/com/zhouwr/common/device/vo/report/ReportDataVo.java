package com.zhouwr.common.device.vo.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.ReportMode;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportDataVo {
    private java.lang.String id;
    /**
     * 实例设备
     */
    private java.lang.String instanceId;

    private java.lang.String instanceName;
    /**
     * 上报数据
     */
    private java.lang.String data;
    /**
     * 上报日期
     */
    private java.util.Date reportTime;
    /**
     * 录入方式
     */
    private ReportMode reportMode;
    /**
     * 数据值
     */
    private List<ReportParamVo> reportParams;

}
