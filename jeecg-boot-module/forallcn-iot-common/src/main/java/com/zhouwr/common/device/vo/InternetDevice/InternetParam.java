package com.zhouwr.common.device.vo.InternetDevice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 函数参数
 *
 * @author zhouwenrong
 * @date 2022/02/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InternetParam {
    /**
     * 参数id
     */
    private String paramId;
    /**
     * 参数名称
     */
    private String paramName;
    /**
     * 参数值
     */
    private String paramValue;
}
