package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/***
 * 数据编解码类型
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum DataCodecType implements IDictEnum<String> {
    /**
     *
     */
    HTTP("jar", "jar包"),
    TCP("class", "class文件"),
    MQTT("javascript", "js脚本");

    @EnumValue
    private final String value;
    private final String text;

    @JsonCreator
    public static DataCodecType of(String value) {
        for (DataCodecType codecType : DataCodecType.values()) {
            if (codecType.value.equals(value)) {
                return codecType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(DataCodecType.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

}
