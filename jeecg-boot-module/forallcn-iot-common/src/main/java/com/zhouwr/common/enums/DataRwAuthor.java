package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum DataRwAuthor implements IDictEnum<String> {
    /**
     *
     */
    READ("只读", "r"),
    WRITE("只写", "w"),
    READ_WRITE("可读写", "rw");

    private final String name;
    @EnumValue
    private final String code;

    @JsonCreator
    public static DataRwAuthor of(String value) {
        for (DataRwAuthor rwAuthor : DataRwAuthor.values()) {
            if (rwAuthor.code.equals(value)) {
                return rwAuthor;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(DataRwAuthor.values()).map(rwAuthor -> {
            JSONObject json = new JSONObject();
            json.put("value", rwAuthor.getValue());
            json.put("text", rwAuthor.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
