package com.zhouwr.common.network;

import com.zhouwr.common.enums.ProtocolType;
import com.zhouwr.common.message.Message;

import javax.annotation.Nonnull;

/**
 * 消息编解码器
 * 用于将平台统一的消息(Message)与设备端能处理的消息(EncodedMessage)进行相互转换. 设备网关从网络组件中接收到报文后,会调用对应协议包的消息编解码器进行处理.
 *
 * @author zhouwenrong
 */
public interface DeviceMessageCodec<T extends CommonConnectContext> {
    /**
     * 此编解码器支持的网络协议,如: ProtocolType.MQTT
     *
     * @return
     */
    ProtocolType getProtocolType();

    @Nonnull
    Message decode(T connectContext, @Nonnull Object o);

    @Nonnull
    Message encode(T connectContext, @Nonnull Object o);
}
