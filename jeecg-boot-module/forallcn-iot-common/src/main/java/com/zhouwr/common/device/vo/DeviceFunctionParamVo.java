package com.zhouwr.common.device.vo;

import com.zhouwr.common.enums.FunctionParamDirection;
import com.zhouwr.common.enums.FunctionParamInputMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: 功能参数VO
 * @Author: zhouwr
 * @Date: 2020-12-05
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceFunctionParamVo {
    private String id;
    private String functionId;
    private String dataId;
    private String dataType;
    private FunctionParamDirection direction;
    private FunctionParamInputMode inputMode;

    private Integer sort;
}
