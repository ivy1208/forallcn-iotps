package com.zhouwr.common.network;

import com.zhouwr.common.enums.ProtocolType;

import javax.annotation.Nonnull;

/**
 * 协议支持提供者
 * @author zhouwenrong
 * @data 2022/03/09 21:00
 * @version 1.0
 */
public interface IProtocolSupportProvider {

    /**
     * @return 协议ID
     */
    @Nonnull
    String getId();

    /**
     * @return 协议名称
     */
    String getName();

    /**
     * @return 说明
     */
    String getDescription();

    /**
     * @return 获取支持的协议类型
     */
    ProtocolType getProtocolType();

    /**
     * 获取设备消息编码解码器
     * <ul>
     * <li>用于将平台统一的消息对象转码为设备的消息</li>
     * <li>用于将设备发送的消息转吗为平台统一的消息对象</li>
     * </ul>
     *
     * @return 消息编解码器
     */
    @Nonnull
    DeviceMessageCodec getMessageCodec(ProtocolType protocolType);

}
