package com.zhouwr.common.metadata.unit;

import lombok.AllArgsConstructor;

/**
 * 符号价值单位
 *
 * @author zhouwenrong
 * @date 2022/02/23
 */
@AllArgsConstructor(staticName = "of")
public class SymbolValueUnit implements ValueUnit {

    /**
     * 象征
     */
    private final String symbol;

    /**
     * 得到象征
     *
     * @return {@link String}
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * 格式
     *
     * @param value 价值
     * @return {@link Object}
     */
    @Override
    public Object format(Object value) {
        if (value == null) {
            return null;
        }
        return value + "" + symbol;
    }

    /**
     * 得到描述
     *
     * @return {@link String}
     */
    @Override
    public String getDescription() {
        return symbol;
    }
}
