package com.zhouwr.common.device.vo;

import com.zhouwr.common.enums.DataRwAuthor;
import com.zhouwr.common.metadata.DataType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-05 23:19
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDataVo {
    private String id;
    private String name;
    private String code;
    private String type;
    private DataRwAuthor rwAuthor;
    private DataType metadata;
}
