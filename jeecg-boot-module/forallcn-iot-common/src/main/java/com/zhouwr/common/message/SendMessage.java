package com.zhouwr.common.message;

import com.zhouwr.common.device.vo.function.FunctionParamVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @program: jeecg-boot-module-iot
 * @description: 网络发送数据
 * @author: zhouwr
 * @create: 2020-05-26 16:34
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SendMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 消息名称
     */
    private String name;
    private String code;

    /**
     * 设备模型
     */
    private String modelId;

    /**
     * 设备实例
     */
    private String instanceId;

    /**
     * 发送时间
     */
    private long datetime = System.currentTimeMillis();

    /**
     * 设备数据节点
     */
    private List<FunctionParamVo> inputParams;

    private FunctionParamVo outputParam;

    public SendMessage(String name, String code, String modelId, String instanceId, List<FunctionParamVo> inputParams, FunctionParamVo outputParam) {
        this.name = name;
        this.code = code;
        this.modelId = modelId;
        this.instanceId = instanceId;
        this.inputParams = inputParams;
        this.outputParam = outputParam;
    }
}
