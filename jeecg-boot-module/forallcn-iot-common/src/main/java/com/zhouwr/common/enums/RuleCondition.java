package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@Getter
@AllArgsConstructor
public enum RuleCondition implements IDictEnum<String> {
    /**
     *
     */
    GT(">", "大于", (tempVal, ruleValue) -> {
        return Double.parseDouble(tempVal.toString()) > Double.parseDouble(ruleValue.toString());
    }),
    GTE(">=", "大于等于", (tempVal, ruleValue) -> {
        return Double.parseDouble(tempVal.toString()) >= Double.parseDouble(ruleValue.toString());
    }),
    LT("<", "小于", (tempVal, ruleValue) -> {
        return Double.parseDouble(tempVal.toString()) < Double.parseDouble(ruleValue.toString());
    }),
    LTE("<=", "小于等于", (tempVal, ruleValue) -> {
        return Double.parseDouble(tempVal.toString()) <= Double.parseDouble(ruleValue.toString());
    }),
    EQ("==", "等于", (tempVal, ruleValue) -> {
        return tempVal.toString().equals(ruleValue.toString());
    }),
    NE("!=", "不等于", (tempVal, ruleValue) -> {
        return !tempVal.toString().equals(ruleValue.toString());
    }),
    LIKE("like", "包含", (tempVal, ruleValue) -> {
        return tempVal.toString().contains(ruleValue.toString());
    }),
    IN("in", "包含于", (tempVal, ruleValue) -> {
        return ruleValue.toString().contains(tempVal.toString());
    })

    ;

    @EnumValue
    private final String value;
    private final String text;
    private final BiFunction<Object, Object, Boolean> invoke;

    @JsonCreator
    public static RuleCondition of(String value) {
        for (RuleCondition condition : RuleCondition.values()) {
            if (condition.value.equals(value)) {
                return condition;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static void main(String[] args) {
        System.out.println(RuleCondition.LIKE.getInvoke().apply(99,"999"));
    }

    public static List<Map<String, Object>> toJSONArray() {
        return Arrays.stream(RuleCondition.class.getEnumConstants()).map(rule -> {
            Map<String, Object> map = new HashMap<>();
            map.put("value", rule.getValue());
            map.put("text", rule.getText());
            return map;
        }).collect(Collectors.toList());
    }
}
