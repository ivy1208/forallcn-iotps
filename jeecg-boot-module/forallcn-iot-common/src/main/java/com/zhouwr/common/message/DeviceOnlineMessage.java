package com.zhouwr.common.message;

/**
 * 设备上线消息
 * @author zhouwenrong
 * @date 2022/03/19 23:33
 */
public class DeviceOnlineMessage extends DeviceMessage{
    public MessageType getMessageType() {
        return MessageType.ONLINE;
    }
}
