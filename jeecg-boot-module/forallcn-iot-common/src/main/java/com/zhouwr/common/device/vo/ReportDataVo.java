package com.zhouwr.common.device.vo;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.ReportMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * @description: 上报数据
 * @author: zhouwr
 * @create: 2020-11-24 10:06
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReportDataVo {

    private JSONObject dataObj = new JSONObject();
    @JsonIgnore
    private String data;
    private Date reportTime;
    private ReportMode reportMode;
    private Object value;

    public ReportDataVo(String data, ReportMode reportMode, Date reportTime) {
        this(data);
        this.reportMode = reportMode;
        this.reportTime = reportTime;
    }

    public ReportDataVo(String data) {
        this();
        if (StringUtils.isNotBlank(data)) {
            this.dataObj.putAll(JSONObject.parseObject(data));
        }
    }

    public JSONObject getDataObj() {
        if (StringUtils.isNotBlank(data)) {
            try {
                this.dataObj.putAll(JSONObject.parseObject(data));
            } catch (JSONException e){
                throw new RuntimeException(e.getMessage());
            }
        }
        return dataObj;
    }
}
