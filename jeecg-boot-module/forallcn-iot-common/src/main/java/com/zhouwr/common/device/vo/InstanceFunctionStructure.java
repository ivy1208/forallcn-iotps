package com.zhouwr.common.device.vo;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: forallcn-iotps
 * @description: 实例功能执行结构体
 * @author: zhouwr
 * @create: 2020-11-21 17:18
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InstanceFunctionStructure {
    /**
     * 设备模型id
     */
    String deviceModelId;
    /**
     * 设备实例id
     */
    String deviceInstanceId;

    /**
     * 1、功能函数体
     */
    String id;
    String name;
    String code;
    String type;
    /**
     * 是否同步执行
     */
    Boolean isSync = false;

    /**
     * 2、输入参数
     */
    List<InstanceFunctionInputParam> inputParams;

    /**
     * 3、输出参数
     */
    InstanceFunctionInputParam outputParam;

    /**
     * 功能执行配置
     */
    InstanceFunctionExecConfig execConfig;



    /**
     * 构造函数
     *
     * @param function            设备功能实体
     * @param functionInputParams 设备功能输入参数集合
     */
    public InstanceFunctionStructure(String deviceInstanceId, DeviceFunctionVo function, List<InstanceFunctionInputParam> functionInputParams) {
        this.deviceModelId = function.getDeviceModelBy();
        this.deviceInstanceId = deviceInstanceId;
        this.id = function.getId();
        this.name = function.getName();
        this.code = function.getCode();
        this.type = function.getType();
        this.inputParams = functionInputParams;
        this.isSync = function.getIsSync();
    }

    /**
     * 构造函数
     *
     * @param function     设备功能实体
     * @param inputParams  功能输入参数集合
     * @param funcExecConf 功能执行配置信息
     */
    public InstanceFunctionStructure(
            String deviceInstanceId,
            DeviceFunctionVo function,
            List<InstanceFunctionInputParam> inputParams,
            String funcExecConf
    ) {
        this(deviceInstanceId, function, inputParams);
        final InstanceFunctionExecConfig execConfig = JSON.parseObject(funcExecConf, InstanceFunctionExecConfig.class);
        if (execConfig == null) {
            this.execConfig = new InstanceFunctionExecConfig("* * * * * ? *", "task", false);
        } else {
            this.execConfig = execConfig;
        }
    }

    /**
     * 构造函数
     *
     * @param function     设备功能实体
     * @param inputParams  功能输入参数集合
     * @param funcExecConf 功能执行配置信息
     * @param outputParam  功能输出参数
     */
    public InstanceFunctionStructure(
            String deviceInstanceId,
            DeviceFunctionVo function,
            List<InstanceFunctionInputParam> inputParams,
            String funcExecConf,
            InstanceFunctionInputParam outputParam
    ) {
        this(deviceInstanceId, function, inputParams, funcExecConf);
        this.outputParam = outputParam;
    }

}
