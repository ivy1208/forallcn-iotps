package com.zhouwr.common.utils;

import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.ArrayData;
import com.zhouwr.common.metadata.Metadata;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-06 20:17
 * @version：1.0
 **/
@Slf4j
public class MessageUtils {

    public static Object buildSendMessage(ModelDataType dataType, Metadata metadata, Object value) {
        final String type = dataType.getCode();
        log.info("数据类型: {}", type);
        switch (type) {
            case "array":
                final ArrayData array = (ArrayData) metadata;
                final String split = array.getArraySplit();
                final String arrayType = array.getArrayType().getCode();
                log.info("数组项类型: {}", arrayType);
                return HexConvertUtil.hexStringToBytes(value.toString());
//                return Arrays.stream(inputParam.getValue().toString().split(split))
//                        .filter(Objects::nonNull)
//                        .peek(System.out::println)
//                        .map(s -> {
//                            switch (arrayType) {
//                                case "int":
//                                    return Integer.valueOf(s);
//                                case "float":
//                                    return Float.valueOf(s);
//                                case "double":
//                                    return Double.valueOf(s);
//                                case "byte":
//                                    return HexConvertUtil.hexStringToBytes(s)[0];
//                                case "boolean":
//                                    return "true,1,y,yes".contains(s.toLowerCase());
//                                default:
//                                    return s;
//                            }
//                        }).toArray(Byte[]::new);
            case "int":
                return Integer.valueOf(value.toString());
            case "float":
                return Float.valueOf(value.toString());
            case "double":
                return Double.valueOf(value.toString());
            case "byte":
                return HexConvertUtil.hexStringToBytes(value.toString())[0];
            case "boolean":
                return "true,1,y,yes".contains(value.toString().toLowerCase());
            case "string":
                return value.toString();
            case "enum":
                return null;
            default:
                return null;
        }
    }
}
