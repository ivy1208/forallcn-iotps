package com.zhouwr.common.metadata;

import com.alibaba.fastjson.JSON;
import com.zhouwr.common.enums.ModelDataType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-28 22:46
 * @version：1.0
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BoolData extends AbstractData<BoolData> implements DataType, Converter<Boolean>{

    private String trueText;
    private String falseText;


    private Boolean value = false;
    private ModelDataType type = ModelDataType.BOOL;

    @Override
    public ValidateResult validate(Object value) {
        return null;
    }

    public static Metadata form(String metadata) {
        return JSON.toJavaObject(JSON.parseObject(metadata), BoolData.class);
    }

    @Override
    public Boolean convert(Object value) {
        if (value instanceof Boolean) {
            return ((Boolean) value);
        }

        String stringVal = String.valueOf(value).trim();
        if (stringVal.equals(trueText)) {
            return true;
        }

        if (stringVal.equals(falseText)) {
            return false;
        }
        return stringVal.equals("1")
                || stringVal.equals("true")
                || stringVal.equals("y")
                || stringVal.equals("yes")
                || stringVal.equals("ok")
                || stringVal.equals("是")
                || stringVal.equals("正常");
    }

    @Override
    public Object format(Object value) {
        return JSON.toJSON(value);
    }

    public static void main(String[] args) {
        BoolData boolData = new BoolData();
        boolData.value = false;
        boolData.falseText = "错误";
        boolData.trueText = "正确";
        System.out.println(boolData);
    }
}
