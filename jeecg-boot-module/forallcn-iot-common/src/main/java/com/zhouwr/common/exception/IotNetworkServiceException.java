package com.zhouwr.common.exception;

/**
 * @program: forallcn-iotps
 * @description: 物联网服务异常
 * @author: zhouwr
 * @create: 2020-12-09 11:47
 * @version：1.0
 **/
public class IotNetworkServiceException extends RuntimeException{
    public IotNetworkServiceException(Object msg) {
        super(msg.toString());
    }
}
