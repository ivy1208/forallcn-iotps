package com.zhouwr.common.device.vo.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.message.SendMessage;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 实例函数签证官
 *
 * @author zhouwenrong
 * @date 2022/02/11
 */
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InstanceFunctionVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 实例id
     */
    private String instanceId;
    /**
     * 实例名
     */
    private String instanceName;
    /**
     * 模型id
     */
    private String modelId;
    /**
     * 模型名称
     */
    private String modelName;

    /**
     * id 功能id
     */
    private String id;
    /**
     * 代码
     */
    private java.lang.String code;
    /**
     * 名字
     */
    private java.lang.String name;
    /**
     * 类型
     */
    private java.lang.String type;
    /**
     * 是同步
     */
    private java.lang.Boolean isSync;
    /**
     * 描述
     */
    private java.lang.String description;

    /**
     * 输入参数个数
     */
    private List<FunctionParamVo> inputParams;

    /**
     * 输出参数
     */
    private FunctionParamVo outputParam;

    /**
     * 调用配置
     */
    private FunctionExecuteConfig executeConfig;

    public SendMessage toSendMessage() {
        return new SendMessage(
                this.name,
                this.code,
                this.modelId,
                this.instanceId,
                this.inputParams,
                this.outputParam);
    }
}
