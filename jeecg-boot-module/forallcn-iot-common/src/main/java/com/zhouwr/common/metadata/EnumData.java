package com.zhouwr.common.metadata;

import com.alibaba.fastjson.JSON;
import com.zhouwr.common.enums.ModelDataType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-28 22:22
 * @version：1.0
 **/
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnumData extends AbstractData<EnumData> implements DataType{

    /** 枚举项 */
    Map<Object,String> items = new HashMap<Object,String>();

    private Object value = new Object();

    private ModelDataType type = ModelDataType.ENUM;

    public static Metadata form(String metadata) {
        log.info("{}", metadata);
        return JSON.toJavaObject(JSON.parseObject(metadata), EnumData.class);
    }

    @Override
    public ValidateResult validate(Object value) {
        return null;
    }

    @Override
    public ModelDataType getType() {
        return this.type;
    }

    @Override
    public Object format(Object value) {
        return null;
    }
}
