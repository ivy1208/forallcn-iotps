package com.zhouwr.common.network;

import com.zhouwr.common.enums.NetworkType;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public abstract class CommonConnectContext implements NetworkConnectContext {

    /**
     * 网络接入时间
     */
    private long registTime;

    /**
     * 网络注销时间
     */
    private long unregistTime;

    /**
     * 网络类型
     */
    private NetworkType networkType;

    private ChannelHandlerContext channelHandlerContext;

    /**
     * 设备数据解析器
     * DeviceDataParser
     */
    private DeviceMessageCodec messageCodec;

    private Network network;

    private NetworkDevice device;

    @Override
    public String getDeviceId() {
        return Optional.ofNullable(device).orElseThrow().getId();
    }

    @Override
    public String getDeviceName() {
        return Optional.ofNullable(device).orElseThrow().getName();
    }

    @Override
    public String getSceneBy() {
        return Optional.ofNullable(device).orElseThrow().getSceneBy();
    }
}
