package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 告警级别
 *
 * @author zhouwenrong
 */
@AllArgsConstructor
@Getter
public enum AlarmLevel implements IDictEnum<Integer> {

    /**
     *
     */
    NORMAL(0, "正常", "green"),
    GENERAL(1, "一般", "darkseagreen"),
    SERIOUS(2, "严重", "darkorange"),
    CRITICAL(3, "危急", "red");

    @EnumValue
    private final Integer value;
    private final String text;
    private final String color;

    @JsonCreator
    public static AlarmLevel of(Integer value) {
        final List<AlarmLevel> alarmLevel = Arrays.stream(AlarmLevel.values())
                .filter(type -> type.value.equals(value))
                .collect(Collectors.toList());
        if (alarmLevel.size() > 0) {
            return alarmLevel.get(0);
        }
        return null;
    }

    public static JSONArray toJSONArray() {
        return Arrays.stream(AlarmLevel.values())
                .map(type -> {
                    JSONObject json = new JSONObject();
                    json.put("text", type.text);
                    json.put("value", type.value);
                    json.put("color", type.color);
                    return json;
                }).collect(Collectors.toCollection(JSONArray::new));
    }

    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        json.put("text", this.text);
        json.put("value", this.value);
        json.put("color", this.color);
        return json;
    }
}
