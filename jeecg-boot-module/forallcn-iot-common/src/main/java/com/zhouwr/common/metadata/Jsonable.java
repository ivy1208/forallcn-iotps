package com.zhouwr.common.metadata;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface Jsonable {

    default JSONObject toJson() {
        return new ObjectMapper().convertValue(this, JSONObject.class);
    }

    default void fromJson(JSONObject json) {
        JSONObject.parseObject(json.toJSONString(), this.getClass());
    }
}
