package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.zhouwr.common.network.CommonConnectContext;
import com.zhouwr.common.network.TcpConnectContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */
@AllArgsConstructor
@Getter
public enum NetworkType implements IDictEnum<String> {

    /**
     *
     */
    TCP_CLIENT("tcp-client", "TCP客户端", TcpConnectContext::new),
    TCP_SERVER("tcp-server", "TCP服务", TcpConnectContext::new),
    TCP_UDP("tcp-udp", "UDP", TcpConnectContext::new),

    MQTT_SUBSCRIPTION("mqtt-subscription", "MQTT订阅", TcpConnectContext::new),
    MQTT_PUBLISH("mqtt-publish", "MQTT发布", TcpConnectContext::new),

    HTTP_CLIENT("http-client", "HTTP客户端", TcpConnectContext::new),
    HTTP_SERVER("http-server", "HTTP服务", TcpConnectContext::new),

    WEB_SOCKET_CLIENT("websocket-client", "WebSocket客户端", TcpConnectContext::new),
    WEB_SOCKET_SERVER("websocket-server", "WebSocket服务", TcpConnectContext::new),

    COAP_CLIENT("coap-client", "CoAP客户端", TcpConnectContext::new),
    COAP_SERVER("coap-server", "CoAP服务", TcpConnectContext::new),

    ;

    @EnumValue
    private final String code;
    private final String name;
    private final Supplier<CommonConnectContext> connectContext;

    @JsonCreator
    public static NetworkType of(String value) {
        for (NetworkType networkType : values()) {
            if (networkType.code.equals(value)) {
                return networkType;
            }
        }
        throw new UnsupportedOperationException("不支持的消息事件:" + value);
    }

    public static List<Map<String, Object>> toJSONArray() {
        return Arrays.stream(NetworkType.class.getEnumConstants()).map(eventLevel -> {
            Map<String, Object> map = new HashMap<>();
            map.put("value", eventLevel.getCode());
            map.put("text", eventLevel.getName());
            return map;
        }).collect(Collectors.toList());
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
