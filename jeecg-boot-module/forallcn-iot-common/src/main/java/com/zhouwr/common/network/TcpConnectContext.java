package com.zhouwr.common.network;

import com.zhouwr.common.enums.ProtocolType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.net.SocketAddress;

@Getter
@Setter
@NoArgsConstructor
public class TcpConnectContext extends CommonConnectContext {

    /**
     * 网络接入地址-客户端：/192.168.1.100:8888
     */
    private SocketAddress socketAddress;

    /**
     * 网络行为：1-注册、0-注销
     */
    private int action;

    @Override
    public ProtocolType getNetworkConnectType() {
        return ProtocolType.TCP;
    }
}
