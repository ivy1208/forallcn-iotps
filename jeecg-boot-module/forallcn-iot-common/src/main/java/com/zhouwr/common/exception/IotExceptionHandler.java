package com.zhouwr.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理器
 *
 * @Author scott
 * @Date 2019
 */
@RestControllerAdvice
@Slf4j
public class IotExceptionHandler {

    // todo 集成websocket

    /**
     * 处理设备配置异常
     */
    @ExceptionHandler(IotConfigException.class)
    public Result<?> handleConfigException(IotConfigException e) {
        log.error(e.getMessage(), e);
        return Result.error(e.getMessage());
    }

    /**
     * 网络服务异常
     * @param e
     * @return
     */
    @ExceptionHandler(IotNetworkServiceException.class)
    public Result<?> handleDeviceOfflineException(IotNetworkServiceException e){
        log.error(e.getMessage(), e);
        return Result.error(e.getMessage());
    }

    /**
     * 设备离线异常
     * @param e
     * @return
     */
    @ExceptionHandler(IotDeviceOfflineException.class)
    public Result<?> handleDeviceOfflineException(IotDeviceOfflineException e){
        log.error(e.getMessage(), e);
        return Result.error(e.getMessage());
    }


}
