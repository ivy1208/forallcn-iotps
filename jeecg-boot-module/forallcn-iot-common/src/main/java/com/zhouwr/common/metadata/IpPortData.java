package com.zhouwr.common.metadata;

import com.zhouwr.common.enums.ModelDataType;
import lombok.*;

/**
 * @program: forallcn-iotps
 * @description: 字符串类型
 * @author: zhouwr
 * @create: 2020-11-19 15:15
 * @version：1.0
 **/
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class IpPortData extends StringData {
    private String value = "192.168.1.1:80";
    private ModelDataType type = ModelDataType.IPPORT;
}
