package com.zhouwr.common.message;

import com.zhouwr.common.enums.ReportMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: jeecg-boot-module-iot
 * @description: 网络入栈数据类
 * @author: zhouwr
 * @create: 2020-05-26 15:09
 * @version：1.0
 **/
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReceiveMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据节点
     */
    Map<String, Object> dataMap = new HashMap<>();
    /**
     * 消息类型
     */
    private String type;
    /**
     * 设备实例
     */
    private String instanceId;
    /**
     * 接收时间
     */
    private long datetime;

    private ReportMode reportMode;

    public void addData(String code, Object value) {
        this.dataMap.put(code, value);
    }

}
