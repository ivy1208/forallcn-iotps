package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.zhouwr.common.metadata.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum ModelDataType implements IDictEnum<String> {
    /**
     *
     */
    STRING("字符串", "string", StringData::new),
    INT("整型", "int", IntData::new),
    LONG("长整型", "long", LongData::new),
    FLOAT("单精度浮点", "float", FloatData::new),
    DOUBLE("双精度浮点", "double", DoubleData::new),
    ENUM("枚举", "enum", EnumData::new),
    BOOL("布尔", "bool", BoolData::new),
    ARRAY("数组", "array", ArrayData::new),
    BYTE("字节", "byte", ByteData::new),
    IP("IP", "ip", IpData::new),
    IPPORT("IP:PORT", "ipPort", IpPortData::new),
    VECTOR3("三维向量", "vector3", Vector3Data::new),
    DATETIME("时间", "datetime", DateTimeData::new),
    DEFAULT("默认类型", "default", null);

    private final String name;
    @EnumValue
    private final String code;
    private final Supplier<DataType> dataType;


    @JsonCreator
    public static ModelDataType of(String value) {
        for (ModelDataType dataType : values()) {
            if (dataType.code.equalsIgnoreCase(value)) {
                return dataType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(ModelDataType.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

    @JsonValue
    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
