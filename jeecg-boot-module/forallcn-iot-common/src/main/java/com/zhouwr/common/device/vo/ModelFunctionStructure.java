package com.zhouwr.common.device.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: forallcn-iotps
 * @description: 模型功能结构体
 * @author: zhouwr
 * @create: 2020-11-21 17:18
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelFunctionStructure {
    /**
     * 设备模型id
     */
    String modelId;

    /**
     * 1、功能参数
     */
    private DeviceFunctionVo function;

    /**
     * 2、输入参数
     */
    List<DeviceFunctionParamVo> inputParams;
    /**
     * 3、输出参数
     */
    DeviceFunctionParamVo outputParam;

}
