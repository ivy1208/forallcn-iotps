package com.zhouwr.common.metadata;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.utils.Vector3;
import lombok.*;

/**
 * @program: forallcn-iotps
 * @description: 三维坐标
 * @author: zhouwr
 * @create: 2020-11-24 16:20
 * @version：1.0
 **/

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Vector3Data extends AbstractData<Vector3Data> implements DataType, Converter<Vector3> {
    private Vector3 value = new Vector3(0, 0, 0);

    private ModelDataType type = ModelDataType.VECTOR3;

    @Override
    public Vector3 convert(Object value) {
        if (value instanceof Vector3) {
            this.value = (Vector3) value;
            return this.value;
        }
        if (value instanceof String) {
            this.value = new Vector3(value.toString());
            return this.value;
        }
        if (value instanceof JSONObject){
            this.value = new Vector3((JSONObject) value);
            return this.value;
        }
        return null;
    }

    @Override
    public Object format(Object value) {
        if(value instanceof Vector3) {
            return value.toString();
        }
        return JSON.toJSON(value);
    }

    @Override
    public ValidateResult validate(Object value) {
        Vector3 convert = convert(value);
        if(convert != null) {
            return ValidateResult.success();
        } else {
            return ValidateResult.fail("三维向量验证失败");
        }
    }

    public static void main(String[] args) {
        Vector3Data vector3Data = new Vector3Data();
        vector3Data.validate("7.0,8.0,9.0");
        vector3Data.convert("3,4,5");
        System.out.println(JSON.toJSON(vector3Data));
    }
}
