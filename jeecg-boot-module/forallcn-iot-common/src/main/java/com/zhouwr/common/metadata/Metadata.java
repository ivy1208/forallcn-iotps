package com.zhouwr.common.metadata;

import java.util.Map;

/**
 * 元数据
 *
 * @author zhouwenrong
 * @description: 基础数据
 * @version：1.0
 * @date 2022/02/18
 */
public interface Metadata extends FormatSupport{

    String getDescription();

    Map<String, Object> getExpands();

    default Object getExpand(String name) {
        return getExpands().get(name);
    }

    default void setExpands(Map<String, Object> expands) {
    }

    default void setDescription(String description) {

    }
}
