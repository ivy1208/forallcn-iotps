package com.zhouwr.common.message;

import com.zhouwr.common.metadata.Jsonable;

import java.io.Serializable;

public interface Message extends Jsonable, Serializable {

    default MessageType getMessageType() {
        return MessageType.UNKNOWN;
    }

    /**
     * 消息的唯一标识,用于在请求响应模式下对请求和响应进行关联.
     * <p>
     * 注意: 此消息ID为全系统唯一. 但是在很多情况下,设备可能不支持此类型的消息ID,
     * 此时需要在协议包中做好映射关系
     * @return 消息ID
     */
    String getMessageId();

    /**
     * @return 毫秒时间戳
     * @see System#currentTimeMillis()
     */
    long getTimestamp();
}
