package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */
@AllArgsConstructor
@Getter
public enum NoticeType implements IDictEnum<Integer> {
    /**
     *
     */
    INTERNET_SERVICE(6, "网络服务"),
    SYSTEM_INFORMATION(4, "系统消息"),
    WECHAT_MESSAGE(3, "微信消息"),
    DINGDING_MESSAGE(5, "钉钉消息"),
    SMS_MESSAGE(1, "短信消息"),
    EMAIL_NOTIFICATION(2, "邮件通知");

    @EnumValue
    private final Integer value;
    private final String text;

    @JsonCreator
    public static NoticeType of(Integer value) {
        for (NoticeType type : NoticeType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toJSONArray() {
        return Arrays.stream(NoticeType.values())
                .map(type -> {
                    JSONObject json = new JSONObject();
                    json.put("text", type.text);
                    json.put("value", type.value);
                    return json;
                }).collect(Collectors.toCollection(JSONArray::new));
    }
}
