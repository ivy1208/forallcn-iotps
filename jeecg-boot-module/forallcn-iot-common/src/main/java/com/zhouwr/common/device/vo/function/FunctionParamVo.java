package com.zhouwr.common.device.vo.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.FunctionParamDirection;
import com.zhouwr.common.enums.FunctionParamInputMode;
import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.Converter;
import com.zhouwr.common.metadata.DataType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Optional;

/**
 * 功能参数VO类
 *
 * @author zhouwenrong
 * @date 2022/02/13
 */
@Data
@Slf4j
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FunctionParamVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 参数id: deviceData.id
     */
    private String id;
    /**
     * 参数名字
     */
    private String name;
    /**
     * 参数标识
     */
    private String code;
    /**
     * 类型
     */
    private String type;
    /**
     * 参数方向: 输入、输出
     */
    private FunctionParamDirection direction;
    /**
     * 输入模式
     */
    private FunctionParamInputMode inputMode;

    private Integer sort;

    public void setMetadata(DataType metadata) {
        log.info("type：{}，metadata : {}", type, metadata);
        if(metadata != null) {
            this.metadata = metadata;
        } else {
            this.metadata = Optional.ofNullable(this.type)
                    .map(type -> ModelDataType.of(this.type).getDataType().get())
                    .orElse(null);
        }
        log.info("type：{}，metadata : {}", type, metadata);
    }

    /**
     * 元数据
     */
    private DataType metadata;
    /**
     * 参数值
     */
    private Object value;

    private String formatValue;

    /**
     * 参数描述
     */
    private String description;

    public Object setValue(Object value) {
        log.info("type：{}，value({})：{}，metadata : {}", type, value.getClass(), value, metadata);
        if(metadata != null) {
            this.value = ((Converter<?>)metadata).convert(value);
            this.formatValue = String.valueOf(metadata.format(this.value));
        } else {
            this.value = ((Converter<?>)ModelDataType.of(this.type).getDataType().get()).convert(value);
            this.formatValue = String.valueOf(value);
        }
        log.info("{} setValue: {}-{}", name, this.value, this.formatValue);
        return this.value;
    }

//    public String getFormatValue() {
//        if(metadata != null) {
//            this.formatValue = String.valueOf(metadata.format(this.value));
//        } else {
//            return String.valueOf(ModelDataType.of(this.type).getDataType().get().format(this.value));
//        }
//        return this.formatValue;
//    }

    @Override
    public String toString() {
        return "FunctionParamVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", direction=" + direction +
                ", inputMode=" + inputMode +
                ", metadata=" + metadata +
                ", value=" + value +
                ", description='" + description + '\'' +
                '}';
    }

}
