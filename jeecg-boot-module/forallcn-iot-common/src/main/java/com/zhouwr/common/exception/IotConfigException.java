package com.zhouwr.common.exception;

/**
 * @program: forallcn-iotps
 * @description: 设备配置异常
 * @author: zhouwr
 * @create: 2020-12-07 16:41
 * @version：1.0
 **/
public class IotConfigException extends RuntimeException {

    public IotConfigException(Object msg) {
        super(msg.toString());
    }

    public IotConfigException(String msg) {
        super(msg.toString());
    }
}
