package com.zhouwr.common.device.vo.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 函数执行配置
 *
 * @author zhouwenrong
 * @date 2022/02/11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FunctionExecuteConfig {
    private String id;

    /**
     * 定时表达式
     */
    private String cron;
    /**
     * 执行模式
     */
    private String mode = "task";

    /**
     * 正在运行
     */
    private Boolean isRunning = false;

    public static void main(String[] args) {
        FunctionExecuteConfig config = new FunctionExecuteConfig();
        config.getIsRunning();
    }
}
