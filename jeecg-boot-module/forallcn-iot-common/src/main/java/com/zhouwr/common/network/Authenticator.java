package com.zhouwr.common.network;

/**
 * 认证器(Authenticator)是用于在收到设备请求(例如MQTT)时,对客户端进行认证时使用,不同的网络协议(Transport)使用不同的认证器.
 */
public interface Authenticator {
}
