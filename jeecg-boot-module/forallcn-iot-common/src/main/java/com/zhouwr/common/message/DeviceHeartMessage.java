package com.zhouwr.common.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 心跳消息
 * @author zhouwenrong
 * @date 2022/03/20 17:50
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceHeartMessage extends DeviceMessage{

    public MessageType getMessageType() {
        return MessageType.HEART;
    }

    /**
     * 心跳内容
     */
    private Object heartContent;
    private Class<?> clazz;
}
