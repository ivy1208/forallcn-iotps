package com.zhouwr.common.device.vo.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.Converter;
import com.zhouwr.common.metadata.DataType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * 功能参数VO类
 *
 * @author zhouwenrong
 * @date 2022/02/13
 */
@Data
@Slf4j
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportParamVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 参数id: deviceData.id
     */
    private String id;
    /**
     * 参数名字
     */
    private String name;
    /**
     * 参数标识
     */
    private String code;
    /**
     * 类型
     */
    private String type;

    /**
     * 元数据
     */
    private DataType metadata;

    /**
     * 警告
     */
    private ParamAlarmVo paramAlarm;

    /**
     * 参数值
     */
    private Object value;

    /**
     * 格式值
     */
    private String formatValue;

    /**
     * 参数描述
     */
    private String description;

    public void setMetadata(DataType metadata) {
        if(metadata != null) {
            this.metadata = metadata;
        } else {
            if(StringUtils.isNotBlank(this.type)) {
                this.metadata = ModelDataType.of(this.type).getDataType().get();
            } else {
                this.metadata = metadata;
            }
        }
    }

    public Object setValue(Object value) {
        if(metadata != null) {
            this.value = ((Converter<?>)metadata).convert(value);
            this.formatValue = String.valueOf(metadata.format(this.value));
        } else {
            this.value = String.valueOf(((Converter<?>)ModelDataType.of(this.type).getDataType().get()).convert(this.value));;
            this.formatValue = String.valueOf(value);
        }
        return this.value;
    }

//    public String getFormatValue() {
//        if(metadata != null) {
//            this.formatValue = String.valueOf(metadata.format(this.value));
//        } else {
//            return String.valueOf(ModelDataType.of(this.type).getDataType().get().format(this.value));
//        }
//        return this.formatValue;
//    }

    @Override
    public String toString() {
        return "FunctionParamVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", metadata=" + metadata +
                ", value=" + value +
                ", description='" + description + '\'' +
                '}';
    }
}
