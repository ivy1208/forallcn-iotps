package com.zhouwr.common.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 功能参数输入模式
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum FunctionParamInputMode implements IDictEnum<String> {

    /**
     *
     */
    INS_INPUT("ins_input", "实例注入"),
    OPT_INPUT("opt_input", "操作输入");

    @EnumValue
    private final String code;
    private final String name;

    @JsonCreator
    public static FunctionParamInputMode of(String value) {
        for (FunctionParamInputMode inputMode : FunctionParamInputMode.values()) {
            if (inputMode.code.equalsIgnoreCase(value)) {
                return inputMode;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(FunctionParamInputMode.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

    @JsonValue
    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
