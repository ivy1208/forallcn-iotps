package com.zhouwr.common.exception;

/**
 * @program: forallcn-iotps
 * @description: 设备数据节点异常
 * @author: zhouwr
 * @create: 2020-12-24 11:53
 * @version：1.0
 **/
public class IotDeviceDataNodeException extends RuntimeException{
    public IotDeviceDataNodeException(String msg){
        super(msg);
    }

    public IotDeviceDataNodeException(String instance, String msg){
        super("设备："+instance+ ", 异常信息：" +msg);
    }
}
