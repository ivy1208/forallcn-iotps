package com.zhouwr.common.exception;

/**
 * @program: forallcn-iotps
 * @description: 设备掉线异常
 * @author: zhouwr
 * @create: 2020-12-09 11:40
 * @version：1.0
 **/
public class IotDeviceOfflineException extends RuntimeException {
    public IotDeviceOfflineException(String msg) {
        super(msg);
    }

    public IotDeviceOfflineException(String instanceId, String instanceName) {
        super("设备：" + instanceName + "[" + instanceId + "]" + "已离线！");
    }
}
