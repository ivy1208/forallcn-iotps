package com.zhouwr.common.metadata.typeHandler;

import com.zhouwr.common.metadata.unit.UnifyUnit;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UnitTypeHandler extends BaseTypeHandler<UnifyUnit> {
    // 写库时数据转化
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, UnifyUnit unit, JdbcType jdbcType) throws SQLException {
        ps.setString(i, String.valueOf(unit.getSymbol()));
    }

    // 读库时数据转化
    @Override
    public UnifyUnit getNullableResult(ResultSet rs, String unit) throws SQLException {
        return UnifyUnit.of(rs.getString(unit));
    }

    // 读库时数据转化
    @Override
    public UnifyUnit getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return UnifyUnit.of(rs.getString(columnIndex));
    }

    // 读库时数据转化
    @Override
    public UnifyUnit getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return UnifyUnit.of(cs.getString(columnIndex));
    }
}
