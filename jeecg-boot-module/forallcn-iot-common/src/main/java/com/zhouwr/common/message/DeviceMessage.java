package com.zhouwr.common.message;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 设备消息
 */
@Data
@NoArgsConstructor
public class DeviceMessage implements Message{
    private static final long serialVersionUID = -6849794470754667710L;

    /**
     * 设备id，instanceId
     */
    private String deviceId;

    /**
     * 设备名称，instanceName
     */
    private String deviceName;

    /**
     * 消息id
     */
    private String messageId;

    /**
     * 消息头
     */
    private Map<String, Object> headers;

    /**
     * 时间
     */
    private long timestamp = System.currentTimeMillis();
}
