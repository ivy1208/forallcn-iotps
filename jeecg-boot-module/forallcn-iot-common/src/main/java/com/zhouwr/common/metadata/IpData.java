package com.zhouwr.common.metadata;

import com.alibaba.fastjson.JSON;
import com.zhouwr.common.enums.ModelDataType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description: 字符串类型
 * @author: zhouwr
 * @create: 2020-11-19 15:15
 * @version：1.0
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class IpData extends AbstractData<IpData> implements DataType, Converter<String>{
    private String value = "192.168.1.1";
    private ModelDataType type = ModelDataType.IP;

    public static Metadata form(String metadata) {
        return JSON.toJavaObject(JSON.parseObject(metadata), IpData.class);
    }

    @Override
    public String convert(Object value) {
        return null;
    }

    @Override
    public Object format(Object value) {
        return null;
    }

    @Override
    public ValidateResult validate(Object value) {
        return null;
    }

}
