package com.zhouwr.common.device.vo.InternetDevice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 联网设备
 *
 * @author zhouwenrong
 * @date 2022/02/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InternetDeviceVo {
    /**
     * id
     */
    private String id;
    /**
     * 模型id
     */
    private String modelId;
    /**
     * 实例名
     */
    private String instanceName;
    /**
     * 实例状态
     */
    private String instanceStatus;
    /**
     * 函数id
     */
    private String functionId;
    /**
     * 函数名
     */
    private String functionName;
    /**
     * 函数参数
     */
    private List<InternetParam> internetParams;
}

