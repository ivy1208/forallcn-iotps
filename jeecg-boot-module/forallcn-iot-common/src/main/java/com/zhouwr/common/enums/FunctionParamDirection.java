package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.StringJoiner;

/**
 * 功能参数输入模式
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum FunctionParamDirection implements IDictEnum<String> {

    /**
     *
     */
    INPUT("input", "输入"),
    OUTPUT("output", "输出");

    @EnumValue
    private final String code;
    private final String name;

    @JsonCreator
    public static FunctionParamDirection of(String value) {
        for (FunctionParamDirection direction : FunctionParamDirection.values()) {
            if (direction.code.equalsIgnoreCase(value)) {
                return direction;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FunctionParamDirection.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("name='" + name + "'")
                .toString();
    }

    @JsonValue
    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
