package com.zhouwr.common.metadata;

public interface Converter<T> {

    T convert(Object value);
}
