package com.zhouwr.common.metadata;

public interface FormatSupport {
    Object format(Object value);
}
