package com.zhouwr.common.enums;


import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
@JSONType(serializeEnumAsJavaBean = true)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum DeviceConfigType implements IDictEnum<String> {

    /**
     *
     */
    DATA("数据", "data"),
    FUNCTION("功能", "function"),
    EVENT("事件", "event"),
    LABEL("标签", "label"),
    OTHER("其它", "other");

    @EnumValue
    private final String name;
    private final String code;

    @JsonCreator
    public static DeviceConfigType of(String value) {
        for (DeviceConfigType configType : DeviceConfigType.values()) {
            if (configType.code.equals(value)) {
                return configType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
