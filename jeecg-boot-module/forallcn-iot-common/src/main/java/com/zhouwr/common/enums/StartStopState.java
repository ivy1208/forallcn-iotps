package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/**
 * @author zhouwenrong
 */

@Getter
@AllArgsConstructor
public enum StartStopState implements IDictEnum<Byte> {
    /**
     *
     */
    STARTED((byte) 0x01, "已启动"),
    STOPPED((byte) 0x00, "已停止");

    @EnumValue
    private final Byte value;
    private final String text;

    @JsonCreator
    public static StartStopState of(Byte value) {
        for (StartStopState state : StartStopState.values()) {
            if (state.value.equals(value)) {
                return state;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }
}
