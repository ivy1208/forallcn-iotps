package com.zhouwr.common.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
@JSONType(serializeEnumAsJavaBean = true)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FuncExecMode {

    /**
     *
     */
    TASK("任务", "task"),
    DEBUG("调试", "debug");

    private final String name;
    private final String code;
}
