package com.zhouwr.common.device.vo.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 实例函数签证官
 *
 * @author zhouwenrong
 * @date 2022/02/11
 */
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelFunctionVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 模型id
     */
    private String modelId;

    /**
     * id
     */
    private String id;
    /**
     * 代码
     */
    private String code;
    /**
     * 名字
     */
    private String name;
    /**
     * 类型
     */
    private String type;
    /**
     * 是同步
     */
    private Boolean isSync;
    /**
     * 描述
     */
    private String description;

    /**
     * 输入参数
     */
    private List<FunctionParamVo> inputParams = new ArrayList<>();

    /**
     * 输出参数
     */
    private FunctionParamVo outputParam;

}
