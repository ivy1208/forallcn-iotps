package com.zhouwr.common.device.vo;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 功能执行结构（提交后台数据）
 *
 * @author: zhouwr
 * @create: 2020-11-25 21:52
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InstanceFunctionExecStructure implements Serializable {
    private static final long serialVersionUID = 1L;

    private String modelId;
    private String instanceId;
    private String functionCode;
    private String type;
    /**
     * 是否同步，false => 异步
     */
    private Boolean isSync = false;
    private List<InstanceFunctionInputParam> inputParams;
    private InstanceFunctionInputParam outputParam;
    private InstanceFunctionExecConfig execConfig;

    public InstanceFunctionExecStructure(String instanceId, InstanceFunctionStructure functionStructure) {
        this.modelId = functionStructure.getDeviceModelId();
        this.instanceId = instanceId;
        this.functionCode = functionStructure.getCode();
        this.type = functionStructure.getType();
        this.isSync = functionStructure.getIsSync();
        this.inputParams = functionStructure.getInputParams();
        this.execConfig = functionStructure.getExecConfig();
    }

    public InstanceFunctionExecStructure(String instanceId, DeviceFunctionVo function, List<InstanceFunctionInputParam> functionInputParams, String funcExecConf) {
        this.modelId = function.getDeviceModelBy();
        this.instanceId = instanceId;
        this.functionCode = function.getCode();
        this.type = function.getType();
        this.isSync = function.getIsSync();
        this.inputParams = functionInputParams;
        final InstanceFunctionExecConfig execConfig = JSON.parseObject(funcExecConf, InstanceFunctionExecConfig.class);
        if (execConfig == null) {
            this.execConfig = new InstanceFunctionExecConfig("* * * * * ? *", "task", false);
        } else {
            this.execConfig = execConfig;
        }
    }

}
