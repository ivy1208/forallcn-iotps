package com.zhouwr.common.device.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: 功能定义VO
 * @Author: zhouwr
 * @Date: 2020-12-05
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceFunctionVo {
    private String id;
    private String deviceModelBy;
    private String code;
    private String name;
    private String type;
    private Boolean isSync;
}
