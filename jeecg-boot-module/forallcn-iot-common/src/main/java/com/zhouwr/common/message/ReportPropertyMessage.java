package com.zhouwr.common.message;

import java.util.Map;

/**
 * 设备上报属性消息
 * @author zhouwenrong
 * @date 2022/03/19 23:35
 */
public class ReportPropertyMessage extends DeviceMessage{
    /**
     * 设备属性（数据节点）map，key：属性id；value：属性数值
     */
    private Map<String, Object> properties;
}
