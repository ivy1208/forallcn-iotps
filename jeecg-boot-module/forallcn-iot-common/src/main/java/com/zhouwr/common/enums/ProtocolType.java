package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/***
 * 协议类型
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum ProtocolType implements IDictEnum<String> {
    /**
     *
     */
    HTTP("http", "Http协议"),
    TCP("tcp", "Tcp协议"),
    MQTT("mqtt", "Mqtt协议");

    @EnumValue
    private final String value;
    private final String text;

    @JsonCreator
    public static ProtocolType of(String value) {
        for (ProtocolType protocolType : ProtocolType.values()) {
            if (protocolType.value.equals(value)) {
                return protocolType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

}
