package com.zhouwr.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Function;

/**
 * @program: forallcn-iotps
 * @description: mqtt主题
 * @author: zhouwr
 * @create: 2020-12-22 15:47
 * @version：1.0
 */
@Getter
@AllArgsConstructor
public enum MqttTopics {

    /**
     *
     */
    DATA_RECEIVE("/data/receive", "数据接收", sceneId -> {
        return "@forallcn/iotps/" + sceneId + "/data/receive";
    }),

    DEVICE_ONLINE("/device/online", "设备上线", sceneId -> {
        return "@forallcn/iotps/" + sceneId + "/data/online";
    }),

    DEVICE_OFFLINE("/device/offline", "设备离线", sceneId -> {
        return "@forallcn/iotps/" + sceneId + "/data/offline";
    }),

    INVOKE_FUNCTION("/device/function", "执行功能", sceneId -> {
        return "@forallcn/iotps/" + sceneId + "/device/function";
    });

    private final String topic;
    private final String name;
    /**
     * 输入设备id，输出场景id
     */
    private final Function<String, String> sceneTopic;

}
