package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 消息事件类型
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum MessageEventType implements IDictEnum<String> {
    /**
     *
     */
    DEVICE_ONLINE("设备上线", "device_online", 0),
    DEVICE_OFFLINE("设备离线", "device_offline", 1),
    DEVICE_NOT_FIND("设备未发现", "device_not_find", 1),
    NETWORK_START("服务开启", "network_start", 0),
    NETWORK_STOP("服务关闭", "network_close", 0),
    NETWORK_ERROR("服务错误", "network_error", 2),
    DATA_RECEIVE("数据接收", "data_receive", 0),
    ;
    private final String name;
    @EnumValue
    private final String code;
    private final Integer level;

    @JsonCreator
    public static MessageEventType of(String value) {
        for (MessageEventType eventType : values()) {
            if (eventType.code.equals(value)) {
                return eventType;
            }
        }
        throw new UnsupportedOperationException("不支持的消息事件:" + value);
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }

    public static List<MessageEventType> toList(){
        return Arrays.stream(MessageEventType.class.getEnumConstants()).collect(Collectors.toList());
    }

    public static List<Map<String, Object>> toJSONArray() {
        return Arrays.stream(MessageEventType.class.getEnumConstants()).map(eventLevel -> {
            Map<String, Object> map = new HashMap<>();
            map.put("value", eventLevel.getValue());
            map.put("code", eventLevel.getCode());
            map.put("name", eventLevel.getName());
            return map;
        }).collect(Collectors.toList());
    }
}
