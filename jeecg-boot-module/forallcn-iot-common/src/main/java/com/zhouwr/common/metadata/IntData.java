package com.zhouwr.common.metadata;

import com.alibaba.fastjson.JSON;
import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.unit.UnifyUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @program: forallcn-iotps
 * @description: 整数数据结构
 * @author: zhouwr
 * @create: 2020-11-19 16:00
 * @version：1.0
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IntData extends NumberData<Integer> {
    private Integer value = 0;
    private ModelDataType type = ModelDataType.INT;

    @Override
    public Integer convert(Object value) {
        this.value = super.convertNumber(value, Number::intValue);
        return this.value;
    }

    public static void main(String[] args) {
        IntData intData = new IntData();
        intData.value = 89;
        intData.setMax(100);
        intData.setMin(10);
        intData.setUnit(UnifyUnit.meter);
        System.out.println(intData);
        System.out.println(JSON.toJSON(intData));
        System.out.println(intData.convert("999.33"));
        System.out.println(intData.validate(999));
        System.out.println(JSON.toJSON(intData));
        System.out.println(intData.format(99));
    }
}
