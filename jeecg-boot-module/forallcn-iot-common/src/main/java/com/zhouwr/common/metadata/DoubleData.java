package com.zhouwr.common.metadata;

import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.unit.UnifyUnit;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
@Setter
public class DoubleData extends NumberData<Double> {
    public static final String ID = "double";

    private Integer accuracy = 2;
    private Double value = 0d;
    private ModelDataType type = ModelDataType.DOUBLE;

    public DoubleData accuracy(Integer accuracy) {
        this.accuracy = accuracy;
        return this;
    }

    @Override
    public Object format(Object value) {
        Number val = convertNumber(value);
        if (val == null) {
            return super.format(value);
        }
        String scaled = new BigDecimal(val.toString())
                .setScale(accuracy, RoundingMode.HALF_UP)
                .toString();
        return super.format(scaled);
    }

    @Override
    public Double convert(Object value) {
        return super.convertNumber(value, Number::doubleValue);
    }

    public static void main(String[] args) {
        DoubleData doubleData = new DoubleData();
        doubleData.value = 89.9999d;
        doubleData.setMax(100);
        doubleData.setMin(10);
        doubleData.setUnit(UnifyUnit.meter);
        System.out.println(doubleData);
    }
}
