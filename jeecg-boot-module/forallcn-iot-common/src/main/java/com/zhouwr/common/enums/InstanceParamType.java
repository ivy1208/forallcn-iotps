package com.zhouwr.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.StringJoiner;

/**
 * 功能参数输入模式
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum InstanceParamType implements IDictEnum<String> {

    /**
     *
     */
    INPUT("input", "输入"),
    OUTPUT("output", "输出"),
    EVENT("event", "事件");

    @EnumValue
    private final String code;
    private final String name;

    @JsonCreator
    public static InstanceParamType of(String code) {
        for (InstanceParamType value : InstanceParamType.values()) {
            if (value.code.equalsIgnoreCase(code)) {
                return value;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + code);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", InstanceParamType.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("name='" + name + "'")
                .toString();
    }

    @JsonValue
    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
