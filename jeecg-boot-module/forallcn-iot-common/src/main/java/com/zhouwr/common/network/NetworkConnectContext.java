package com.zhouwr.common.network;

import com.zhouwr.common.enums.NetworkType;
import com.zhouwr.common.enums.ProtocolType;
import io.netty.channel.ChannelHandlerContext;

import java.net.SocketAddress;

public interface NetworkConnectContext {
    /* 网络连接类型 */
    ProtocolType getNetworkConnectType();

    /* 设备id，instanceId */
    String getDeviceId();

    /* 设备名称，instanceName */
    String getDeviceName();

    ChannelHandlerContext getChannelHandlerContext();

    String getSceneBy();

    void setRegistTime(long currentTimeMillis);

    void setSocketAddress(SocketAddress remoteAddress);

    void setNetworkType(NetworkType networkType);

    void setAction(int i);

    void setChannelHandlerContext(ChannelHandlerContext ctx);

    void setDevice(NetworkDevice device);

    NetworkDevice getDevice();

    void setMessageCodec(DeviceMessageCodec protocol);

    void setNetwork(Network network);
}
