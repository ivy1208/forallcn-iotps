package org.jeecg.config.jarslink;

import com.alipay.jarslink.api.Module;
import com.alipay.jarslink.api.ModuleConfig;
import com.alipay.jarslink.api.ModuleLoader;
import com.alipay.jarslink.api.ModuleManager;
import com.alipay.jarslink.api.impl.ModuleLoaderImpl;
import com.alipay.jarslink.api.impl.ModuleManagerImpl;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;

class JarsLinkUtilTest {
    @Autowired
    private ModuleLoader moduleLoader;
    @Autowired
    private ModuleManager moduleManager;

    @Test
    void load() {
        moduleLoader = new ModuleLoaderImpl();
        moduleManager = new ModuleManagerImpl();
        System.out.println(moduleLoader);

        // 加载模块jar包，可以在任意路径下
        String jarUrl = "demo-2.9-RELEASE.jar";
        URL modelUrl = Thread.currentThread().getContextClassLoader().getResource(jarUrl);
        System.out.println(modelUrl);

        ModuleConfig moduleConfig = new ModuleConfig();
        moduleConfig.setName("demo");
        moduleConfig.setEnabled(true);
        moduleConfig.setVersion("2.9");

        assert modelUrl != null;
        moduleConfig.setModuleUrl(ImmutableList.of(modelUrl));
        System.out.println(moduleConfig);

        Module module = moduleLoader.load(moduleConfig);
        moduleManager.register(module);
        String result = module.doAction("hello-world-action", "rabbit");

    }

}
