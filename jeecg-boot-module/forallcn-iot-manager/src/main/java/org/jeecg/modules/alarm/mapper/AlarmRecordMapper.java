package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.alarm.entity.AlarmRecord;

/**
 * @Description: 告警记录
 * @Author: zhouwr
 * @Date: 2021-01-19
 * @Version: V1.0
 */
public interface AlarmRecordMapper extends BaseMapper<AlarmRecord> {

}
