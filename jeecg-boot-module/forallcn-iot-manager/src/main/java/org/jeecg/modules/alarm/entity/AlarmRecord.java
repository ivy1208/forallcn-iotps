package org.jeecg.modules.alarm.entity;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import com.zhouwr.common.enums.AlarmLevel;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 告警记录
 * @Author: jeecg-boot
 * @Date: 2021-01-19
 * @Version: V1.0
 */
@Data
@TableName("iot_alarm_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "iot_alarm_record对象", description = "告警记录")
public class AlarmRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 上报数据
     */
    @Excel(name = "上报数据", width = 15)
    @ApiModelProperty(value = "上报数据")
    private String reportDataId;
    /**
     * 实例id
     */
    @Excel(name = "实例id", width = 15)
    @ApiModelProperty(value = "实例id")
    @Dict(dictTable = "iot_device_instance", dicText = "name", dicCode = "id")
    private java.lang.String instanceId;
    /**
     * 告警配置id
     */
    @Excel(name = "告警配置id", width = 15)
    @ApiModelProperty(value = "告警配置id")
    @Dict(dictTable = "iot_alarm_config", dicText = "name", dicCode = "id")
    private java.lang.String alarmConfigId;
    /**
     * 告警参数
     */
    @Excel(name = "告警参数", width = 15)
    @ApiModelProperty(value = "告警参数")
    private java.lang.String alarmParam;

    @TableField(exist = false)
    private JSONArray alarmParamMap;
    /**
     * 告警级别
     */
    @Excel(name = "告警级别", width = 15)
    @ApiModelProperty(value = "告警级别")
    private AlarmLevel alarmLevel;
    /**
     * 需要处理
     */
    @Excel(name = "需要处理", width = 15)
    @ApiModelProperty(value = "需要处理")
    private java.lang.Boolean isNeedDeal;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**
     * 处理情况
     */
    @Excel(name = "处理情况", width = 15)
    @ApiModelProperty(value = "处理情况")
    private java.lang.String dealContent;
}
