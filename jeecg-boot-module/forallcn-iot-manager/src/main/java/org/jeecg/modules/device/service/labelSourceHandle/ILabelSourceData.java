package org.jeecg.modules.device.service.labelSourceHandle;

import org.jeecg.modules.device.vo.label.ViewDataVo;

/**
 * ilabel源数据
 *
 * @author zhouwenrong
 * @date 2022/01/07
 */
public interface ILabelSourceData {

    /**
     * 获得价值
     *
     * @param instanceId 实例id
     * @return {@link String}
     * @throws ClassNotFoundException 类没有发现异常
     */
    String getValue(String modelId, String instanceId, ViewDataVo viewDataVo) throws ClassNotFoundException;
}
