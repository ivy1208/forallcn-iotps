package org.jeecg.modules.network.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import com.zhouwr.common.exception.IotNetworkServiceException;
import com.zhouwr.common.network.CommonConnectContext;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.jeecg.modules.network.entity.NetworkService;
import org.jeecg.modules.network.mapper.NetworkServiceMapper;
import org.jeecg.modules.network.network.NetworkConnectStore;
import org.jeecg.modules.network.service.INetworkServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description: 网络服务管理
 * @Author: jeecg-boot
 * @Date: 2020-04-12
 * @Version: V1.0
 */

@Slf4j
@Service
public class NetworkServiceServiceImpl extends ServiceImpl<NetworkServiceMapper, NetworkService> implements INetworkServiceService {

    @Autowired
    private IDeviceInstanceService instanceService;

    @Override
    public void debugByAddress(String address, String message) {
        Map<String, CommonConnectContext> connectMap = NetworkConnectStore.getNetworkConnectMap();
        CommonConnectContext connect = connectMap.get(address);
        if (connect == null) {
            throw new IotNetworkServiceException("网络连接[" + address + "]不存在！");
        }
        List<InstanceFunctionVo> functions = instanceService.buildFunctionStructures(connect.getDeviceId());
        ChannelHandlerContext ctx = connect.getChannelHandlerContext();
//        Object bytes = TcpSyncRequestUtil.get(ctx, CommonConstant.SYNC_RECEIVE, message, 1000 * 2);
//        log.info(" >>>>> 同步执行结果 >>>>> {}", bytes);
    }
}
