package org.jeecg.modules.scene.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.scene.entity.SceneScheme;
import org.jeecg.modules.scene.mapper.SceneSchemeMapper;
import org.jeecg.modules.scene.service.ISceneSchemeService;
import org.jeecg.modules.scene.vo.SchemeCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 场景方案
 * @Author: zhouwenrong
 * @Date: 2020-06-05
 * @Version: V1.0
 */
@Service
public class SceneSchemeServiceImpl extends ServiceImpl<SceneSchemeMapper, SceneScheme> implements ISceneSchemeService {
    @Autowired
    private SceneSchemeMapper sceneSchemeMapper;

    @Override
    public List<SchemeCountVo> schemeCountListBySceneId(String sceneId) {
        return sceneSchemeMapper.schemeCountListBySceneId(sceneId);
    }
}
