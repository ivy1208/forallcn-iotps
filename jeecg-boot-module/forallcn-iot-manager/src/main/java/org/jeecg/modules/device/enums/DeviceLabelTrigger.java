package org.jeecg.modules.device.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @Package: org.jeecg.modules.device.enums
 * @Descriprion：设备标签触发方式
 * @Author: zhouwr
 * @Date: 2021/12/8 12:01
 * @Version: V1.0
 */
@AllArgsConstructor
@Getter
public enum DeviceLabelTrigger implements IDictEnum<String> {
    /**
     *
     */
    ONLOAD("页面加载", "onload"),
    ONCLICK("鼠标点击", "onclick"),
    ONDBCLICK("鼠标双击", "ondbclick"),
    ONOVER("鼠标覆盖", "onover"),
    ONDATA("数据触发", "ondata");

    private final String text;
    @EnumValue
    private final String value;

    @JsonCreator
    public static DeviceLabelTrigger of(String code) {
        for (DeviceLabelTrigger trigger : DeviceLabelTrigger.values()) {
            if (trigger.value.equals(code)) {
                return trigger;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + code);
    }

    public static JSONArray toArray() {
        return Arrays.stream(DeviceLabelTrigger.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }
}