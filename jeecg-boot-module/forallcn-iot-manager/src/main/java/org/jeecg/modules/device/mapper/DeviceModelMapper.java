package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.device.entity.DeviceModel;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author zhouwenrong
 * @Description: 设备模型
 * @Date: 2020-04-08
 * @Version: V1.0
 */
public interface DeviceModelMapper extends BaseMapper<DeviceModel> {
    /**
     * 通过id获取属性
     *
     * @param modelId    模型id
     * @param column     列
     * @param dictTable  dict类型表
     * @param dictKey    dict关键
     * @param dictValue  东西价值
     * @param dateformat dateformat
     * @return {@link String}
     */
    String getAttributeById(@NotNull String modelId, @NotNull String column, String dictTable, String dictKey, String dictValue, String dateformat);

    /**
     * 查询模型功能配置
     * @param modelId 模型id
     * @return {@link ModelFunctionVo}
     */
    List<ModelFunctionVo> selectFunctionsByModelId(@NotNull @Param("modelId") String modelId);

    /**
     * 查询模型功能配置
     * @param functionId 功能id
     * @return {@link ModelFunctionVo}
     */
    ModelFunctionVo selectFunctionById(@NotNull @Param("functionId") String functionId);
}
