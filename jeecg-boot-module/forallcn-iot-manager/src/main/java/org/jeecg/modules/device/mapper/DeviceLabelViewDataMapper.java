package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.device.entity.DeviceLabelViewData;

import java.util.List;

/**
 * 设备标签显示数据映射器
 *
 * @author zhouwenrong
 * @Description: 设备标签
 * @Version: V1.0
 * @date 2022/01/06
 */
public interface DeviceLabelViewDataMapper extends BaseMapper<DeviceLabelViewData> {

    /**
     * 按标签id删除
     *
     * @param labelId 标签id
     * @return boolean
     */
    public boolean deleteByLabelId(@Param("mainId") String labelId);

    /**
     * 选择通过标签id
     *
     * @param labelId 标签id
     * @return {@link List}<{@link DeviceLabelViewData}>
     */
    public List<DeviceLabelViewData> selectByLabelId(@Param("labelId") String labelId);
}