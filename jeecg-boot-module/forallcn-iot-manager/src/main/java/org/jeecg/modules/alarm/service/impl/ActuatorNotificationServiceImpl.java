package org.jeecg.modules.alarm.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.alarm.entity.ActuatorNotification;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import org.jeecg.modules.alarm.entity.AlarmRecord;
import org.jeecg.modules.alarm.mapper.ActuatorNotificationMapper;
import org.jeecg.modules.alarm.service.IActuatorNotificationService;
import org.jeecg.modules.alarm.service.IAlarmConfigService;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.jeecg.modules.network.service.INetworkServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: forallcn-iotps
 * @description: IActuatorNotificationService
 * @author: zhouwr
 * @create: 2021-03-15 21:37
 * @version：1.0
 **/
@Service
@Slf4j
public class ActuatorNotificationServiceImpl extends ServiceImpl<ActuatorNotificationMapper, ActuatorNotification> implements IActuatorNotificationService {

    @Autowired
    private IActuatorNotificationService notificationService;
    @Autowired
    private IAlarmConfigService alarmConfigService;
    @Autowired
    private IDeviceInstanceService instanceService;
    @Autowired
    private INetworkServiceService networkService;

    @Autowired
    private ISysBaseAPI sysBaseAPI;

    public Map<String, String> alarmRecordIntoMap(AlarmRecord alarmRecord) {
        Map<String, String> msgMap = BeanUtil.beanToMap(alarmRecord)
                .entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> {
                    if (entry.getValue() instanceof Date) {
                        return DateUtils.formatDate(new Date(entry.getValue().toString()), "yyyy-MM-dd HH:mm:ss");
                    } else {
                        return entry.getValue().toString();
                    }
                }));
        final JSONObject alarmParam = (JSONObject) JSONObject.parse(alarmRecord.getAlarmParam());
        msgMap.put("ruleExpre", alarmParam.getString("ruleExpre"));
        msgMap.put("alarmValue", alarmParam.getString(""));

        final DeviceInstance instance = instanceService.getById(alarmRecord.getInstanceId());
        if (instance != null) {
            msgMap.put("instanceName", instance.getName());
        }

        final AlarmConfig alarmConfig = alarmConfigService.getById(alarmRecord.getAlarmConfigId());

        msgMap.put("alarmName", alarmConfig.getName());
        msgMap.put("alarmLevelValue", alarmConfig.getAlarmLevel().getValue().toString());
        msgMap.put("alarmLevelText", alarmConfig.getAlarmLevel().getText());
        msgMap.put("alarmLevelColor", alarmConfig.getAlarmLevel().getColor());
        return msgMap;

    }
}
