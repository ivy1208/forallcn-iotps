package org.jeecg.modules.alarm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.StartStopState;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import com.zhouwr.common.enums.AlarmLevel;
import org.jeecg.modules.alarm.enums.TriggerMethod;

import java.util.*;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-15 11:53
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AlarmConfigVo {

    private String id;
    /**
     * 告警配置名称
     */
    private String name;
    /**
     * 设备实例id
     */
    private String instanceId;
    /**
     * 设备实例名称
     */
    private String instanceName;
    /**
     * 触发方式
     */
    private TriggerMethod triggerMethod;
    /**
     * 告警等级
     */
    private AlarmLevel alarmLevel;
    /**
     * 是否处理
     */
    private Boolean isDeal;
    /**
     * 状态
     */
    private StartStopState state;
    /**
     * 定时触发时的定时表达式
     */
    private String triggerCorn;
    /**
     * 触发器
     */
    private List<AlarmConfigTriggerVo> triggers = new ArrayList<>();
    /**
     * 执行器
     */
    private List<AlarmConfigActuatorVo> actuators = new ArrayList<>();

    private String createBy;
    private Date createTime;
    private String description;


    public AlarmConfigVo(AlarmConfig alarmConfig) {

        this.id = alarmConfig.getId();
        this.name = alarmConfig.getName();
        this.instanceId = alarmConfig.getInstanceId();
        this.triggerMethod = alarmConfig.getTriggerMethod();
        this.triggerCorn = alarmConfig.getCorn();
        this.isDeal = alarmConfig.getIsDeal();
        this.alarmLevel = alarmConfig.getAlarmLevel();
        this.state = alarmConfig.getState();
        this.createTime = alarmConfig.getCreateTime();
        this.createBy = alarmConfig.getCreateBy();
        this.description = alarmConfig.getDescription();
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("alarmId", this.id);
        map.put("alarmName", this.name);
        map.put("alarmInstanceId", this.instanceId);
        map.put("alarmInstanceName", this.instanceName);
        map.put("triggerMethodText", this.triggerMethod.getText());
        map.put("triggerMethodValue", this.triggerMethod.getValue());
        map.put("triggerCorn", this.triggerCorn);
        map.put("alarmIsDeal", this.isDeal.toString());
        map.put("alarmLevelText", this.alarmLevel.getText());
        map.put("alarmLevelValue", this.alarmLevel.getValue().toString());
        map.put("alarmLevelColor", this.alarmLevel.getColor());
        map.put("alarmStateText", this.state.getText());
        map.put("alarmStateValue", this.state.getValue().toString());
        map.put("alarmCreateTime", DateUtils.formatDate(this.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
        map.put("alarmCreateBy", this.createBy);

        triggers.forEach(triggerVo -> {
            map.put("triggerId", triggerVo.getId());
            map.put("triggerName", triggerVo.getName());
            map.put("triggerFilerExpre", triggerVo.getFilterExpre());
            map.put("triggerModelDataId", triggerVo.getModelDataId());
            map.put("triggerSourceText", triggerVo.getSource().getText());
            map.put("triggerSourceValue", triggerVo.getSource().getValue());
        });

        actuators.forEach(actuatorVo -> {
            map.put("actuatorId", actuatorVo.getId());
            map.put("actuatorName", actuatorVo.getName());
            map.put("actuatorType", actuatorVo.getActuatorType().toString());
            map.put("actuatorDataForward", actuatorVo.getDataForward() == null ? null : actuatorVo.getDataForward().toString());
            map.put("actuatorInvokerFunction", actuatorVo.getInvokeFunction() == null ? null : actuatorVo.getInvokeFunction().toString());
            map.put("actuatorNotification", actuatorVo.getNotification() == null ? null : actuatorVo.getNotification().toString());
        });

        return map;
    }
}
