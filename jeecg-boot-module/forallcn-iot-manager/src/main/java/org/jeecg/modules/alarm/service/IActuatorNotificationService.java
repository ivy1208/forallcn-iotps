package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.alarm.entity.ActuatorNotification;

/**
 * @author zhouwenrong
 */
public interface IActuatorNotificationService extends IService<ActuatorNotification> {

}
