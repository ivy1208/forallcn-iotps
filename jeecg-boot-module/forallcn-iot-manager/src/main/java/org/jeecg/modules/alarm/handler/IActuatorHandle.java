package org.jeecg.modules.alarm.handler;

import java.util.Map;

public interface IActuatorHandle {
    boolean invoke(String actuatorId, Map<String, Object> dataMap);
}
