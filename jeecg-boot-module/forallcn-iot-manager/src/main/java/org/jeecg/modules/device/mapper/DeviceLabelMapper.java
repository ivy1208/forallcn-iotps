package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.device.entity.DeviceLabel;

import java.util.List;

/**
 * @Description: 设备标签
 * @Author: jeecg-boot
 * @Date: 2021-12-07
 * @Version: V1.0
 */
public interface DeviceLabelMapper extends BaseMapper<DeviceLabel> {
    public boolean deleteByMainId(@Param("mainId") String mainId);

    public List<DeviceLabel> selectByMainId(String id);

    /**
     * 根据设备实例id，获取设备的数据节点
     *
     * @param instanceId
     * @return
     */
    public List<DeviceLabel> selectByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 根据实例id、数据标签code，获取数据标签
     *
     * @param instanceId
     * @param labelCode
     * @return
     */
    @Select("SELECT d.* FROM iot_device_label b, iot_device_model m, iot_device_instance i " +
            "WHERE 1 = 1 AND i.id=#{instanceId} AND b.code=#{labelCode} AND i.model_by=m.id AND b.device_model_by=m.id")
    public List<DeviceLabel> select(@Param("instanceId") String instanceId, @Param("labelCode") String labelCode);

    /**
     * 获取表中某一条数据的属性值
     *
     * @param table  表名
     * @param id     表主键id
     * @param column 列名
     * @return
     */
    // @Select("SELECT t.${column} FROM ${table} t WHERE t.id = #{id}")
    public Object getAttributeById(@Param("table") String table, @Param("id") String id, @Param("column") String column);

//    @Select("SELECT count(0) FROM iot_device_label WHERE default_trigger=#{defaultTrigger} GROUP BY device_model_by")
public Integer validateDuplicateDefaultTrigger(@Param("id") String id, @Param("defaultTrigger") String defaultTrigger);
}