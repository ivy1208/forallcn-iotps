package org.jeecg.modules.alarm.handler;

import org.jeecg.modules.alarm.enums.AlarmActuatorType;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-06-17 09:17
 * @version：1.0
 **/
public class ActuatorContext {
    public static IActuatorHandle getInstance(AlarmActuatorType actuatorType) {
        IActuatorHandle actuatorHandle = null;
        try {
            if (actuatorType != null) {
                actuatorHandle = (IActuatorHandle) Class.forName(actuatorType.getActuatorClass()).newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return actuatorHandle;
    }
}
