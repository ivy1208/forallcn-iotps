package org.jeecg.modules.device.service.labelSourceHandle;

import com.zhouwr.common.device.vo.InstanceDataStructure;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.jeecg.modules.device.vo.label.ViewDataVo;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class LabelPropertyDataImpl implements ILabelSourceData {
    @Autowired
    private IDeviceInstanceService instanceService;

    public LabelPropertyDataImpl() {
        this.instanceService = SpringContextUtils.getBean(IDeviceInstanceService.class);
    }

    @Override
    public String getValue(String modelId, String instanceId, ViewDataVo viewDataVo) throws ClassNotFoundException {
        Object value = instanceService.listInstanceDataLatest(instanceId).stream()
                .filter(instanceData -> instanceData.getCode().equals(viewDataVo.getKey()))
                .findFirst()
                .orElse(new InstanceDataStructure())
                .getLatestData().getValue();
        return String.valueOf(value);
    }
}
