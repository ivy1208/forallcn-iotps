package org.jeecg.modules.device.vo.label;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 标签数据转换
 *
 * @author zhouwenrong
 * @date 2022/01/06
 */
@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ConvertVo {
    /**
     * 表格
     */
    private String dictTable = "";
    /**
     * 关键
     */
    private String dictKey = "";
    /**
     * 价值
     */
    private String dictValue = "";
}
