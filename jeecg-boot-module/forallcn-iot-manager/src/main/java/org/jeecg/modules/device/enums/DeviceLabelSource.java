package org.jeecg.modules.device.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;
import org.jeecg.modules.device.service.labelSourceHandle.ILabelSourceData;
import org.jeecg.modules.device.service.labelSourceHandle.LabelInstanceDataImpl;
import org.jeecg.modules.device.service.labelSourceHandle.LabelModelDataImpl;
import org.jeecg.modules.device.service.labelSourceHandle.LabelPropertyDataImpl;

/**
 * 设备标签数据来源
 *
 * @author zhouwenrong
 * @date 2022/01/06
 */
@Getter
public enum DeviceLabelSource implements IDictEnum<String> {
    /**
     * 设备模型
     */
    MODEL("model", "设备模型", LabelModelDataImpl.class),
    /**
     * 设备实例
     */
    INSTANCE("instance", "设备实例", LabelInstanceDataImpl.class),
    /**
     * 设备属性
     */
    PROPERTY("property", "设备属性", LabelPropertyDataImpl.class),
    ;

    /**
     * 值
     */
    @EnumValue
    private final String value;
    /**
     * 文本
     */
    private final String text;
    /**
     * 源处理
     */
    private final Class<? extends ILabelSourceData> sourceHandle;

    /**
     * 设备标签来源
     *
     * @param value        价值
     * @param text         文本
     * @param sourceHandle 源处理
     */
    DeviceLabelSource(String value, String text, Class<? extends ILabelSourceData> sourceHandle) {
        this.value = value;
        this.text = text;
        this.sourceHandle = sourceHandle;
    }

    /**
     * @param val 值
     * @return {@link DeviceLabelSource}
     */
    @JsonCreator
    public static DeviceLabelSource of(String val) {
        for (DeviceLabelSource source : values()) {
            if (source.value.equals(val)) {
                return source;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + val);
    }
}
