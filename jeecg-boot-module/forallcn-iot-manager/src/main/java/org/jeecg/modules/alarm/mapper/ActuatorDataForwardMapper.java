package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.alarm.entity.ActuatorDataForward;

/**
 * @author zhouwenrong
 */
public interface ActuatorDataForwardMapper extends BaseMapper<ActuatorDataForward> {
}
