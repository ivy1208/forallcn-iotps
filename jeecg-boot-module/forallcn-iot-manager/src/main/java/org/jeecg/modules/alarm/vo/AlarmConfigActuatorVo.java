package org.jeecg.modules.alarm.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jeecg.modules.alarm.enums.AlarmActuatorType;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-15 11:59
 * @version：1.0
 **/
@NoArgsConstructor
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@TableName("iot_alarm_config_actuator")
public class AlarmConfigActuatorVo {
    private String id;
    /**
     * 执行器名称
     */
    private String name;
    /**
     * 执行类型
     */
    private AlarmActuatorType actuatorType;
    /**
     * 消息通知
     */
    private ActuatorNotificationVo notification;
    /**
     * 执行功能
     */
    private ActuatorInvokeFunctionVo invokeFunction;
    /**
     * 数据转发
     */
    private ActuatorDataForwardVo dataForward;
}
