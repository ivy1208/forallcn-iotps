package org.jeecg.modules.protocol.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 协议包类型
 * @author zhouwenrong
 */
@Getter
@AllArgsConstructor
public enum ProtocolPackageType implements IDictEnum<String> {
    /**
     *
     */
    JAR("jar", "Jar包"),
    CLASS("class", "类"),
    SCRIPT("script", "script脚本");

    @EnumValue
    private final String value;
    private final String text;

    @JsonCreator
    public static ProtocolPackageType of(String value) {
        for (ProtocolPackageType codecType : ProtocolPackageType.values()) {
            if (codecType.value.equals(value)) {
                return codecType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(ProtocolPackageType.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }
}
