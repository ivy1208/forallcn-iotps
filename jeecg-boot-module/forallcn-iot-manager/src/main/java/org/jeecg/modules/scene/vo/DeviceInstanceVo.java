package org.jeecg.modules.scene.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.entity.DeviceInstance;
import com.zhouwr.common.enums.DeviceInstanceState;
import org.jeecg.modules.scene.entity.SceneScheme;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description: 设备实例
 * @Author: jeecg-boot
 * @Date: 2020-04-11
 * @Version: V1.0
 */
@Slf4j
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@NoArgsConstructor
@AllArgsConstructor
public class DeviceInstanceVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String code;
    private String name;
    private String scheme;
    private DeviceInstanceState status;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Long statusUpdateTime;
    private String createUser;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String description;

    private List<com.zhouwr.common.device.vo.InstanceExtentParams> extentParams;

    public DeviceInstanceVo(DeviceInstance instance, SceneScheme scheme, String createRealname) {
        this.id = instance.getId();
        this.code = instance.getCode();
        this.name = instance.getName();
        this.scheme = scheme.getName();
        this.status = instance.getStatus();
        this.createUser = createRealname;
        this.createTime = instance.getCreateTime();
        this.description = instance.getDescription();
        // 状态维持时间
        if (instance.getStatusUpdateTime() != null) {
            this.statusUpdateTime = System.currentTimeMillis() - instance.getStatusUpdateTime().getTime();
        }
    }

    public DeviceInstanceVo(DeviceInstance instance, SceneScheme scheme, List<com.zhouwr.common.device.vo.InstanceExtentParams> instanceExtentParams, String createRealname) {
        this(instance, scheme, createRealname);
        this.extentParams = instanceExtentParams;
    }
}
