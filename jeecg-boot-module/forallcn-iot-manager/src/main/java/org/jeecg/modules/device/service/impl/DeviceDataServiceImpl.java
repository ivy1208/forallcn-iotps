package org.jeecg.modules.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import org.jeecg.modules.device.entity.DeviceData;
import org.jeecg.modules.device.mapper.DeviceDataMapper;
import org.jeecg.modules.device.service.IDeviceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 数据节点
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
@Service
public class DeviceDataServiceImpl extends ServiceImpl<DeviceDataMapper, DeviceData> implements IDeviceDataService {

    @Autowired
    private DeviceDataMapper deviceDataMapper;

    @Override
    public DeviceData getByCode(String code) {

        final List<DeviceData> dataList = this.lambdaQuery().eq(DeviceData::getCode, code).list();
        if(dataList.size() > 1) {
            throw new RuntimeException("数据节点的code："+code+"不唯一，请检查配置！");
        } else if(dataList.size() < 1){
            throw new RuntimeException("数据节点的code："+code+"找不到，请检查配置！");
        }
        return dataList.get(0);
    }


    @Override
    public List<DeviceData> selectByMainId(String mainId) {
        return deviceDataMapper.selectByMainId(mainId);
    }

    @Override
    public List<DeviceData> listByModelId(String modelId) {
        return deviceDataMapper.selectByMainId(modelId);
    }

    @Override
    public List<DeviceData> listByInstanceId(String instanceId) {
        return deviceDataMapper.selectByInstanceId(instanceId);
    }

    @Override
    public InstanceDataStructure getLastDataStructure(String instanceId, String propertyCode) {
        return deviceDataMapper.selectLastDataStructure(instanceId, propertyCode);
    }

    @Override
    public List<InstanceDataStructure> getLastDataStructures(String instanceId) {
        return deviceDataMapper.selectLastDataStructure(instanceId);
    }
}
