package org.jeecg.modules.device.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.enums.ReportMode;
import org.jeecg.modules.device.entity.DataReport;
import org.jeecg.modules.device.vo.DataReportCountVo;

import java.util.List;
import java.util.Map;

/**
 * @Description: 设备上传数据
 * @Author: jeecg-boot
 * @Date: 2020-05-14
 * @Version: V1.0
 */
public interface IDataReportService extends IService<DataReport> {

    /**
     * 保存设备数据
     *
     * @param instanceId
     * @param data
     * @param reportMode
     * @return
     */
    String saveData(String instanceId, Map<String, Object> data, ReportMode reportMode);

    /**
     * 校验并构建数据节点的数据
     *
     * @param deviceInstanceId
     * @param dataMap
     * @return
     */
    JSONObject verifyBuildDataNode(String deviceInstanceId, Map<String, Object> dataMap);

    /**
     * 获取上报数据
     * @param instanceId
     * @param dataCode
     * @return
     */
    DataReport getReportDataLast(String instanceId, String dataCode);

    /**
     * 获取上报数据统计
     *
     * @param sceneId
     * @return
     */
    List<DataReportCountVo> getReportDataCount(String sceneId);

    List<DataReportCountVo> getReportDataCount(String sceneId, String sceneSchemeIs);

    void reportExtents(IPage<DataReport> pageList);
}
