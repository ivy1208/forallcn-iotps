package org.jeecg.modules.scene.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-11-13 12:58
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceCountVo {
    private String modelId;
    private String modelName;
    private String modelCode;
    private Integer deviceCount;
}
