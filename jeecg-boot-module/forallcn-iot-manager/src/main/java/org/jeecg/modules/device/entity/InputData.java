package org.jeecg.modules.device.entity;

import com.zhouwr.common.enums.FunctionParamInputMode;
import lombok.Data;

@Data
public class InputData {

    /**
     * 参数集合
     */
    private DeviceData deviceData;

    /**
     * 输入方式
     */
    private FunctionParamInputMode inputMode;
}
