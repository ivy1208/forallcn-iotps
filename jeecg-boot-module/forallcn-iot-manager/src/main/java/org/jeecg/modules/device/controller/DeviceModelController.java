package org.jeecg.modules.device.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import com.zhouwr.common.enums.DataRwAuthor;
import com.zhouwr.common.enums.FunctionParamInputMode;
import com.zhouwr.common.enums.ModelDataType;
import com.zhouwr.common.metadata.unit.UnifyUnit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.CommonUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.device.entity.*;
import org.jeecg.modules.device.enums.DeviceType;
import org.jeecg.modules.device.enums.FunctionExecWay;
import org.jeecg.modules.device.enums.FunctionType;
import org.jeecg.modules.device.service.*;
import org.jeecg.modules.device.vo.DeviceModelPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 设备模型
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
@Api(tags = "设备模型")
@RestController
@RequestMapping("/device/model")
@Slf4j
public class DeviceModelController {
    @Autowired
    private IDeviceModelService modelService;
    @Autowired
    private IDeviceDataService dataService;
    @Autowired
    private IDeviceFunctionService functionService;
    @Autowired
    private IDeviceInstanceParamService instanceParamService;
    @Autowired
    private IDeviceEventService eventService;
    @Autowired
    private IDeviceLabelService labelService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Value(value = "${jeecg.path.upload}")
    private String uploadpath;

    /**
     * 分页列表查询
     *
     * @param model
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "设备模型-分页列表查询")
    @ApiOperation(value = "设备模型-分页列表查询", notes = "设备模型-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(DeviceModel model,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<DeviceModel> queryWrapper = QueryGenerator.initQueryWrapper(model, req.getParameterMap());
        Page<DeviceModel> page = new Page<DeviceModel>(pageNo, pageSize);
        log.info("{}", queryWrapper.getSqlComment());
        IPage<DeviceModel> pageList = modelService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    @AutoLog(value = "设备模型-查询所有")
    @ApiOperation(value = "设备模型-查询所有", notes = "设备模型-查询所有")
    @GetMapping(value = "/listAll")
    public Result<?> listAll() {
        return Result.ok(this.modelService.lambdaQuery().list());
    }

    @ApiOperation(value = "获取数据单位")
    @GetMapping(value = "/getDataUnits")
    public Result<?> getDataUnits() {
        return Result.ok(UnifyUnit.toArray());
    }

    @ApiOperation(value = "设备模型-获取功能类型枚举列表")
    @GetMapping(value = "/getFunctionTypes")
    public Result<?> getFunctionTypes() {
        return Result.ok(FunctionType.toArray());
    }

    @ApiOperation(value = "设备模型-获取执行方式枚举列表")
    @GetMapping(value = "/getExecutionWays")
    public Result<?> getExecutionWays() {
        return Result.ok(FunctionExecWay.toArray());
    }

    @ApiOperation(value = "设备模型-获取输入模式枚举列表")
    @GetMapping(value = "/getInputModes")
    public Result<?> getInputModes() {
        return Result.ok(FunctionParamInputMode.toArray());
    }

    @ApiOperation(value = "设备模型-获取数据类型枚举列表")
    @GetMapping(value = "/getDataTypes")
    public Result<?> getDataTypes() {
        return Result.ok(ModelDataType.toArray());
    }

    @ApiOperation(value = "设备模型-获取数据读写权限枚举列表")
    @GetMapping(value = "/getRwAuthor")
    public Result<?> getRwAuthor() {
        return Result.ok(DataRwAuthor.toArray());
    }

    @ApiOperation(value = "获取可写设备属性", notes = "根据设备模型、数据类型条件搜索")
    @GetMapping(value = "/getWritableDeviceProperties")
    public Result<?> getWritableDeviceProperties(@RequestParam(name = "deviceModelId") String deviceModelId,
                                   @RequestParam(name = "type", required = false) String type) {
        final List<DeviceData> list = dataService.lambdaQuery()
                .eq(DeviceData::getDeviceModelBy, deviceModelId)
                .eq(StringUtils.isNotBlank(type), DeviceData::getType, type)
                .ne(DeviceData::getRwAuthor, "r")
                .list();
        return Result.ok(list);
    }

    /**
     * @return
     * @功能：刷新缓存
     */
    @RequestMapping(value = "/refreshCache")
    public Result<?> refreshCache() {
        //清空缓存
        Set<String> keys1 = redisTemplate.keys(CacheConstant.CACHE_BUILD_FUNC_DATA_STRUCTURE + "*");
        Set<String> keys2 = redisTemplate.keys(CacheConstant.CACHE_DEVICE_INSTANCE_DATA_NODES + "*");

        redisTemplate.delete(keys1);
        redisTemplate.delete(keys2);
        return Result.ok("刷新缓存成功！");
    }

    @AutoLog(value = "设备模型-检测功能标识")
    @ApiOperation(value = "设备模型-检测功能标识", notes = "设备模型-检测功能标识")
    @GetMapping(value = "/validateDataCode")
    public Result<?> validateDataCode(@RequestParam(name = "deviceModelId") String deviceModelId,
                                      @RequestParam(name = "dataCode") String dataCode,
                                      @RequestParam(name = "dataId", defaultValue = "") String dataId) {
        List<DeviceData> list = dataService.lambdaQuery()
                .eq(DeviceData::getDeviceModelBy, deviceModelId)
                .eq(DeviceData::getCode, dataCode)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(dataId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("功能标识重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    @AutoLog(value = "设备模型-检测功能名称")
    @ApiOperation(value = "设备模型-检测功能名称", notes = "设备模型-检测功能名称")
    @GetMapping(value = "/validateDataName")
    public Result<?> validateDataName(@RequestParam(name = "deviceModelId") String deviceModelId,
                                      @RequestParam(name = "dataName") String dataName,
                                      @RequestParam(name = "dataId", defaultValue = "") String dataId) {
        List<DeviceData> list = dataService.lambdaQuery()
                .eq(DeviceData::getDeviceModelBy, deviceModelId)
                .eq(DeviceData::getName, dataName)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(dataId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("功能名称重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    @AutoLog(value = "设备模型-检测功能标识")
    @ApiOperation(value = "设备模型-检测功能标识", notes = "设备模型-检测功能标识")
    @GetMapping(value = "/validateFuncCode")
    public Result<?> validateFuncCode(@RequestParam(name = "deviceModelId") String deviceModelId,
                                      @RequestParam(name = "funcCode") String funcCode,
                                      @RequestParam(name = "funcId", defaultValue = "") String funcId) {
        List<DeviceFunction> list = functionService.lambdaQuery()
                .eq(DeviceFunction::getDeviceModelBy, deviceModelId)
                .eq(DeviceFunction::getCode, funcCode)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(funcId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("功能标识重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    @AutoLog(value = "设备模型-检测功能名称")
    @ApiOperation(value = "设备模型-检测功能名称", notes = "设备模型-检测功能名称")
    @GetMapping(value = "/validateFuncName")
    public Result<?> validateFuncName(@RequestParam(name = "deviceModelId") String deviceModelId,
                                      @RequestParam(name = "funcName") String funcName,
                                      @RequestParam(name = "funcId", defaultValue = "") String funcId) {
        List<DeviceFunction> list = functionService.lambdaQuery()
                .eq(DeviceFunction::getDeviceModelBy, deviceModelId)
                .eq(DeviceFunction::getName, funcName)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(funcId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("功能名称重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    @AutoLog(value = "设备模型-校验事件标识")
    @ApiOperation(value = "设备模型-校验事件标识", notes = "设备模型-校验事件标识")
    @GetMapping(value = "/validateEventCode")
    public Result<?> validateEventCode(@RequestParam(name = "deviceModelId") String deviceModelId,
                                       @RequestParam(name = "eventCode") String eventCode,
                                       @RequestParam(name = "eventId", defaultValue = "") String eventId) {
        List<DeviceEvent> list = eventService.lambdaQuery()
                .eq(DeviceEvent::getDeviceModelBy, deviceModelId)
                .eq(DeviceEvent::getCode, eventCode)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(eventId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("事件标识重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    @AutoLog(value = "设备模型-校验事件名称")
    @ApiOperation(value = "设备模型-校验事件名称", notes = "设备模型-校验事件名称")
    @GetMapping(value = "/validateEventName")
    public Result<?> validateEventName(@RequestParam(name = "deviceModelId") String deviceModelId,
                                       @RequestParam(name = "eventName") String eventName,
                                       @RequestParam(name = "eventId", defaultValue = "") String eventId) {
        List<DeviceEvent> list = eventService.lambdaQuery()
                .eq(DeviceEvent::getDeviceModelBy, deviceModelId)
                .eq(DeviceEvent::getName, eventName)
                .list();
        if (list.size() > 0) {
            if (list.size() == 1 && list.get(0).getId().equals(eventId)) {
                return Result.ok("校验通过！");
            } else {
                return Result.error("事件名称重复");
            }
        } else {
            return Result.ok("校验通过！");
        }
    }

    /**
     * 添加
     *
     * @param model
     * @return
     */
    @AutoLog(value = "设备模型-添加")
    @ApiOperation(value = "设备模型-添加", notes = "设备模型-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody DeviceModel model) {
        modelService.save(model);
        return Result.ok("添加成功！");
    }

    @AutoLog(value = "设备模型-克隆")
    @ApiOperation(value = "设备模型-克隆", notes = "设备模型-克隆")
    @GetMapping(value = "/clone")
    public Result<?> clone(@RequestParam String id) {
        modelService.clone(id);
        return Result.ok("克隆成功！");
    }

    @AutoLog(value = "设备模型-数据节点-添加")
    @ApiOperation(value = "设备模型-数据节点-添加", notes = "设备模型-数据节点-添加")
    @PostMapping(value = "/addData")
    public Result<?> addData(@RequestBody DeviceData deviceData) {
        dataService.save(deviceData);
        return Result.ok("添加成功！");
    }


    @AutoLog(value = "设备模型-功能定义-添加")
    @ApiOperation(value = "设备模型-功能定义-添加", notes = "设备模型-功能定义-添加")
    @PostMapping(value = "/addFunc")
    public Result<?> addFunc(@RequestBody ModelFunctionVo modelFunctionVo) {
        // 1、输入参数防重复验证
        Boolean b = functionService.functionParamCheckUnique(modelFunctionVo);
        if (!b) {
            return Result.error("输入参数重复！");
        }
        // 2、保存
        functionService.saveWithParams(modelFunctionVo);
        return Result.ok("添加成功！");
    }

    @AutoLog(value = "设备模型-事件定义-添加")
    @ApiOperation(value = "设备模型-事件定义-添加", notes = "设备模型-事件定义-添加")
    @PostMapping(value = "/addEvent")
    public Result<?> addEvent(@RequestBody DeviceEvent deviceEvent) {
        eventService.save(deviceEvent);
        return Result.ok("添加成功！");
    }

    @AutoLog(value = "设备模型-标签定义-添加")
    @ApiOperation(value = "设备模型-标签定义-添加", notes = "设备模型-标签定义-添加")
    @PostMapping(value = "/addLabel")
    public Result<?> addLabel(@RequestBody DeviceLabel deviceLabel) {
        labelService.save(deviceLabel);
        return Result.ok("添加成功！");
    }

    @AutoLog(value = "设备模型-数据节点-更新")
    @ApiOperation(value = "设备模型-数据节点-更新", notes = "设备模型-数据节点-更新")
    @PutMapping(value = "/editData")
    public Result<?> editData(@RequestBody DeviceData deviceData) {
        dataService.saveOrUpdate(deviceData);
        return Result.ok("更新成功！");
    }

    @AutoLog(value = "设备模型-功能定义-更新")
    @ApiOperation(value = "设备模型-功能定义-更新", notes = "设备模型-功能定义-更新")
    @PutMapping(value = "/editFunc")
    public Result<?> editFunc(@RequestBody ModelFunctionVo modelFunctionVo) {
        functionService.saveOrUpdateWithParams(modelFunctionVo);
        return Result.ok("更新成功！");
    }

    @AutoLog(value = "设备模型-事件定义-更新")
    @ApiOperation(value = "设备模型-事件定义-更新", notes = "设备模型-事件定义-更新")
    @PutMapping(value = "/editEvent")
    public Result<?> editEvent(@RequestBody DeviceEvent deviceEvent) {
        eventService.saveOrUpdate(deviceEvent);
        return Result.ok("更新成功！");
    }

    @AutoLog(value = "设备模型-标签定义-更新")
    @ApiOperation(value = "设备模型-标签定义-更新", notes = "设备模型-标签定义-更新")
    @PutMapping(value = "/editLabel")
    public Result<?> editLabel(@RequestBody DeviceLabel deviceLabel) {
        labelService.saveOrUpdate(deviceLabel);
        return Result.ok("更新成功！");
    }

    /**
     * 获取属性子集
     *
     * @param id    属性的id
     * @param aName 属性类型
     * @return
     */
    @AutoLog(value = "设备模型-根据id，获取数据节点-添加")
    @ApiOperation(value = "设备模型-根据id，获取数据节点-添加", notes = "设备模型-根据id，获取数据节点-添加")
    @GetMapping(value = "/getAttributeById")
    public Result<?> getAttributeById(
            @RequestParam(name = "id", required = true) String id,
            @RequestParam(name = "aName", required = true) String aName
    ) {
        if ("data".equals(aName)) {
            return Result.ok(dataService.getById(id));
        } else if ("func".equals(aName)) {
            return Result.ok(modelService.getFunctionParam(id));
        } else if ("event".equals(aName)) {
            return Result.ok(eventService.getById(id));
        } else if ("label".equals(aName)) {
            return Result.ok(labelService.getById(id));
        } else {
            return Result.error("获取子集失败");

        }
    }

    @AutoLog(value = "设备模型-根据id，获取数据节点-添加")
    @ApiOperation(value = "设备模型-根据id，获取数据节点-添加", notes = "设备模型-根据id，获取数据节点-添加")
    @GetMapping(value = "/listDeviceType")
    public Result<?> listDeviceType() {
        return Result.ok(DeviceType.toArray());
    }

    /**
     * 编辑
     *
     * @param model
     * @return
     */
    @AutoLog(value = "设备模型-编辑")
    @ApiOperation(value = "设备模型-编辑", notes = "设备模型-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody DeviceModel model) {
        if (model.getId() != null) {
            modelService.updateById(model);
        }
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备模型-通过id删除")
    @ApiOperation(value = "设备模型-通过id删除", notes = "设备模型-通过id删除")
    @DeleteMapping(value = "/delete")
    @CacheEvict(value = CacheConstant.CACHE_DEVICE_INSTANCE_DATA_NODES, allEntries = true)
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {

        DeviceModel model = modelService.getById(id);
        if (model == null) {
            return Result.error("安全公告" + id + "不存在!");
        }
        if (oConvertUtils.isNotEmpty(model.getThreeModelFile())) {
            Arrays.stream(model.getThreeModelFile().split(",")).forEach(path -> {
                boolean delFlag = CommonUtils.deleteFile(uploadpath + File.separatorChar + model.getThreeModelFile());
                log.info("删除文件 {} {}", path, delFlag);
            });
        }

        modelService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "设备模型-批量删除")
    @ApiOperation(value = "设备模型-批量删除", notes = "设备模型-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    @CacheEvict(value = CacheConstant.CACHE_DEVICE_INSTANCE_DATA_NODES, allEntries = true)
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {

        modelService.lambdaQuery().in(DeviceModel::getId, (Object) ids.split(",")).list().forEach(model -> {
            if (oConvertUtils.isNotEmpty(model.getThreeModelFile())) {
                Arrays.stream(model.getThreeModelFile().split(",")).forEach(path -> {
                    boolean delFlag = CommonUtils.deleteFile(uploadpath + File.separatorChar + model.getThreeModelFile());
                    log.info("删除文件 {} {}", path, delFlag);
                });
            }
        });

        this.modelService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功！");
    }

    @DeleteMapping(value = "/data/deleteBatch")
    public Result<?> dataDeleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.dataService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功！");
    }

    @DeleteMapping(value = "/func/deleteBatch")
    public Result<?> funcDeleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        // 查询设备实例参数，是否有功能引用
        List<DeviceInstanceParam> instanceParams = instanceParamService.queryByFunctionIds(Arrays.asList(ids.split(",")));
        if(instanceParams != null) {
            return Result.ok("功能实例有调用，不能删除！");
        }
        this.functionService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功！");
    }

    @DeleteMapping(value = "/event/deleteBatch")
    public Result<?> eventDeleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.eventService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功！");
    }

    @DeleteMapping(value = "/label/deleteBatch")
    public Result<?> labelDeleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.labelService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功！");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备模型-通过id查询")
    @ApiOperation(value = "设备模型-通过id查询", notes = "设备模型-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        DeviceModel model = modelService.getById(id);
        if (model == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(model);

    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "数据节点集合-通过id查询")
    @ApiOperation(value = "数据节点集合-通过id查询", notes = "数据节点-通过id查询")
    @GetMapping(value = "/queryDataByModelId")
    public Result<?> queryDataByModelId(@RequestParam(name = "id", required = true) String id) {
        List<DeviceData> deviceDataList = dataService.selectByMainId(id);
        return Result.ok(deviceDataList);
    }

    @AutoLog(value = "数据节点集合-通过instanceId查询")
    @ApiOperation(value = "数据节点集合-通过instanceId查询", notes = "数据节点-通过instanceId查询")
    @GetMapping(value = "/queryDataByInstanceId")
    public Result<?> queryDataByInstanceId(@RequestParam(name = "instanceId", required = true) String instanceId) {
        List<DeviceData> deviceDataList = dataService.listByInstanceId(instanceId);
        return Result.ok(deviceDataList);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "功能定义集合-通过id查询")
    @ApiOperation(value = "功能定义集合-通过id查询", notes = "功能定义-通过id查询")
    @GetMapping(value = "/queryFunctionByModelId")
    public Result<?> queryFunctionByModelId(@RequestParam(name = "id", required = true) String id) {
        List<DeviceFunction> deviceFunctions = functionService.lambdaQuery().eq(DeviceFunction::getDeviceModelBy, id).list();
        return Result.ok(deviceFunctions);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "事件定义集合-通过id查询", operateType = 1, logType = 0)
    @ApiOperation(value = "事件定义集合-通过id查询", notes = "事件定义-通过id查询")
    @GetMapping(value = "/queryEventByModelId")
    public Result<?> queryEventByModelId(@RequestParam(name = "id", required = true) String id) {
        List<DeviceEvent> deviceEventList = eventService.selectByMainId(id);
        return Result.ok(deviceEventList);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "数据标签集合-通过id查询")
    @ApiOperation(value = "数据标签集合-通过id查询", notes = "数据标签-通过id查询")
    @GetMapping(value = "/queryLabelByModelId")
    public Result<?> queryLabelByModelId(@RequestParam(name = "id", required = true) String id) {
        List<DeviceLabel> deviceLabelList = labelService.selectByMainId(id);
        return Result.ok(deviceLabelList);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param model
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DeviceModel model) {
        // Step.1 组装查询条件查询数据
        QueryWrapper<DeviceModel> queryWrapper = QueryGenerator.initQueryWrapper(model, request.getParameterMap());
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        //Step.2 获取导出数据
        List<DeviceModel> queryList = modelService.list(queryWrapper);
        // 过滤选中数据
        String selections = request.getParameter("selections");
        List<DeviceModel> deviceModelList = new ArrayList<DeviceModel>();
        if (oConvertUtils.isEmpty(selections)) {
            deviceModelList = queryList;
        } else {
            List<String> selectionList = Arrays.asList(selections.split(","));
            deviceModelList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
        }

        // Step.3 组装pageList
        List<DeviceModelPage> pageList = new ArrayList<DeviceModelPage>();
        for (DeviceModel main : deviceModelList) {
            DeviceModelPage vo = new DeviceModelPage();
            BeanUtils.copyProperties(main, vo);
            List<DeviceData> deviceDataList = dataService.selectByMainId(main.getId());
            vo.setDeviceDataList(deviceDataList);
            List<DeviceFunction> deviceFunctionList = functionService.selectByMainId(main.getId());
            vo.setDeviceFunctionList(deviceFunctionList);
            List<DeviceEvent> deviceEventList = eventService.selectByMainId(main.getId());
            vo.setDeviceEventList(deviceEventList);
            List<DeviceLabel> deviceLabelList = labelService.selectByMainId(main.getId());
            vo.setDeviceLabelList(deviceLabelList);
            pageList.add(vo);
        }

        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "设备模型列表");
        mv.addObject(NormalExcelConstants.CLASS, DeviceModelPage.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("设备模型数据", "导出人:" + sysUser.getRealname(), "设备模型"));
        mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
        return mv;
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            params.setTitleRows(2);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try {
                List<DeviceModelPage> list = ExcelImportUtil.importExcel(file.getInputStream(), DeviceModelPage.class, params);
                for (DeviceModelPage page : list) {
                    DeviceModel po = new DeviceModel();
                    BeanUtils.copyProperties(page, po);
                    modelService.saveMain(po, page.getDeviceDataList(), page.getDeviceFunctionList(), page.getDeviceEventList(), page.getDeviceLabelList());
                }
                return Result.ok("文件导入成功！数据行数:" + list.size());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            } finally {
                try {
                    file.getInputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.ok("文件导入失败！");
    }

}
