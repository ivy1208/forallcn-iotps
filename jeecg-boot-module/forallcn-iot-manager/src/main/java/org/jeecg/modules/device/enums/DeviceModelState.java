package org.jeecg.modules.device.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

/**
 *
 * @author zhouwenrong
 */
@AllArgsConstructor
@Getter
public enum DeviceModelState implements IDictEnum<Byte> {
    /**
     *
     */
    unregistered("未发布", (byte) 0),
    registered("已发布", (byte) 1),
    other("其它", (byte) -100),
    forbidden("禁用", (byte) -1);

    private final String text;
    @EnumValue
    private final Byte value;

    @JsonCreator
    public static DeviceModelState of(byte val) {
        for (DeviceModelState state : values()) {
            if (state.value == val) {
                return state;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + val);
    }
}
