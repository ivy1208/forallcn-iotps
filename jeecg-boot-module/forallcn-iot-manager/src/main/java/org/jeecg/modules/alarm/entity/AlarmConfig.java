package org.jeecg.modules.alarm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhouwr.common.enums.StartStopState;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.zhouwr.common.enums.AlarmLevel;
import org.jeecg.modules.alarm.enums.TriggerMethod;
import org.jeecg.modules.alarm.vo.AlarmConfigVo;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 告警配置
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
@ApiModel(value="iot_alarm_config对象", description="告警配置")
@Data
@TableName("iot_alarm_config")
@NoArgsConstructor
public class AlarmConfig implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**实例id*/
	@Excel(name = "实例id", width = 15)
    @ApiModelProperty(value = "实例id")
    private String instanceId;
    /**
     * 名称
     */
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 触发方式
     */
    @Excel(name = "触发方式", width = 15)
    @ApiModelProperty(value = "触发方式")
    private TriggerMethod triggerMethod;
    /**
     * 告警等级
     */
    @Excel(name = "告警等级", width = 15)
    @ApiModelProperty(value = "告警等级")
    private AlarmLevel alarmLevel;

    /**
     * 是否保存
     */
    @Excel(name = "是否保存", width = 15)
    @ApiModelProperty(value = "是否保存")
    private Boolean isSave;

    /**
     * 是否处理
     */
    @Excel(name = "是否处理", width = 15)
    @ApiModelProperty(value = "是否处理")
    private Boolean isDeal;
    /**
     * 定时表达式
     */
    @Excel(name = "定时表达式", width = 15)
    @ApiModelProperty(value = "定时表达式")
    private String corn;
    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private StartStopState state;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;
    /**
     * 创建日期
     */
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**描述*/
	@Excel(name = "描述", width = 15)
    @ApiModelProperty(value = "描述")
    private String description;

    public AlarmConfig(AlarmConfigVo alarmConfigVo) {
        this.id = alarmConfigVo.getId();
        this.name = alarmConfigVo.getName();
        this.triggerMethod = alarmConfigVo.getTriggerMethod();
        this.corn = alarmConfigVo.getTriggerCorn();
        this.instanceId = alarmConfigVo.getInstanceId();
        this.isDeal = alarmConfigVo.getIsDeal();
        this.alarmLevel = alarmConfigVo.getAlarmLevel();
    }

}
