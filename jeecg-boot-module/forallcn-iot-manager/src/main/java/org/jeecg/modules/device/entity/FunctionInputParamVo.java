package org.jeecg.modules.device.entity;

import com.zhouwr.common.enums.FunctionParamInputMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouwr
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FunctionInputParamVo {

    /**
     * 设备数据id
     */
    private String dataId;

    /**
     * 输入方式
     */
    private FunctionParamInputMode inputMode;


    public FunctionInputParamVo(DeviceFunctionParam functionParam) {
        this.dataId = functionParam.getDataId();
        this.inputMode = functionParam.getInputMode();
    }
}
