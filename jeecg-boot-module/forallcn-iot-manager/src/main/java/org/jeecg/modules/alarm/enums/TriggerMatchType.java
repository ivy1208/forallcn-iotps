package org.jeecg.modules.alarm.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 执行器的匹配类型
 * @author zhouwenrong
 */
@AllArgsConstructor
@Getter
public enum TriggerMatchType implements IDictEnum<String> {
    /**
     *
     */
    ANY("any", "任意"),
    ALL("all", "所有");

    @EnumValue
    private final String value;
    private final String text;

    @JsonCreator
    public static TriggerMatchType of(String value) {
        final List<TriggerMatchType> typeList = Arrays.stream(TriggerMatchType.values())
                .filter(type -> Objects.equals(type.value, value))
                .collect(Collectors.toList());
        if (typeList.size() > 0) {
            return typeList.get(0);
        }
        return null;
    }

    public static JSONArray toJSONArray() {
        return Arrays.stream(TriggerMatchType.values())
                .map(type -> {
                    JSONObject json = new JSONObject();
                    json.put("text", type.text);
                    json.put("value", type.value);
                    return json;
                }).collect(Collectors.toCollection(JSONArray::new));
    }
}
