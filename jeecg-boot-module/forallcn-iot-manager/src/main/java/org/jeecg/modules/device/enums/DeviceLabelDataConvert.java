package org.jeecg.modules.device.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

@Getter
public enum DeviceLabelDataConvert implements IDictEnum<String> {
    LENGTH("length", "长度约束"),
    FMT("fmt", "时间日期格式化"),
    USER("user", "用户属性替换"),
    ;

    @EnumValue
    private final String value;
    private final String text;

    DeviceLabelDataConvert(String value, String text) {
        this.value = value;
        this.text = text;
    }

    @JsonCreator
    public static DeviceLabelDataConvert of(String val) {
        for (DeviceLabelDataConvert source : values()) {
            if (source.value.equals(val)) {
                return source;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + val);
    }
}
