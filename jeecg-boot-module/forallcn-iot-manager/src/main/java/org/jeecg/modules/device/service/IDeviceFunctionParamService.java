package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.enums.FunctionParamDirection;
import com.zhouwr.common.enums.FunctionParamInputMode;
import org.jeecg.modules.device.entity.DeviceFunctionParam;

import java.util.List;

public interface IDeviceFunctionParamService extends IService<DeviceFunctionParam> {
    /**
     * 根据功能id，获取功能参数
     * @param functionId
     * @return
     */
    List<DeviceFunctionParam> getFunctionParams(String functionId);

    /**
     * 根据功能id、参数方向，获取功能参数
     * @param functionId
     * @param paramDirection
     * @return
     */
    List<DeviceFunctionParam> getFunctionParams(String functionId, FunctionParamDirection paramDirection);

    /**
     * 根据功能id、输入方式，获取功能参数
     * @param functionId
     * @param inputMode
     * @return
     */
    List<DeviceFunctionParam> getFunctionParams(String functionId, FunctionParamInputMode inputMode);

}
