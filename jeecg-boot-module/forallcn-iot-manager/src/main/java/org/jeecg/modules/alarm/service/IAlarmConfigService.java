package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.enums.MessageEventType;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;
import org.jeecg.modules.alarm.enums.AlarmActuatorType;
import org.jeecg.modules.alarm.vo.AlarmConfigActuatorVo;
import org.jeecg.modules.alarm.vo.AlarmConfigVo;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description: 告警配置
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface IAlarmConfigService extends IService<AlarmConfig> {

	/**
	 * @param configId
	 * @param actuators
	 */
	void addActuators(String configId, List<AlarmConfigActuatorVo> actuators);

	/**
	 * 添加一对多
	 * @param alarmConfigVo
	 */
	public void saveMain(AlarmConfigVo alarmConfigVo) ;
	
	/**
	 * 修改一对多
	 * @param alarmConfigVo
	 */
	public void updateMain(AlarmConfigVo alarmConfigVo);

	/**
	 * @param actuatorId
	 * @param actuatorType
	 */
	@Transactional(rollbackFor = Exception.class)
	void deleteActuatorProperties(String actuatorId, AlarmActuatorType actuatorType);

	/**
	 * 删除一对多
	 * @param id
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 * @param idList
	 */
	void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 按ID建立警报配置Vo
	 * @param id
	 * @return
	 */
    AlarmConfigVo buildAlarmConfigVoById(String id);

	/**
	 *
	 * 获取告警触发过滤器，根据实例id
	 * @param instanceId
	 * @param dataCode
	 * @return
	 */
	List<AlarmConfigTrigger> listDataReceiveFilter(String instanceId, String dataCode);

	/**
	 * 按实例ID查询
	 *
	 * @param instanceId
	 * @return
	 */
	List<AlarmConfig> queryByInstanceId(String instanceId);

	/**
	 * 执行触发规则
	 *
	 * @param rule
	 * @param dataMap
	 * @return Boolean
	 */
	Boolean invokeTriggerRule(String rule, Map<String, Object> dataMap);

	/**
	 * 生成规则表达式，根据告警配置id和数据标识set
	 *
	 * @param alarmConfigId
	 * @param dataCode
     * @return
     */
    String generateRuleExpre(String alarmConfigId, MessageEventType triggerSource, Set<String> dataCode);

    /**
     * 调用执行器
     *
     * @param alarmConfig {@link AlarmConfig}
     * @param dataMap {@link Map<String, Object>}
     * @return
     */
    void invokeActuators(AlarmConfig alarmConfig, Map<String, Object> dataMap);

    /**
     * 调用告警配置
     *
     * @param instanceId
     */
    void invokeAlarmConfig(String instanceId, String reportDataId, Map<String, Object> dataMap);

	/**
	 * 根据instanceId，删除告警配置及附属子数据
	 * @param instanceId
	 * @return
	 */
    boolean deleteByInstanceId(String instanceId);
}
