package org.jeecg.modules.alarm.entity;

import cn.hutool.core.lang.Console;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhouwr.common.enums.MessageEventType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.alarm.vo.AlarmConfigTriggerVo;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
@ApiModel(value="iot_alarm_config对象", description="告警配置")
@Data
@NoArgsConstructor
@TableName("iot_alarm_config_trigger")
public class AlarmConfigTrigger implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private String id;
	/**警告配置*/
	@ApiModelProperty(value = "警告配置")
	private String alarmConfigId;
	/**触发器名称*/
	@Excel(name = "触发名称", width = 15)
	@ApiModelProperty(value = "触发名称")
	private String name;
	/**触发源*/
	@Excel(name = "触发源", width = 15)
	@ApiModelProperty(value = "触发源")
	private MessageEventType source;
	/**数据节点*/
	@Excel(name = "数据节点", width = 15)
	@ApiModelProperty(value = "数据节点")
	private String dataId;
	/**过滤器*/
	@Excel(name = "过滤", width = 20)
	@ApiModelProperty(value = "过滤")
	private String filterRule;

	public AlarmConfigTrigger(AlarmConfigTriggerVo triggerVo) {
		Console.log(">>>> {} ", triggerVo.getFilters());
		this.id = triggerVo.getId();
		this.name = triggerVo.getName();
		this.source = triggerVo.getSource();
		this.dataId = triggerVo.getModelDataId();
		this.filterRule = JSONArray.toJSONString(triggerVo.getFilters());
	}
}
