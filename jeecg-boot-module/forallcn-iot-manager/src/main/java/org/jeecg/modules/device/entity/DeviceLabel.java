package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.jeecg.modules.device.enums.DeviceLabelCloseMethod;
import org.jeecg.modules.device.enums.DeviceLabelTrigger;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 设备标签
 * @Author: jeecg-boot
 * @Date: 2021-12-07
 * @Version: V1.0
 */
@Data
@ToString
@TableName("iot_device_label")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "iot_device_label对象", description = "设备标签")
public class DeviceLabel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 设备模型
     */
    @Excel(name = "设备模型", width = 15)
    @ApiModelProperty(value = "设备模型")
    private java.lang.String deviceModelBy;
    /**
     * 标识
     */
    @Excel(name = "标识", width = 15)
    @ApiModelProperty(value = "标识")
    private java.lang.String code;
    /**
     * 标题
     */
    @Excel(name = "标题", width = 15)
    @ApiModelProperty(value = "标题")
    private java.lang.String title;
    /**
     * 隐藏标题
     */
    @Excel(name = "隐藏标题", width = 15)
    @ApiModelProperty(value = "隐藏标题")
    private java.lang.Boolean hideTitle;
    /**
     * 标签样式
     */
    @Excel(name = "标签样式", width = 15)
    @ApiModelProperty(value = "标签样式")
    private java.lang.String style;
    /**
     * 使能状态
     */
    @Excel(name = "使能状态", width = 15)
    @ApiModelProperty(value = "使能状态")
    private java.lang.Boolean enable;
    /**
     * 触发方式
     */
    @Excel(name = "触发方式", width = 15)
    @ApiModelProperty(value = "触发方式")
    private DeviceLabelTrigger defaultTrigger;
    /**
     * 显示时长
     */
    @Excel(name = "显示时长", width = 15)
    @ApiModelProperty(value = "显示时长")
    private java.lang.Integer displayDuration;
    /**
     * 关闭方式
     */
    @Excel(name = "关闭方式", width = 15)
    @ApiModelProperty(value = "关闭方式")
    private DeviceLabelCloseMethod closeMethod;
    /**
     * 描述
     */
    @Excel(name = "描述", width = 15)
    @ApiModelProperty(value = "描述")
    private java.lang.String description;
}