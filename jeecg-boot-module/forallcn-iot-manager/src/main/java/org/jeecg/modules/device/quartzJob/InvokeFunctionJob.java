package org.jeecg.modules.device.quartzJob;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.service.IDeviceFunctionService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: jeecg-boot-module-iot
 * @description: 执行实例功能，需调用网络组件，发送请求
 * @author: zhouwr
 * @create: 2020-05-23 11:29
 * @version：1.0
 **/
@Component
@Slf4j
public class InvokeFunctionJob implements Job {
    @Autowired
    private IDeviceFunctionService deviceFunctionsService;

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    @Setter
    private String executeConfig;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        // 网络连接
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            InstanceFunctionVo readValue = objectMapper.readValue(executeConfig, InstanceFunctionVo.class);
            log.info("InvokeFunctionJob >> readValue >>> {}", readValue);
            deviceFunctionsService.invokeFunctionAsync(readValue);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

}
