package org.jeecg.modules.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.message.entity.MessageEvent;

/**
 * @Description: 消息事件
 * @Author: jeecg-boot
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface IMessageEventService extends IService<MessageEvent> {

}
