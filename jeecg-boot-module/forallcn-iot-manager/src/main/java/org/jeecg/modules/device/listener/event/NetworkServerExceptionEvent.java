package org.jeecg.modules.device.listener.event;

import com.zhouwr.common.enums.MessageEventType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jeecg.modules.device.listener.MessageEvent;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-10 13:07
 * @version：1.0
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class NetworkServerExceptionEvent extends MessageEvent {
    private String serverName;

    public NetworkServerExceptionEvent(String serverName, String message) {
        this.serverName = serverName;
        super.message = message;
    }

    @Override
    public MessageEventType getEventType() {
        return MessageEventType.NETWORK_ERROR;
    }
}
