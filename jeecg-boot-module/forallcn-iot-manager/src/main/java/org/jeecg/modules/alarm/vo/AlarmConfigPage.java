package org.jeecg.modules.alarm.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description: 告警配置
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
@Data
@ApiModel(value="iot_alarm_configPage对象", description="告警配置")
public class AlarmConfigPage {

	/**主键*/
	@ApiModelProperty(value = "主键")
	private String id;
	/**实例id*/
	@Excel(name = "实例id", width = 15)
	@ApiModelProperty(value = "实例id")
	private String instanceId;
	/**名称*/
	@Excel(name = "名称", width = 15)
	@ApiModelProperty(value = "名称")
	private String name;
	/**触发方式*/
	@Excel(name = "触发方式", width = 15)
	@ApiModelProperty(value = "触发方式")
	private String triggerMethod;
	/**定时表达式*/
	@Excel(name = "定时表达式", width = 15)
	@ApiModelProperty(value = "定时表达式")
	private String corn;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private String sysOrgCode;
	/**描述*/
	@Excel(name = "描述", width = 15)
	@ApiModelProperty(value = "描述")
	private String description;
	
	@ExcelCollection(name="告警触发器")
	@ApiModelProperty(value = "告警触发器")
	private List<AlarmConfigTrigger> alarmConfigTriggerList;

}
