package org.jeecg.modules.scene.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-11-13 21:36
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceStateVo {
    private Long online;
    private Long offline;
}
