package org.jeecg.modules.network.network.tcp;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @program: forallcn-iotps
 * @description: ChannelFutureListener
 * @author: zhouwr
 * @create: 2021-01-06 13:33
 * @version：1.0
 **/
@Slf4j
@Component
public class TcpChannelFutureListener implements ChannelFutureListener {
    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        log.info("seqId={} sent",future);
    }
}
