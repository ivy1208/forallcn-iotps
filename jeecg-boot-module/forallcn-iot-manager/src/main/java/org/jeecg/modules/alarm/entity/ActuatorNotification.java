package org.jeecg.modules.alarm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhouwr.common.enums.NoticeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.alarm.vo.ActuatorNotificationVo;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-15 21:20
 * @version：1.0
 **/
@ApiModel(value = "ActuatorNotification对象", description = "执行器消息通知")
@Data
@NoArgsConstructor
@TableName("iot_actuator_notification")
public class ActuatorNotification implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "通知类型")
    private NoticeType noticeType;

    @ApiModelProperty(value = "通知")
    private String noticeConfigId;

    @ApiModelProperty(value = "消息模板")
    private String noticeTemplateCode;

    public ActuatorNotification(ActuatorNotificationVo notificationVo) {
        this();
        this.setNoticeType(notificationVo.getNoticeType());
        this.setNoticeConfigId(notificationVo.getNoticeConfigId());
        this.setNoticeTemplateCode(notificationVo.getNoticeTemplateCode());
    }

    public ActuatorNotification(String id, ActuatorNotificationVo notificationVo) {
        this(notificationVo);
        this.id = id;
    }

    public ActuatorNotificationVo vo() {
        ActuatorNotificationVo vo = new ActuatorNotificationVo();
        vo.setId(this.id);
        vo.setNoticeType(this.noticeType);
        vo.setNoticeConfigId(this.noticeConfigId);
        vo.setNoticeTemplateCode(this.noticeTemplateCode);
        return vo;
    }
}
