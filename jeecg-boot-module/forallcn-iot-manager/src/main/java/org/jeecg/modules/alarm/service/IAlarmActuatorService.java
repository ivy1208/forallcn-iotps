package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.alarm.entity.AlarmConfigActuator;

import java.util.List;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface IAlarmActuatorService extends IService<AlarmConfigActuator> {

	/**
	 * 根据配置id查询
	 * @param configId
	 * @return
	 */
	public List<AlarmConfigActuator> selectByMainId(String configId);
}
