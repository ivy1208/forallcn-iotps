package org.jeecg.modules.scene.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.scene.entity.SceneScheme;
import org.jeecg.modules.scene.vo.SchemeCountVo;

import java.util.List;

/**
 * @Description: 场景方案
 * @Author: jeecg-boot
 * @Date: 2020-06-05
 * @Version: V1.0
 */
public interface SceneSchemeMapper extends BaseMapper<SceneScheme> {
    public List<SchemeCountVo> schemeCountListBySceneId(@Param("sceneId") String sceneId);
}
