package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import com.zhouwr.common.device.vo.InternetDevice.InternetDeviceVo;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.entity.DeviceModel;
import org.jeecg.modules.scene.vo.DeployConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 设备映射器实例
 *
 * @author zhouwenrong
 * @Description: 设备实例
 * @Author: jeecg-boot
 * @Date: 2020-04-11
 * @Version: V1.0
 * @date 2022/02/11
 */
public interface DeviceInstanceMapper extends BaseMapper<DeviceInstance> {

    /**
     * 创建数据表
     *
     * @param instanceId  实例id
     * @param dataColumns 数据列
     */
    public void createDataTable(@Param("instanceId") String instanceId, @Param("dataColumns") Map<String, String> dataColumns);

    /**
     * 获取网关类型的实例设备
     *
     * @param modelType 模型类型
     * @return {@link List}<{@link DeviceInstance}>
     */
    public List<DeviceInstance> getInstanceByModelType(@Param("modelType") String modelType);

    /**
     * 根据设备实例的ModelId，获取设备模型
     *
     * @param modelId 模型id
     * @return {@link DeviceModel}
     */
    public DeviceModel getDeviceModelByModelId(@Param("modelId") String modelId);

    /**
     * 获取在线实例列表，根据tcp地址
     *
     * @param address 地址
     * @return {@link List}<{@link DeviceInstance}>
     */
    public List<DeviceInstance> listOnlineInstanceByAddress(@Param("address") String address);

    /**
     * 部署设备列表
     *
     * @return {@link List}<{@link DeployConfig}>
     */
    public List<DeployConfig> listDeployDevice();

    /**
     * 通过id获取属性
     *
     * @param instanceId 实例id
     * @param column     列
     * @param dictTable  dict类型表
     * @param dictKey    dict关键
     * @param dictValue  东西价值
     * @param dateformat dateformat
     * @return {@link String}
     */
    String getAttributeById(@NotNull String instanceId, @NotNull String column, String dictTable, String dictKey, String dictValue, String dateformat);

    /**
     * 获取最新数据
     *
     * @param instanceId   实例id
     * @return {@link List}<{@link InstanceDataStructure}>
     */
    List<InstanceDataStructure> selectInstanceLatestData(@Param("instanceId") String instanceId);

    /**
     * 选择实例历史数据
     * @param instanceId
     * @param startTime
     * @param endTime
     * @param pageIndex
     * @param pageSize
     * @return {@link List}<{@link InstanceDataStructure}>
     */
    List<InstanceDataStructure> selectInstanceHistoryData(
            @Param("instanceId") String instanceId,
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startTime,
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endTime,
            @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);

    /**
     * 搜索联网设备
     * @param instanceId
     * @param functionType
     * @return
     */
    List<InternetDeviceVo> selectInternetDevice(@Param("instanceId") String instanceId, @Param("functionType") String functionType);

    /**
     * 获取所有的功能
     * @return
     */
    List<InstanceFunctionVo> selectFunction();

    /**
     * 获取功能参数
     *
     * @param instanceId   实例id
     *      @see org.jeecg.modules.device.enums.FunctionType
     * @return InstanceFunctionVo
     *      @see InstanceFunctionVo
     */
    List<InstanceFunctionVo> selectFunctionByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 获取功能参数
     * @param functionId 功能id
     * @return InstanceFunctionVo
     */
    InstanceFunctionVo selectFunctionById(@Param("functionId") String functionId);

    /**
     * 获取执行功能配置信息，包括功能输入、输出参数，和执行配置
     * @param instanceId
     * @return
     */
    List<InstanceFunctionVo> selectFunctionExecuteByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 获取执行功能配置信息
     * @param functionId
     * @return
     */
    InstanceFunctionVo selectFunctionExecuteById(@Param("functionId") String functionId);

}
