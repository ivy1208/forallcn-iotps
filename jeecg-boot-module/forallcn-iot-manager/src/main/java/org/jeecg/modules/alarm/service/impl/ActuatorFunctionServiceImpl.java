package org.jeecg.modules.alarm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.alarm.entity.ActuatorInvokeFunction;
import org.jeecg.modules.alarm.mapper.ActuatorInvokeFunctionMapper;
import org.jeecg.modules.alarm.service.IActuatorFunctionService;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.service.IDeviceFunctionService;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: forallcn-iotps
 * @description: IActuatorFunctionService
 * @author: zhouwr
 * @create: 2021-03-05 09:55
 * @version：1.0
 **/
@Service
@Slf4j
public class ActuatorFunctionServiceImpl extends ServiceImpl<ActuatorInvokeFunctionMapper, ActuatorInvokeFunction> implements IActuatorFunctionService {
    @Autowired
    private IDeviceFunctionService functionService;
    @Autowired
    private IDeviceInstanceService instanceService;

    @Override
    public InstanceFunctionVo getExecuteConfig(String actuatorId) {
        ActuatorInvokeFunction invokeFunction = this.getById(actuatorId);
        log.debug("invokeFunction >>> {}", invokeFunction);
        InstanceFunctionVo functionVo = new InstanceFunctionVo();
        DeviceInstance instance = instanceService.getById(invokeFunction.getInstanceId());
        assert instance != null;
        try {
            functionVo = instanceService.buildFunctionStructures(instance.getId()).stream()
                    .filter(function -> function.getId().equals(invokeFunction.getFunctionId()))
                    .findAny().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return functionVo;
    }
}
