package org.jeecg.modules.alarm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description: 警报触发过滤器
 * @author: zhouwr
 * @create: 2021-01-21 12:47
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlarmTriggerFilterVo {
    private String filterKey;
    private String filterRule;
    private Object filterValue;
}
