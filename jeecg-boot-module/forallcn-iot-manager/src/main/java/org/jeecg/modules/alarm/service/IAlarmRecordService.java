package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import org.jeecg.modules.alarm.entity.AlarmRecord;

import java.util.Map;

/**
 * @Description: 告警记录
 * @Author: jeecg-boot
 * @Date: 2021-01-19
 * @Version: V1.0
 */
public interface IAlarmRecordService extends IService<AlarmRecord> {

    /**
     * 保存告警记录
     *
     * @param alarmConfig
     * @param dataMap
     * @return
     */
    AlarmRecord save(AlarmConfig alarmConfig, Map<String, Object> dataMap, String reportDataId);

    /**
     * 扩展参数
     *
     * @param pageList
     */
    void recordExtents(IPage<AlarmRecord> pageList);
}
