package org.jeecg.modules.network.network;

import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Future;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: forallcn-iotps
 * @description: 请求Future池
 * @author: zhouwr
 * @create: 2021-02-22 08:49
 * @version：1.0
 **/
public class RequestFuturePool {
    public static final Map<String, OperationResultFuture> futureMap = new ConcurrentHashMap<>();

    public static Future<Object> add(String key) {
        OperationResultFuture future = new OperationResultFuture();
        futureMap.put(key, future);
        return future;
    }

    public static void setResult(String key, Object result) {
        try {
            OperationResultFuture future = futureMap.get(key);
            if (future != null) {
                future.setSuccess(result);
            }
        } finally {
            futureMap.remove(key);
        }
    }
}

class OperationResultFuture extends DefaultPromise<Object> {

}
