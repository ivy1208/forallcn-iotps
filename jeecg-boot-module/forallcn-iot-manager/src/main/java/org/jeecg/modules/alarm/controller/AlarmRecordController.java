package org.jeecg.modules.alarm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.alarm.entity.AlarmRecord;
import org.jeecg.modules.alarm.service.IAlarmRecordService;
import org.jeecg.modules.device.service.IDataReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 告警记录
 * @Author: jeecg-boot
 * @Date: 2021-01-19
 * @Version: V1.0
 */
@Api(tags = "告警记录")
@RestController
@RequestMapping("/alarm/record")
@Slf4j
public class AlarmRecordController extends JeecgController<AlarmRecord, IAlarmRecordService> {
    @Autowired
    private IAlarmRecordService alarmRecordService;
    @Autowired
    private IDataReportService dataService;

    /**
     * 分页列表查询
     *
     * @param alarmRecord
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "告警记录-分页列表查询")
    @ApiOperation(value = "告警记录-分页列表查询", notes = "告警记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(AlarmRecord alarmRecord,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        log.info("{}", alarmRecord);
        QueryWrapper<AlarmRecord> queryWrapper = QueryGenerator.initQueryWrapper(alarmRecord, req.getParameterMap());
        Page<AlarmRecord> page = new Page<AlarmRecord>(pageNo, pageSize);
        IPage<AlarmRecord> pageList = alarmRecordService.page(page, queryWrapper);
        alarmRecordService.recordExtents(pageList);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param alarmRecord
     * @return
     */
    @AutoLog(value = "告警记录-添加")
    @ApiOperation(value = "告警记录-添加", notes = "告警记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody AlarmRecord alarmRecord) {
        alarmRecordService.save(alarmRecord);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param alarmRecord
     * @return
     */
    @AutoLog(value = "告警记录-编辑")
    @ApiOperation(value = "告警记录-编辑", notes = "告警记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody AlarmRecord alarmRecord) {
        alarmRecordService.updateById(alarmRecord);
        return Result.ok("编辑成功!");
    }

    /**
     * 更新处理内容
     *
     * @param id
     * @param dealContent
     * @return
     */
    @ApiOperation(value = "告警记录-处理")
    @GetMapping(value = "/updateDeal")
    public Result<?> updateDeal(@RequestParam String id, @RequestParam String dealContent) {
        boolean update = alarmRecordService.lambdaUpdate()
                .eq(AlarmRecord::getId, id)
                .set(StringUtils.isNotBlank(dealContent), AlarmRecord::getDealContent, dealContent)
                .update();
        return Result.ok("处理" + (update ? "成功" : "失败") + "!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "告警记录-通过id删除")
    @ApiOperation(value = "告警记录-通过id删除", notes = "告警记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        alarmRecordService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "告警记录-批量删除")
    @ApiOperation(value = "告警记录-批量删除", notes = "告警记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.alarmRecordService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "告警记录-通过id查询")
    @ApiOperation(value = "告警记录-通过id查询", notes = "告警记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        AlarmRecord alarmRecord = alarmRecordService.getById(id);
        if (alarmRecord == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(alarmRecord);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param alarmRecord
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, AlarmRecord alarmRecord) {
        return super.exportXls(request, alarmRecord, AlarmRecord.class, "告警记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, AlarmRecord.class);
    }

}
