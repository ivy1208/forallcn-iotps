package org.jeecg.modules.alarm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.alarm.entity.AlarmConfigActuator;
import org.jeecg.modules.alarm.mapper.AlarmConfigActuatorMapper;
import org.jeecg.modules.alarm.service.IAlarmActuatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 告警执行器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
@Service
public class AlarmActuatorServiceImpl extends ServiceImpl<AlarmConfigActuatorMapper, AlarmConfigActuator> implements IAlarmActuatorService {
	
	@Autowired
	private AlarmConfigActuatorMapper actuatorMapper;
	
	@Override
	public List<AlarmConfigActuator> selectByMainId(String mainId) {
		return actuatorMapper.selectByConfigId(mainId);
	}
}
