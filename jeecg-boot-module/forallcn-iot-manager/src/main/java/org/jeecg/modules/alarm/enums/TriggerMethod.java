package org.jeecg.modules.alarm.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum TriggerMethod implements IDictEnum<String> {
    /**
     *
     */
    DEVICE("device", "设备触发"),
    TIMER("timer", "定时触发");

    @EnumValue
    private final String value;
    private final String text;

    @JsonCreator
    public static TriggerMethod of(String value) {
        final List<TriggerMethod> triggerMethods = Arrays.stream(TriggerMethod.values())
                .filter(type -> Objects.equals(type.value, value))
                .collect(Collectors.toList());
        if (triggerMethods.size() > 0) {
            return triggerMethods.get(0);
        }
        return null;
    }

    public static JSONArray toJSONArray() {
        return Arrays.stream(TriggerMethod.values())
                .map(type -> {
                    JSONObject json = new JSONObject();
                    json.put("text", type.text);
                    json.put("value", type.value);
                    return json;
                }).collect(Collectors.toCollection(JSONArray::new));
    }
}
