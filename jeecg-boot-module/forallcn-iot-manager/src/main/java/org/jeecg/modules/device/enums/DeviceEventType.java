package org.jeecg.modules.device.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

@Getter
@AllArgsConstructor
public enum DeviceEventType implements IDictEnum<Byte> {
    /**
     *
     */
    RULE_CHECK((byte)0x0, "规则校验"),
    EVENT_REPORT((byte)0x01, "事件上报");

    private final Byte value;
    private final String text;

    @JsonCreator
    public static DeviceEventType of(Byte value) {
        for (DeviceEventType source : DeviceEventType.values()) {
            if (source.value.equals(value)) {
                return source;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

}
