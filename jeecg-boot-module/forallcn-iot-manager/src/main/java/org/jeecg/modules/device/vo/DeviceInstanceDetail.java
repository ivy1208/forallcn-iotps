package org.jeecg.modules.device.vo;

import com.zhouwr.common.device.vo.InstanceDataStructure;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import lombok.Getter;
import lombok.Setter;
import org.jeecg.modules.device.entity.DataReport;
import org.jeecg.modules.device.entity.DeviceEvent;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.entity.DeviceModel;

import java.util.List;

@Getter
@Setter
public class DeviceInstanceDetail {

    public DeviceModel model;

    public DeviceInstance instance;

    public List<InstanceDataStructure> instanceDatas;

    public List<InstanceFunctionVo> functions;

    public List<DeviceEvent> deviceEventList;

    public List<DataReport> dataReportList;

    @Override
    public String toString() {
        return "DeviceInstanceDetail{" +
                "model=" + model +
                ", instance=" + instance +
                ", instanceDatas=" + instanceDatas +
                ", functions=" + functions +
                ", deviceEventList=" + deviceEventList +
                ", dataReportList=" + dataReportList +
                '}';
    }

}
