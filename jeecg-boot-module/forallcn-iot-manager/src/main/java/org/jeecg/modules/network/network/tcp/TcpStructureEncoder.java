package org.jeecg.modules.network.network.tcp;

import com.zhouwr.common.device.vo.InstanceFunctionExecStructure;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.util.internal.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.List;

/**
 * @program: jeecg-boot-module-iot
 * @description: Tcp字符编码
 * @author: zhouwr
 * @create: 2020-05-27 13:59
 * @version：1.0
 **/
@Slf4j
@Sharable
public class TcpStructureEncoder extends MessageToMessageEncoder<InstanceFunctionExecStructure> {
    private Charset charset;

    public TcpStructureEncoder() {
        this(Charset.defaultCharset());
    }

    public TcpStructureEncoder(Charset charset) {
        this.charset = (Charset) ObjectUtil.checkNotNull(charset, "charset");
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, InstanceFunctionExecStructure execStructure, List<Object> out) throws Exception {
        log.info("TcpStructureEncoder >>>>> " + execStructure);

        if (execStructure != null) {
            out.add(execStructure);
        }
    }
}

