package org.jeecg.modules.device.vo.label;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.entity.DeviceLabel;
import org.jeecg.modules.device.entity.DeviceLabelViewData;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DeviceLabelVo extends DeviceLabel {
    private String instanceId;
    private List<ViewDataVo> viewData = new ArrayList<>();

    public DeviceLabelVo(DeviceLabel label) {
        super();
        super.setId(label.getId());
        super.setDeviceModelBy(label.getDeviceModelBy());
        super.setCode(label.getCode());
        super.setTitle(label.getTitle());
        super.setHideTitle(label.getHideTitle());
        super.setCloseMethod(label.getCloseMethod());
        super.setDefaultTrigger(label.getDefaultTrigger());
        super.setDescription(label.getDescription());
        super.setDisplayDuration(label.getDisplayDuration());
        super.setEnable(label.getEnable());
        super.setStyle(label.getStyle());
    }

    public DeviceLabelVo(DeviceLabel label, List<DeviceLabelViewData> viewData) {
        this(label);
        if (viewData != null && viewData.size() > 0) {
            final List<ViewDataVo> viewDataVos = viewData.stream().map(ViewDataVo::new).collect(Collectors.toList());
            this.setViewData(viewDataVos);
        }
    }

    @Override
    public String toString() {
        return "DeviceLabelVo{" +
                super.toString() +
                "viewData=" + viewData +
                '}';
    }
}
