package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.alarm.entity.AlarmConfigActuator;

import java.util.List;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface AlarmConfigActuatorMapper extends BaseMapper<AlarmConfigActuator> {

	/**
	 * 根据配置id删除
	 * @param configId
	 * @return
	 */
	boolean deleteByConfigId(@Param("configId") String configId);

	/**
	 * 根据配置id查询
	 * @param configId
	 * @return
	 */
	List<AlarmConfigActuator> selectByConfigId(@Param("configId") String configId);
}
