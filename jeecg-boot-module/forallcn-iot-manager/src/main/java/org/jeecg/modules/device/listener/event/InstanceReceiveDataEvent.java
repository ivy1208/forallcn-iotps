package org.jeecg.modules.device.listener.event;

import com.zhouwr.common.enums.MessageEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jeecg.modules.device.listener.MessageEvent;

import java.util.Map;

/**
 * @program: forallcn-iotps
 * @description: 实例数据接收
 * @author: zhouwr
 * @create: 2020-12-22 15:16
 * @version：1.0
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstanceReceiveDataEvent extends MessageEvent {
    private String instanceId;
    Map<String, Object> dataMap;

    @Override
    public MessageEventType getEventType() {
        return MessageEventType.DATA_RECEIVE;
    }
}
