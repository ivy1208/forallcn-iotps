package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouwr.common.device.vo.function.FunctionExecuteConfig;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.device.entity.DeviceFunction;

import java.util.List;

/**
 * @Description: 功能定义
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
public interface DeviceFunctionMapper extends BaseMapper<DeviceFunction> {

    /**
     * 按主要ID删除
     * @param mainId
     * @return
     */
    boolean deleteByMainId(@Param("mainId") String mainId);

    /**
     * 按主要ID选择
     * @param mainId
     * @return
     */
    List<DeviceFunction> selectByMainId(@Param("mainId") String mainId);

    boolean deleteExecuteConfig(@Param("instanceId") String instanceId, @Param("functionId") String functionId);

    boolean saveExecuteConfig(@Param("instanceId") String instanceId, @Param("functionId") String functionId, @Param("executeConfig") FunctionExecuteConfig executeConfig);
}
