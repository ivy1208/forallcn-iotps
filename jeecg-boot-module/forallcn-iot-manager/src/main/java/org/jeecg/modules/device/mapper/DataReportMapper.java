package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouwr.common.device.vo.report.ReportDataVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.device.entity.DataReport;
import org.jeecg.modules.device.vo.DataReportCountVo;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 设备上传数据
 * @Author: jeecg-boot
 * @Date: 2020-05-14
 * @Version: V1.0
 */
public interface DataReportMapper extends BaseMapper<DataReport> {


    /**
     * 按场景ID统计数据
     *
     * @param sceneId
     * @return
     */
    public List<DataReportCountVo> countDataBySceneId(@Param("sceneId") String sceneId);

    /**
     * 当月每天数据量统计
     *
     * @return
     * @author zhouwr
     */
    @Select("select count(1) as 'count', DATE_FORMAT(a.create_time,'%Y-%m-%d') as datetime from iot_data a " +
            "where MONTH(a.create_time) = MONTH(NOW()) " +
            "group by datetime")
    List<Map<String, Object>> countDailyDataByMonth();

    List<ReportDataVo> selectReportData(@Param("instanceId") String sceneId,
                                        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startTime,
                                        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endTime,
                                        @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);

    Long selectReportDataTotal(@Param("instanceId") String sceneId,
                          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startTime,
                          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endTime,
                          @Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize);
}
