package org.jeecg.modules.network.network;

import com.zhouwr.common.enums.NetworkType;
import com.zhouwr.common.network.Network;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.network.entity.NetworkService;
import org.jeecg.modules.network.service.INetworkServiceService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhouwenrong
 */
@Component
@Slf4j
public class NetworkManager implements BeanPostProcessor, CommandLineRunner {

    @Autowired
    private INetworkServiceService networkServiceService;

    // 网络组件：NettyServiceI 注册的bean
    private Map<String, Network> networkServiceMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, NetworkProvider<?>> networkProviderMap = new ConcurrentHashMap<>();

    public Map<String, Network> getServiceList() {
        return this.networkServiceMap;
    }

    /**
     * 重新开启网络
     *
     * @param networkService
     */
    public void reload(NetworkService networkService) {
        String addr = networkService.getHost() + ":" + networkService.getPort();
        Network nettyService = this.networkServiceMap.computeIfAbsent(addr, k -> null);
        if (nettyService == null) {
            networkServiceMap.put(addr, createNetworkService(networkService));
        }
        log.info("网络开启{}，现有网络：{}", addr, networkServiceMap.toString());

    }

    /**
     * 关闭网络
     *
     * @param networkService
     */
    public void shutdown(NetworkService networkService) {
        String addr = networkService.getHost() + ":" + networkService.getPort();
        Network nettyService = this.networkServiceMap.computeIfAbsent(addr, k -> null);
        if (nettyService != null) {
            nettyService.shutdown();
            log.info("网络关闭{}，现有网络：{}", addr, networkServiceMap.toString());
        }
        networkServiceMap.remove(addr);
    }

    /**
     * 创建网络服务
     *
     * @param networkProperties
     * @return
     */
    protected Network createNetworkService(NetworkService networkProperties) {
        NetworkProvider<?> provider = networkProviderMap.get(networkProperties.getType());
        // 判断网络组件是否已经加载
        if (provider == null) {
            log.error("网络组件类型：{}不支持", networkProperties.getType());
            throw new RuntimeException("网络组件类型：" + networkProperties.getType() + "不支持");
        } else {
            String addr = networkProperties.getHost() + ":" + networkProperties.getPort();
            Network network = networkServiceMap.computeIfAbsent(addr, id -> null);
            if (network == null) {
                network = (Network) provider.createNetwork(networkProperties);
            } else {
                //单例，已经存在则重新加载
                provider.reload(network, networkProperties);
            }
            return network;
        }
    }

    /**
     * 根据数据表中数据，启动相应服务
     */
    protected void checkNetwork() {
        log.info("checkNetwork......");
        List<NetworkService> networkProperties = networkServiceService.lambdaQuery().list();
        networkProperties.forEach(networkProperty -> {
            if (networkProperty.getState()) {
                String addr = networkProperty.getHost() + ":" + networkProperty.getPort();
                networkServiceMap.put(addr, createNetworkService(networkProperty));
            }
        });
    }

    protected void register(NetworkProvider<Object> provider) {
        log.info(">>>>>>>>> 注册网络通信组件：" + provider.getType().getName() + " <<<<<<<<<<");
        this.networkProviderMap.put(provider.getType().getCode(), provider);
    }

    /**
     * springboot启动过程中，注册的bean
     *
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof NetworkProvider) {
            this.register((NetworkProvider) bean);
        }
        return bean;
    }

    /**
     * springboot 启动完成时，执行操作
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        log.info(">>>>>>>>>>>>>>>> 网络服务初始化-开始 <<<<<<<<<<<<<<<<");
        this.checkNetwork();
        log.info(">>>>>>>>>>>>>>>> 网络服务初始化-结束 <<<<<<<<<<<<<<<<");
    }


    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Synchronization implements Serializable {
        private NetworkType type;
        private String id;
    }
}
