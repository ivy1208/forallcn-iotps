package org.jeecg.modules.device.vo.label;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.metadata.Metadata;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jeecg.modules.device.entity.DeviceLabelViewData;

/**
 * 视图数据签证官
 *
 * @author zhouwenrong
 * @date 2022/01/06
 */// 该注解用于子类对象之间进行比较的时候
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ViewDataVo extends DeviceLabelViewData {

    /**
     * code->key
     * 数据关键字
     */
    private String key;
    /**
     * 转换
     */
    private ConvertVo convert = new ConvertVo();
    private Metadata metadata;
    /**
     * 数据值
     */
    private String value;
    /**
     * 数值更新时间
     */
    private String valueTime;

    /**
     * 视图数据签证官
     *
     * @param viewData 视图数据
     */
    public ViewDataVo(DeviceLabelViewData viewData) {
        super();
        super.setId(viewData.getId());
//        super.setDeviceLabelBy(viewData.getDeviceLabelBy());
        this.setKey(viewData.getCode());
        super.setName(viewData.getName());
        super.setType(viewData.getType());
        super.setSource(viewData.getSource());
        super.setLength(viewData.getLength());
        super.setDateformat(viewData.getDateformat());
        this.convert = new ConvertVo(viewData.getDictTable(), viewData.getDictKey(), viewData.getDictValue());
    }

    /**
     * 数据长度
     *
     * @param value
     * @return
     */
    public String length(String value) {
        if (super.getLength() > 0 && super.getLength() < value.length()) {
            this.value = value.substring(0, super.getLength());
        } else {
            this.value = value;
        }
        return this.value;
    }

}

