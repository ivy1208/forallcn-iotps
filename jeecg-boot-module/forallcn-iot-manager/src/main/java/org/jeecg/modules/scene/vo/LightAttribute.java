package org.jeecg.modules.scene.vo;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.zhouwr.common.utils.Vector3;

/**
 * @program: jeecg-boot-iotcp
 * @description: 灯光属性
 * @author: zhouwr
 * @create: 2020-11-05 11:48
 * @version：1.0
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class LightAttribute {
    private String color;
    private Float intensity;
    private Vector3 position;

    public LightAttribute(JSONObject json) {
        if (json != null) {
            this.color = json.getString("color");
            this.intensity = json.getFloat("color");
            this.position = new Vector3(json.getJSONObject("position"));
        }
    }

    public LightAttribute(String color) {
        this.color = color;
    }

    public LightAttribute(String color, Float intensity) {
        this.color = color;
        this.intensity = intensity;
    }

    public LightAttribute(String color, Vector3 position) {
        this.color = color;
        this.position = position;
    }
}
