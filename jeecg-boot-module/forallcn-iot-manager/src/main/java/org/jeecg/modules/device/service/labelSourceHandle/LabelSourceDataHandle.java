package org.jeecg.modules.device.service.labelSourceHandle;

import org.jeecg.modules.device.enums.DeviceLabelSource;
import org.jeecg.modules.device.vo.label.ViewDataVo;

import java.util.Objects;

/**
 * 设备标签元数据处理对象
 *
 * @author zhouwenrong
 * @date 2022/01/07
 */
public class LabelSourceDataHandle {

    /**
     * @param modelId    模型id
     * @param instanceId 实例id
     * @param viewDataVo 视图数据签证官
     * @return {@link String}
     */
    public static String getValue(String modelId, String instanceId, ViewDataVo viewDataVo) {
        try {
            return Objects.requireNonNull(getSource(viewDataVo.getSource())).getValue(modelId, instanceId, viewDataVo);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param source
     * @return {@link ILabelSourceData}
     */
    public static ILabelSourceData getSource(DeviceLabelSource source) {
        try {
            if (source != null) {
                return source.getSourceHandle().newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
