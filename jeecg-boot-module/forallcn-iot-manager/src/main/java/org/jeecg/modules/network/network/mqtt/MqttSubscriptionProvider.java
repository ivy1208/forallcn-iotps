package org.jeecg.modules.network.network.mqtt;

import com.zhouwr.common.enums.NetworkType;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.network.entity.NetworkService;
import com.zhouwr.common.network.Network;
import org.jeecg.modules.network.network.NetworkProvider;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

/**
 * @author zhouwenrong
 */
@Component
@Slf4j
public class MqttSubscriptionProvider implements NetworkProvider<NettyMqttSubscriber> {

    @Nonnull
    @Override
    public NetworkType getType() {
        return NetworkType.MQTT_SUBSCRIPTION;
    }

    @Nonnull
    @Override
    public NettyMqttSubscriber createNetwork(@Nonnull NetworkService properties) {
        NettyMqttSubscriber subscriber = new NettyMqttSubscriber(properties);
        subscriber.shutdown();
        subscriber.initMqttSubscriber();
        return subscriber;
    }

    @Override
    public void reload(@Nonnull Network network, @Nonnull NetworkService properties) {
        NettyMqttSubscriber subscriber = ((NettyMqttSubscriber) network);
        subscriber.shutdown();
        subscriber.initMqttSubscriber();
    }
}
