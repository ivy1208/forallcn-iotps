package org.jeecg.modules.device.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.device.vo.function.FunctionParamVo;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import com.zhouwr.common.enums.InstanceParamType;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.entity.DeviceInstanceParam;
import org.jeecg.modules.device.mapper.DeviceInstanceParamMapper;
import org.jeecg.modules.device.service.IDeviceInstanceParamService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-01 15:00
 * @version：1.0
 **/
@Slf4j
@Service
public class DeviceInstanceParamServiceImpl extends ServiceImpl<DeviceInstanceParamMapper, DeviceInstanceParam> implements IDeviceInstanceParamService {
    @Override
    public List<DeviceInstanceParam> getInstanceParams(String instanceId, String functionId) {
        return this.lambdaQuery().eq(DeviceInstanceParam::getInstanceId, instanceId).eq(DeviceInstanceParam::getFunctionId, functionId).list();
    }

    @Override
    public DeviceInstanceParam getInstanceParams(String instanceId, String functionId, String dataId) {
        final List<DeviceInstanceParam> list = this.lambdaQuery()
                .eq(DeviceInstanceParam::getInstanceId, instanceId)
                .eq(DeviceInstanceParam::getFunctionId, functionId)
                .eq(DeviceInstanceParam::getDataId, dataId)
                .list();
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return new DeviceInstanceParam();
        }
    }

    @Override
    public List<DeviceInstanceParam> getInstanceParamsByInstanceId(String instanceId) {
        return this.lambdaQuery().eq(DeviceInstanceParam::getInstanceId, instanceId).list();
    }

    @Override
    public List<DeviceInstanceParam> getInstanceParamsByFunctionId(String functionId) {
        return this.lambdaQuery().eq(DeviceInstanceParam::getFunctionId, functionId).list();
    }

    @Override
    public boolean removeByInstanceId(String instanceId) {
        QueryWrapper<DeviceInstanceParam> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("instance_id", instanceId);
        return this.remove(queryWrapper);
    }

    @Override
    public void saveParamBatch(String instanceId, String functionId, @NotNull List<FunctionParamVo> extendParams) {
        List<DeviceInstanceParam> instanceParams = new ArrayList<>();
        for (FunctionParamVo extentParam : extendParams) {
            instanceParams.add(new DeviceInstanceParam(InstanceParamType.INPUT, instanceId, functionId, extentParam.getId(), extentParam.getFormatValue()));
        }
        log.info(JSON.toJSONString(instanceParams));

        this.saveBatch(instanceParams);
    }

    @Override
    public List<DeviceInstanceParam> queryByFunctionIds(List<String> functionIds) {
        return this.lambdaQuery().in(DeviceInstanceParam::getFunctionId, functionIds).list();
    }

    @Override
    public boolean updateInstanceParams(List<InstanceFunctionVo> extendParams) {
        List<DeviceInstanceParam> instanceParams = new ArrayList<>();
        for (InstanceFunctionVo instanceFunctionVo: extendParams) {
            this.removeByInstanceId(instanceFunctionVo.getInstanceId());
            for (FunctionParamVo extentParam : instanceFunctionVo.getInputParams()) {
                // 校验
                instanceParams.add(new DeviceInstanceParam(
                        InstanceParamType.INPUT,
                        instanceFunctionVo.getInstanceId(),
                        instanceFunctionVo.getId(),
                        extentParam.getId(),
                        extentParam.getFormatValue()));
            }
        }
        log.info(JSON.toJSONString(instanceParams));
        return this.saveBatch(instanceParams);
    }
}
