package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.device.vo.InstanceFunctionInputParam;
import com.zhouwr.common.device.vo.function.FunctionExecuteConfig;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import org.jeecg.modules.device.entity.DeviceFunction;
import org.jeecg.modules.device.entity.FunctionInputParamVo;
import org.jeecg.modules.device.entity.InputData;

import java.util.List;

/**
 * @Description: 功能定义
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
public interface IDeviceFunctionService extends IService<DeviceFunction> {

    public List<DeviceFunction> selectByMainId(String mainId);

    /**
     * 获取设备功能定义的输入参数，根据功能定义id
     *
     * @param funcId
     * @return
     */
    public List<FunctionInputParamVo> listInputParamByFuncId(String funcId);

    /**
     * 获取设备功能定义的输入参数数据，根据功能定义id
     *
     * @param funcId
     * @return
     */
    public List<InputData> listInputDataByFuncId(String funcId);

    /**
     * 重复校验
     *
     * @param functionStructure
     * @return Boolean true 无重复，校验通过
     */
    public Boolean functionParamCheckUnique(ModelFunctionVo modelFunctionVo);

    /**
     * 异步执行功能
     *
     * @param executeConfig
     * @return
     */
    Boolean invokeFunctionAsync(InstanceFunctionVo executeConfig);

    /**
     * 同步执行功能
     *
     * @param executeConfig
     * @return
     */
    Object invokeFunctionSync(InstanceFunctionVo executeConfig);

    Object invokeRequestSync(InstanceFunctionVo executeConfig);

    Boolean invokeRequestAsync(InstanceFunctionVo executeConfig);

    Boolean invokeDeviceState(InstanceFunctionVo executeConfig);

    /**
     * 生成发送命令
     *
     * @param inputParam
     * @return Object
     */
    public Object generateSendCmd(InstanceFunctionInputParam inputParam);

    /**
     * 保存功能以及功能参数
     * @param functionStructure
     */
    void saveWithParams(ModelFunctionVo modelFunctionVo);

    /**
     * 更新功能以及功能参数
     * @param modelFunctionVo
     */
    void saveOrUpdateWithParams(ModelFunctionVo modelFunctionVo);

    void saveOrUpdateExecuteConfig(String instanceId, String functionId, FunctionExecuteConfig executeConfig);
}
