package org.jeecg.modules.alarm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.alarm.enums.AlarmActuatorType;
import org.jeecg.modules.alarm.vo.AlarmConfigActuatorVo;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date: 2021-01-11
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("iot_alarm_config_actuator")
public class AlarmConfigActuator implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 警告配置id
     */
    @ApiModelProperty(value = "警告配置")
    private String alarmConfigId;
    /**
     * 执行器名称
     */
    @ApiModelProperty(value = "执行器名称")
    private String name;
    /**
     * 类型
     */
    @Excel(name = "执行类型", width = 15)
    @ApiModelProperty(value = "执行类型")
    private AlarmActuatorType type;

    public AlarmConfigActuator(AlarmConfigActuatorVo actuatorVo) {
        this.id = actuatorVo.getId();
        this.name = actuatorVo.getName();
        this.type = actuatorVo.getActuatorType();
    }

    public AlarmConfigActuator(String alarmConfigId, AlarmConfigActuatorVo actuatorVo) {
        this(actuatorVo);
        this.alarmConfigId = alarmConfigId;
    }

    public AlarmConfigActuatorVo vo() {
        AlarmConfigActuatorVo actuatorVo = new AlarmConfigActuatorVo();
        actuatorVo.setId(this.id);
        actuatorVo.setName(this.name);
        actuatorVo.setActuatorType(this.type);
        return actuatorVo;
    }
}
