package org.jeecg.modules.scene.vo;

import com.zhouwr.common.enums.MessageEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-18 13:05
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MessageEventCountVo {
    private String name;
    private String code;
    private Integer level;
    private Long count;

    public MessageEventCountVo(MessageEventType messageEventType, Long count) {
        this.name = messageEventType.getName();
        this.code = messageEventType.getCode();
        this.level = messageEventType.getLevel();
        this.count = count;
    }
}
