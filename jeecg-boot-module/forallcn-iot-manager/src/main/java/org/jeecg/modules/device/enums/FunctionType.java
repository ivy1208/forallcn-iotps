package org.jeecg.modules.device.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhouwr.common.metadata.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
@JSONType(serializeEnumAsJavaBean = true)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FunctionType implements IDictEnum<String> {
    /**
     *
     */
    MOVE("move", "设备移动", Stream.of(
                    new FunctionParams("vector3", "三维向量", new Vector3Data(), "三维向量：x,y,z"),
                    new FunctionParams("type", "类型", new IntData(), "0：绝对值；1：相对值"))
            .collect(Collectors.toList())),
    SCALE("scale", "设备缩放", Stream.of(
                    new FunctionParams("vector3", "三维向量", new Vector3Data(), "三维向量：x,y,z"),
                    new FunctionParams("type", "类型", new IntData(), "0：绝对值；1：相对值"))
            .collect(Collectors.toList())),
    ROTATE("rotate", "设备旋转", Stream.of(
                    new FunctionParams("vector3", "三维向量", new Vector3Data(), "三维向量：x,y,z"),
                    new FunctionParams("angle", "角度", new DoubleData(), ""),
                    new FunctionParams("type", "类型", new IntData(), "0：绝对值；1：相对值"))
            .collect(Collectors.toList())),
    REQUEST("request", "网络请求", new ArrayList<>()),
    CONNECT_CHECK("connect_check", "联网校验", new ArrayList<FunctionParams>() {{
        add(new FunctionParams("localAddress", "本地地址", new IpPortData(), "设备端地址+端口号"));
        add(new FunctionParams("remoteAddress", "远程地址", new IpPortData(), "服务端地址+端口号"));
    }});

    @EnumValue
    private final String code;
    private final String name;
    private final List<FunctionParams> params;

    @JsonCreator
    public static FunctionType of(String value) {
        for (FunctionType functionType : FunctionType.values()) {
            if (functionType.code.equals(value)) {
                return functionType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return Arrays.stream(FunctionType.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            json.put("params", codecType.getParams());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}

@AllArgsConstructor
@NoArgsConstructor
@Data
class FunctionParams {
    private String code;
    private String name;
    private Metadata metadata;
    private String description;
}
