package org.jeecg.modules.protocol.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 传输介质
 */
@Getter
@AllArgsConstructor
public enum TransmissionMedium {
    OPTICAL_FIBER("optical_fiber", "光纤", "static/images/network/guangxian.png"),
    BROADBAND("broadband", "宽带", "static/images/network/wangxian.png"),
    BUS_485("bus_485", "485总线", "static/images/network/zongxian.png"),
    BUS_232("bus_232", "232总线", "static/images/network/zongxian.png"),
    BUS_USB("bus_usb", "USB总线", "static/images/network/usb.png"),
    BLUETOOTH("bluetooth", "蓝牙", "static/images/network/lanya.png"),
    WIFI("wifi", "WIFI", "static/images/network/wifi.png"),
    RF_24G("rf_2.4g", "2.4G射频", "static/images/network/rf.png"),
    MOBILE5G("mobile_5g", "移动5G", "static/images/network/yidong5G.png");

    private final String value;
    private final String text;
    private final String icon;
}
