package org.jeecg.modules.device.service.labelSourceHandle;

import com.zhouwr.common.utils.EnumUtil;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.jeecg.modules.device.vo.label.ViewDataVo;
import org.springframework.beans.factory.annotation.Autowired;

public class LabelInstanceDataImpl implements ILabelSourceData {
    @Autowired
    private IDeviceInstanceService instanceService;

    public LabelInstanceDataImpl() {
        this.instanceService = SpringContextUtils.getBean(IDeviceInstanceService.class);
    }

    @Override
    public String getValue(String modelId, String instanceId, ViewDataVo viewDataVo) throws ClassNotFoundException {
        String value = instanceService.getAttributeById(instanceId, viewDataVo);
        if ("enum".equals(viewDataVo.getType())) {
            value = (String) EnumUtil.getEnumProperty(value,
                    viewDataVo.getConvert().getDictTable(),
                    viewDataVo.getConvert().getDictKey(),
                    viewDataVo.getConvert().getDictValue());
        }
        return value;
    }
}
