package org.jeecg.modules.scene.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.scene.entity.Scene;
import org.jeecg.modules.scene.vo.DeployConfig;

/**
 * @Description: 场景管理
 * @Author: jeecg-boot
 * @Date: 2020-06-04
 * @Version: V1.0
 */
public interface ISceneService extends IService<Scene> {
    public DeployConfig buildDeploy(Scene scene);

    /**
     * 根据场景Id获取当前默认场景
     * @param id 场景Id
     * @param username
     * @return
     */
    Scene getCurrentScene(String id, String username);

    /**
     * 更新模型属性
     * @param deployConfig
     * @return
     */
    Boolean updateModelAttribute(DeployConfig deployConfig);
}
