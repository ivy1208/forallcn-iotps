package org.jeecg.modules.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.message.entity.MessageEvent;

/**
 * @Description: 消息事件
 * @Author: jeecg-boot
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface MessageEventMapper extends BaseMapper<MessageEvent> {

}
