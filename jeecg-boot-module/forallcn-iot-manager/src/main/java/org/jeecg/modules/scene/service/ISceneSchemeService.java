package org.jeecg.modules.scene.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.scene.entity.SceneScheme;
import org.jeecg.modules.scene.vo.SchemeCountVo;

import java.util.List;

/**
 * @Description: 场景方案
 * @Author: jeecg-boot
 * @Date: 2020-06-05
 * @Version: V1.0
 */
public interface ISceneSchemeService extends IService<SceneScheme> {
    /**
     * 方案下设备统计按场景 ID
     *
     * @param sceneId
     * @return
     */
    public List<SchemeCountVo> schemeCountListBySceneId(String sceneId);
}
