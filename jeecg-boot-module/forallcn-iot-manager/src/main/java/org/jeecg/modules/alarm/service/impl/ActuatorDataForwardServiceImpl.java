package org.jeecg.modules.alarm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.alarm.entity.ActuatorDataForward;
import org.jeecg.modules.alarm.mapper.ActuatorDataForwardMapper;
import org.jeecg.modules.alarm.service.IActuatorDataForwardService;
import org.springframework.stereotype.Service;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-03-15 21:43
 * @version：1.0
 **/
@Service
@Slf4j
public class ActuatorDataForwardServiceImpl extends ServiceImpl<ActuatorDataForwardMapper, ActuatorDataForward> implements IActuatorDataForwardService {

}
