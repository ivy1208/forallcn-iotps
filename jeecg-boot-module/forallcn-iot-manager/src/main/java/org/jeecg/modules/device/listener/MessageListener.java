package org.jeecg.modules.device.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhouwr.common.constant.MqttTopics;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import com.zhouwr.common.enums.DataRwAuthor;
import com.zhouwr.common.enums.DeviceInstanceState;
import com.zhouwr.common.enums.ReportMode;
import com.zhouwr.common.message.DeviceHeartMessage;
import com.zhouwr.common.message.DeviceOfflineMessage;
import com.zhouwr.common.message.DeviceOnlineMessage;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.alarm.service.IAlarmConfigService;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.listener.event.*;
import org.jeecg.modules.device.service.IDataReportService;
import org.jeecg.modules.device.service.IDeviceInstanceService;
import org.jeecg.modules.message.entity.MessageEvent;
import org.jeecg.modules.message.service.IMessageEventService;
import org.jeecg.modules.network.network.mqtt.IMqttSender;
import org.jeecg.modules.scene.service.ISceneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-07 12:43
 * @version：1.0
 **/
// TODO 改用EventBus实现消息总线 2022/03/21 zhouwenrong
@Slf4j
@Component
public class MessageListener {
    private static MessageListener listener;
    @Autowired
    private ISceneService sceneService;
    @Resource
    private IMessageEventService eventService;
    @Resource
    private IDeviceInstanceService instanceService;
    @Resource
    private IDataReportService dataReportService;
    @Resource
    private IAlarmConfigService alarmConfigService;
    @Resource
    private IMqttSender mqttSender;
    ExecutorService threadPool;

    @Async
    @EventListener(InstanceReceiveDataEvent.class)
    public void deviceStateUpdateEven(InstanceReceiveDataEvent event) {
        /* 检查执行告警 */
//        listener.alarmConfigService.queryTrigger(event.getInstanceId(), data);

        listener.threadPool.execute(() -> {
            /* 校验并构建数据节点的数据 */
            final JSONObject data = listener.dataReportService.verifyBuildDataNode(event.getInstanceId(), event.getDataMap());
            /* 保存数据 */
            String reportDataId = listener.dataReportService.saveData(event.getInstanceId(), data, ReportMode.REPORT);
            /* 按实例ID查询告警配置 */
            listener.alarmConfigService.invokeAlarmConfig(event.getInstanceId(), reportDataId, data);
        });
        listener.threadPool.execute(() -> {
            /* mqtt发送消息 */
            final List<InstanceDataStructure> dataStructures = listener.instanceService.listInstanceDataLatest(event.getInstanceId())
                    .stream()
                    .filter(instanceDataStructure -> instanceDataStructure.getRwAuthor().equals(DataRwAuthor.READ))
                    .collect(Collectors.toList());
            listener.mqttSender.sendToMqtt(MqttTopics.DATA_RECEIVE.getSceneTopic().apply(instanceService.getSceneId(event.getInstanceId())), JSON.toJSONString(dataStructures));
        });
    }

    @Async
    @EventListener(InstanceNotFindEvent.class)
    public void deviceStateUpdateEven(InstanceNotFindEvent event) {
        log.info("{}, {}", event.getEventType().getName(), event);
        final DeviceInstance instance = listener.instanceService.getById(event.getInstanceId());
        final MessageEvent messageEvent = new MessageEvent(
                event.getInstanceId(),
                instance != null ? instance.getName() : "",
                event.getEventType(),
                0
        );
        listener.eventService.save(messageEvent);
    }

    @Async
    @EventListener(DeviceHeartMessage.class)
    public void deviceStateUpdateEven(DeviceHeartMessage message) {
        log.info("{}, {}", message.getMessageType().name(), message);
        // mqtt发送消息
        listener.mqttSender.sendToMqtt(MqttTopics.DEVICE_ONLINE.getSceneTopic().apply(instanceService.getSceneId(message.getDeviceId())),
                JSON.toJSONString(message));
    }

    @Async
    @EventListener(DeviceOnlineMessage.class)
    public void deviceStateUpdateEven(DeviceOnlineMessage message) {
        log.info("{}, {}", message.getMessageType().name(), message);
        listener.instanceService.getInstanceChildren(message.getDeviceId(), true)
                .forEach(deviceInstance ->
                        // mqtt发送消息
                        listener.mqttSender.sendToMqtt(MqttTopics.DEVICE_ONLINE.getSceneTopic().apply(instanceService.getSceneId(message.getDeviceId())),
                                JSON.toJSONString(message))
                );
        listener.instanceService.batchUpdateInstanceState(message.getDeviceId(), DeviceInstanceState.ONLINE);
    }

    @Async
    @EventListener(DeviceOfflineMessage.class)
    public void deviceStateUpdateEven(DeviceOfflineMessage message) {
        log.info("{}, {}", message.getMessageType().name(), message);
        listener.instanceService.getInstanceChildren(message.getDeviceId(), true)
                .forEach(deviceInstance ->
                    // mqtt发送消息
                    listener.mqttSender.sendToMqtt(MqttTopics.DEVICE_OFFLINE.getSceneTopic().apply(instanceService.getSceneId(message.getDeviceId())),
                            JSON.toJSONString(message))
                );
        listener.instanceService.batchUpdateInstanceState(message.getDeviceId(), DeviceInstanceState.OFFLINE);
    }

    @Async
    @EventListener(NetworkServerExceptionEvent.class)
    public void deviceStateUpdateEven(NetworkServerExceptionEvent event) {
        log.warn("{}, {}", event.getEventType().getName(), event);
        final MessageEvent messageEvent = new MessageEvent(
                event.getServerName(),
                event.getEventType(),
                0
        );
        listener.eventService.save(messageEvent);
    }

    @Async
    @EventListener(InstanceNotActionEvent.class)
    public void deviceStateUpdateEven(InstanceNotActionEvent event) {
        log.warn("{}, {}", event.getEventType().getName(), event);
        final MessageEvent messageEvent = new MessageEvent(
                event.getInstanceId(),
                event.getEventType(),
                0
        );
        listener.eventService.save(messageEvent);
    }

    /**
     * 初始化操作
     */
    @PostConstruct
    public void init() {
        listener = this;
        listener.alarmConfigService = this.alarmConfigService;
        listener.dataReportService = this.dataReportService;
        listener.eventService = this.eventService;
        listener.instanceService = this.instanceService;
        listener.mqttSender = this.mqttSender;
        // 创建固定大小的线程池
        listener.threadPool = Executors.newFixedThreadPool(10);
    }
}
