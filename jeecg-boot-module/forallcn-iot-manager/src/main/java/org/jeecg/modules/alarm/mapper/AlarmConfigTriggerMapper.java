package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface AlarmConfigTriggerMapper extends BaseMapper<AlarmConfigTrigger> {

    /**
     * 按配置ID删除
     * @param configId
     */
    @Delete("delete from iot_alarm_config_trigger where 1=1 and alarm_config_id = #{configId}")
    void deleteByConfigId(@Param("configId") String configId);

}
