package org.jeecg.modules.device.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @Descriprion：设备标签关闭方式
 * @Author: 周文荣
 * @Date: 2021/12/10 12:01
 * @Version: V1.0
 */
@AllArgsConstructor
@Getter
public enum DeviceLabelCloseMethod implements IDictEnum<String> {
    /**
     *
     */
    ALWAYS_CLOSE("永久可手动关闭", "always_close"),
    ALWAYS_NO_CLOSE("永久不可手动关闭", "always_no_close"),
    DELAY_CLOSE("延时可提前关闭", "delay_close"),
    DELAY_NO_CLOSE("延时不可提前关闭", "delay_no_close");

    private final String text;
    @EnumValue
    private final String value;

    @JsonCreator
    public static DeviceLabelCloseMethod of(String code) {
        for (DeviceLabelCloseMethod method : DeviceLabelCloseMethod.values()) {
            if (method.value.equals(code)) {
                return method;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + code);
    }

    public static JSONArray toArray() {
        return Arrays.stream(DeviceLabelCloseMethod.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }
}