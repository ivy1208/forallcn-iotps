package org.jeecg.modules.alarm.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.enums.DataRwAuthor;
import com.zhouwr.common.enums.ModelDataType;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import org.jeecg.modules.alarm.entity.AlarmRecord;
import org.jeecg.modules.alarm.mapper.AlarmRecordMapper;
import org.jeecg.modules.alarm.service.IAlarmRecordService;
import org.jeecg.modules.device.entity.DeviceData;
import org.jeecg.modules.device.mapper.DeviceDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Description: 告警记录
 * @Author: jeecg-boot
 * @Date: 2021-01-19
 * @Version: V1.0
 */
@Service
public class AlarmRecordServiceImpl extends ServiceImpl<AlarmRecordMapper, AlarmRecord> implements IAlarmRecordService {
    @Autowired
    private AlarmRecordMapper alarmRecordMapper;
    @Autowired
    private DeviceDataMapper deviceDataMapper;

    @Override
    public AlarmRecord save(AlarmConfig alarmConfig, Map<String, Object> dataMap, String reportDataId) {
        AlarmRecord record = new AlarmRecord();
        record.setReportDataId(reportDataId);
        record.setAlarmConfigId(alarmConfig.getId());
        record.setInstanceId(alarmConfig.getInstanceId());
        record.setAlarmParam(dataMap.toString());
        record.setIsNeedDeal(alarmConfig.getIsDeal());
        record.setAlarmLevel(alarmConfig.getAlarmLevel());
        this.save(record);
        return record;
    }

    @Override
    public void recordExtents(IPage<AlarmRecord> pageList) {
        pageList.getRecords().forEach(record -> {
            JSONArray dataArray = new JSONArray();
            String instanceId1 = record.getInstanceId();
            JSONObject.parseObject(record.getAlarmParam()).forEach((key, value) -> {
                // 根据设备实例id，获取设备的数据节点
                List<DeviceData> deviceDataList = deviceDataMapper.select(instanceId1, key);
                if (deviceDataList != null && deviceDataList.size() == 1) {
                    JSONObject dataJson = new JSONObject();
                    DeviceData deviceData = deviceDataList.get(0);
                    String unit = JSONObject.parseObject(deviceData.getValueType()).getString("unit");
                    // 数据名称，数据值
                    dataJson.put("name", deviceData.getName());
                    dataJson.put("code", deviceData.getCode());
                    dataJson.put("type", ModelDataType.of(deviceData.getType()).getName() + "(" + deviceData.getType() + ")");
                    dataJson.put("authority", DataRwAuthor.of(deviceData.getRwAuthor()).getName() + "(" + deviceData.getRwAuthor() + ")");
                    dataJson.put("value", value + unit);
                    dataArray.add(dataJson);
                }
            });
            record.setAlarmParamMap(dataArray);
        });
    }
}
