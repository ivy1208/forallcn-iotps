package org.jeecg.modules.alarm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;
import org.jeecg.modules.alarm.mapper.AlarmConfigTriggerMapper;
import org.jeecg.modules.alarm.service.IAlarmTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date: 2021-01-11
 * @Version: V1.0
 */
@Service
public class AlarmTriggerServiceImpl extends ServiceImpl<AlarmConfigTriggerMapper, AlarmConfigTrigger> implements IAlarmTriggerService {

    @Autowired
    private AlarmConfigTriggerMapper triggerMapper;

    @Override
    public List<AlarmConfigTrigger> selectByMainId(String configId) {
//		return triggerMapper.selectByMainId(configId);
        return null;
    }
}
