package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.device.entity.DeviceLabel;
import org.jeecg.modules.device.vo.label.DeviceLabelVo;

import java.util.List;

/**
 * 标签i系设备之服务
 *
 * @author zhouwenrong
 * @Description: 设备标签
 * @Version: V1.0
 * @date 2022/01/06
 */
public interface IDeviceLabelService extends IService<DeviceLabel> {

    /**
     * 通过代码
     *
     * @param code 代码
     * @return {@link DeviceLabel}
     */
    DeviceLabel getByCode(String code);

    /**
     * 选择通过主id
     *
     * @param mainId 主要id
     * @return {@link List}<{@link DeviceLabel}>
     */
    List<DeviceLabel> selectByMainId(String mainId);

    /**
     * 通过模型id列表
     *
     * @param modelId 模型id
     * @return {@link List}<{@link DeviceLabel}>
     */
    List<DeviceLabel> listByModelId(String modelId);

    /**
     * 通过实例id列表
     *
     * @param instanceId 实例id
     * @return {@link List}<{@link DeviceLabel}>
     */
    List<DeviceLabel> listByInstanceId(String instanceId);

    /**
     * 标签配置
     *
     * @param modelId        模型id
     * @param instanceId     实例id
     * @param defaultTrigger
     * @return {@link List}<{@link DeviceLabel}>
     */
    List<DeviceLabel> labelConfig(String modelId, String instanceId, String defaultTrigger);

    /**
     * 救主
     *
     * @param deviceLabel 设备标签
     */
    void saveMain(DeviceLabelVo deviceLabel);

    /**
     * 更新主要
     *
     * @param deviceLabelVo 设备标签签证官
     */
    void updateByMain(DeviceLabelVo deviceLabelVo);

    /**
     * 通过id获取主要
     *
     * @param id id
     * @return {@link DeviceLabelVo}
     */
    DeviceLabelVo getMainById(String id);
}