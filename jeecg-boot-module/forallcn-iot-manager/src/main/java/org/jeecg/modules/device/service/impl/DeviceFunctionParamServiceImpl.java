package org.jeecg.modules.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouwr.common.enums.FunctionParamDirection;
import com.zhouwr.common.enums.FunctionParamInputMode;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.device.entity.DeviceFunctionParam;
import org.jeecg.modules.device.mapper.DeviceFunctionParamMapper;
import org.jeecg.modules.device.service.IDeviceFunctionParamService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 功能定义
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
@Slf4j
@Service
public class DeviceFunctionParamServiceImpl extends ServiceImpl<DeviceFunctionParamMapper, DeviceFunctionParam> implements IDeviceFunctionParamService {

    @Override
    public List<DeviceFunctionParam> getFunctionParams(String functionId) {
        return this.lambdaQuery().eq(DeviceFunctionParam::getFunctionId, functionId).list();
    }

    @Override
    public List<DeviceFunctionParam> getFunctionParams(String functionId, FunctionParamDirection paramDirection) {
        return this.lambdaQuery()
                .eq(DeviceFunctionParam::getFunctionId, functionId)
                .eq(DeviceFunctionParam::getDirection, paramDirection.getCode())
                .list();
    }

    @Override
    public List<DeviceFunctionParam> getFunctionParams(String functionId, FunctionParamInputMode inputMode) {
        return this.lambdaQuery()
                .eq(DeviceFunctionParam::getFunctionId, functionId)
                .eq(DeviceFunctionParam::getInputMode, inputMode.getCode())
                .list();
    }
}
