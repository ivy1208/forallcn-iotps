package org.jeecg.modules.alarm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.alarm.vo.ActuatorDataForwardVo;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-15 21:21
 * @version：1.0
 **/
@ApiModel(value = "ActuatorDataForward对象", description = "执行器数据转发")
@Data
@NoArgsConstructor
@TableName("iot_actuator_data_forward")
public class ActuatorDataForward implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "网络服务")
    private String networkServiceId;

    @ApiModelProperty(value = "接收")
    private String forwardReceivers;

    public ActuatorDataForward(ActuatorDataForwardVo dataForwardVo) {
        this();
        this.networkServiceId = dataForwardVo.getNetworkServiceId();
        this.forwardReceivers = dataForwardVo.getForwardReceivers();
    }

    public ActuatorDataForward(String id, ActuatorDataForwardVo dataForwardVo) {
        this(dataForwardVo);
        this.id = id;
    }

    public ActuatorDataForwardVo vo() {
        ActuatorDataForwardVo vo = new ActuatorDataForwardVo();
        vo.setId(this.id);
        vo.setNetworkServiceId(this.networkServiceId);
        vo.setForwardReceivers(this.forwardReceivers);
        return vo;
    }
}
