package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhouwr.common.device.vo.DeviceFunctionParamVo;
import com.zhouwr.common.device.vo.function.FunctionParamVo;
import com.zhouwr.common.enums.FunctionParamDirection;
import com.zhouwr.common.enums.FunctionParamInputMode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description: 设备功能输入输出参数
 * @author: zhouwr
 * @create: 2021-01-01 15:13
 * @version：1.0
 **/
@ApiModel(value = "device_function_param对象", description = "设备功能输入输出参数")
@Data
@NoArgsConstructor
@TableName("iot_device_function_param")
public class DeviceFunctionParam implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;

    private java.lang.String functionId;

    private java.lang.String dataId;

    private FunctionParamDirection direction;

    private FunctionParamInputMode inputMode;

    private Integer sort;

    public DeviceFunctionParam(String functionVoId, FunctionParamVo functionParamVo) {
        this.functionId = functionVoId;
        this.dataId = functionParamVo.getId();
        this.direction = functionParamVo.getDirection();
        this.inputMode = functionParamVo.getInputMode();
        this.sort = functionParamVo.getSort();
    }

    public DeviceFunctionParamVo vo() {
        return new DeviceFunctionParamVo(
                this.id,
                this.functionId,
                this.dataId,
                "",
                this.direction,
                this.inputMode,
                this.sort
        );
    }
}
