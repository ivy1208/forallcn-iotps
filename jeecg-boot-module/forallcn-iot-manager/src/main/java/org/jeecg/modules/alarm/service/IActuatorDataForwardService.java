package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.alarm.entity.ActuatorDataForward;

/**
 * @author zhouwenrong
 */
public interface IActuatorDataForwardService extends IService<ActuatorDataForward> {
}
