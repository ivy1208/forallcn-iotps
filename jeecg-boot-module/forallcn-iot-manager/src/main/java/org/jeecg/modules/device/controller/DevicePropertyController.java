package org.jeecg.modules.device.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.device.entity.DeviceData;
import org.jeecg.modules.device.service.IDeviceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Package: org.jeecg.modules.device.controller
 * @Descriprion
 * @Author: zhouwr
 * @Date: 2021/12/21 15:19
 * @Version: V1.0
 */
@Api(tags = "设备属性")
@RestController
@RequestMapping("/device/property")
@Slf4j
public class DevicePropertyController extends JeecgController<DeviceData, IDeviceDataService> {
    @Autowired
    private IDeviceDataService propertyService;

    /**
     * 获取设备模型的可读属性
     *
     * @param modelId 模型id
     * @return
     */
    @ApiOperation(value = "获取设备模型的可读属性")
    @GetMapping(value = "/listReadByModel")
    public Result<?> listReadByModel(@RequestParam(name = "modelId", required = true) String modelId) {
        List<DeviceData> list = propertyService.listByModelId(modelId)
                .stream()
                .filter(property -> property.getRwAuthor().contains("r"))
                .collect(Collectors.toList());
        return Result.ok(list);
    }
}