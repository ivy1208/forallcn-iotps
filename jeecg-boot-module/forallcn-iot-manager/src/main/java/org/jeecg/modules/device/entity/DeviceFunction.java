package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhouwr.common.device.vo.DeviceFunctionVo;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 功能定义
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
@ApiModel(value = "device_model对象", description = "设备模型")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("iot_device_function")
public class DeviceFunction implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 设备模型
     */
    @ApiModelProperty(value = "设备模型")
    private java.lang.String deviceModelBy;
    /**
     * 标识
     */
    @Excel(name = "标识", width = 15)
    @ApiModelProperty(value = "标识")
    private java.lang.String code;
    /**
     * 功能名称
     */
    @Excel(name = "功能名称", width = 20)
    @ApiModelProperty(value = "功能名称")
    private java.lang.String name;
    /**
     * 功能类型
     */
    @Excel(name = "功能类型", width = 20)
    @ApiModelProperty(value = "功能类型")
    @Dict(dicCode = "dm_func_type")
    private java.lang.String type;
    /**
     * 是否同步
     */
    @Excel(name = "是否同步", width = 15)
    @ApiModelProperty(value = "是否同步")
    @Dict(dicCode = "dm_is_sync")
    private java.lang.Boolean isSync;

    /**
     * 描述
     */
    @Excel(name = "描述", width = 150)
    @ApiModelProperty(value = "描述")
    private java.lang.String description;

    public DeviceFunction(ModelFunctionVo modelFunctionVo) {
        this.id = modelFunctionVo.getId();
        this.deviceModelBy = modelFunctionVo.getModelId();
        this.code = modelFunctionVo.getCode();
        this.name = modelFunctionVo.getName();
        this.type = modelFunctionVo.getType();
        this.isSync = modelFunctionVo.getIsSync();
    }

    public DeviceFunctionVo vo() {
        return new DeviceFunctionVo(
                this.id,
                this.deviceModelBy,
                this.code,
                this.name,
                this.type,
                this.isSync
        );
    }

    /**
     * 从Vo类中获取
     * @param functionVo
     * @return
     */
    public DeviceFunction(DeviceFunctionVo functionVo) {
        this.id = functionVo.getId();
        this.deviceModelBy = functionVo.getDeviceModelBy();
        this.code = functionVo.getCode();
        this.name = functionVo.getName();
        this.type = functionVo.getType();
        this.isSync = functionVo.getIsSync();
    }
}
