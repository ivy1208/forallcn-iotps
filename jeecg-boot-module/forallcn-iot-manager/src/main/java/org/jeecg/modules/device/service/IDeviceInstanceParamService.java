package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.device.vo.function.FunctionParamVo;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import org.jeecg.modules.device.entity.DeviceInstanceParam;

import java.util.List;

/**
 * @author zhouwr
 */
public interface IDeviceInstanceParamService  extends IService<DeviceInstanceParam> {
    /**
     * 根据实例id、功能id，获取实例参数
     * @param instanceId 实例id
     * @param functionId 功能id
     * @return
     */
    List<DeviceInstanceParam> getInstanceParams(String instanceId, String functionId);

    DeviceInstanceParam getInstanceParams(String instanceId, String functionId, String dataId);

    /**
     * 根据实例id，获取实例参数
     * @param instanceId
     * @return
     */
    List<DeviceInstanceParam> getInstanceParamsByInstanceId(String instanceId);

    /**
     * 根据功能id，获取实例参数
     * @param functionId
     * @return
     */
    List<DeviceInstanceParam> getInstanceParamsByFunctionId(String functionId);

    /**
     * 根据实例id删除
     * @param instanceId
     * @return boolean
     */
    boolean removeByInstanceId(String instanceId);

    /**
     * 批量保存实例参数
     * @param instanceId
     * @param extendParams
     */
    void saveParamBatch(String instanceId, String functionId, List<FunctionParamVo> extendParams);

    /**
     * 按功能Ids查询
     * @param functionIds
     * @return
     */
    List<DeviceInstanceParam> queryByFunctionIds(List<String> functionIds);

    /**
     * 更新实例参数，先删除实例参数，根据实例id
     * @param extendParams
     * @see InstanceFunctionVo
     * @return
     */
    boolean updateInstanceParams(List<InstanceFunctionVo> extendParams);
}
