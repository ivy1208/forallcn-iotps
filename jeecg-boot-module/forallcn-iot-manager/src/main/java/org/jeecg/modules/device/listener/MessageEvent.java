package org.jeecg.modules.device.listener;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.MessageEventType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class MessageEvent {
    protected Date eventTime = new Date();
    protected String message;
    protected String context;

    public abstract MessageEventType getEventType();
}
