package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.alarm.entity.ActuatorInvokeFunction;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-16 23:10
 * @version：1.0
 **/
public interface ActuatorInvokeFunctionMapper extends BaseMapper<ActuatorInvokeFunction> {
}
