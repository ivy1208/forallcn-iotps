package org.jeecg.modules.scene.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jeecg.modules.device.entity.DeviceInstance;

import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@NoArgsConstructor
public class DeviceDataVO extends DeviceInstance implements Serializable {
    private static final long serialVersionUID = 1L;

    List<InstanceDataStructure> deviceDatas;

    public DeviceDataVO(DeviceInstance instance) {
        super();
        super.setId(instance.getId());
        super.setParentBy(instance.getParentBy());
        super.setCode(instance.getCode());
        super.setName(instance.getName());
    }

    public DeviceDataVO(DeviceInstance instance, List<InstanceDataStructure> deviceDatas) {
        this(instance);
        this.deviceDatas = deviceDatas;
    }
}
