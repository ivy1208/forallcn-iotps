package org.jeecg.modules.scene.service;

import org.jeecg.modules.scene.entity.Scene;
import org.jeecg.modules.scene.vo.MessageEventCountVo;

import java.util.List;

public interface IDisplayService {
    Scene getCurrentScene(String id);

    /**
     * 统计消息事件信息，根据场景id
     *
     * @param sceneId
     */
    public List<MessageEventCountVo> statMessageEvent(String sceneId);
}
