package org.jeecg.modules.protocol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.modules.device.entity.DeviceModel;
import org.jeecg.modules.device.mapper.DeviceModelMapper;
import org.jeecg.modules.protocol.entity.ProtocolData;
import org.jeecg.modules.protocol.mapper.ProtocolDataMapper;
import org.jeecg.modules.protocol.service.IProtocolDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @Description: 数据解析协议
 * @Author: jeecg-boot
 * @Date: 2020-04-19
 * @Version: V1.0
 */
@Slf4j
@Service
public class ProtocolDataServiceImpl extends ServiceImpl<ProtocolDataMapper, ProtocolData> implements IProtocolDataService {

    @Autowired
    private DeviceModelMapper deviceModelMapper;

    @Value("${jeecg.path.upload}")
    private String uploadPath;

    @Override
    @Cacheable(value = CacheConstant.IOT_MODEL_DATA_PROTOCOL_CACHE)
    public ProtocolData getDeviceDataProtocol(String deviceModelId) {
        log.info("无缓存ProtocolData的时候调用这里！");
        DeviceModel model = deviceModelMapper.selectById(deviceModelId);
        String dataProtoclId = model.getDataProtocolBy();
        return this.getById(dataProtoclId);
    }

    public static void main(String[] args) throws Exception {
        File file = new File("/Users/zhouwenrong/IdeaProjects/upload/forllcn-iot/upFiles/temp/data-protocol-1.0.jar");//jar包的路径
        log.info("jar包的路径: {}", file.getAbsolutePath());
        URL url = file.toURI().toURL();
        ClassLoader loader = new URLClassLoader(new URL[]{url});//创建类加载器
        Class<?> cls = loader.loadClass("com.zhouwr.protocol.DataProtocolProvider");//加载指定类，注意一定要带上类的包名
        log.info("加载数据解析类：{}", cls.toString());
    }
}
