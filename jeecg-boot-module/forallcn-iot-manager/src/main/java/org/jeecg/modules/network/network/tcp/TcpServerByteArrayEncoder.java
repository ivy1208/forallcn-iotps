package org.jeecg.modules.network.network.tcp;

import com.zhouwr.common.utils.HexConvertUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-04 10:27
 * @version：1.0
 **/
@Slf4j
@ChannelHandler.Sharable
public class TcpServerByteArrayEncoder extends MessageToByteEncoder<byte[]> {

    @Override
    protected void encode(ChannelHandlerContext ctx, byte[] msg, ByteBuf out) throws Exception {
        log.info("TcpServerByteArrayEncoder >>>>>> " + HexConvertUtil.BinaryToHexString(msg));
        out.writeBytes(msg);
//        Thread.sleep(1000);
    }
}
