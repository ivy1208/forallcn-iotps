package org.jeecg.modules.scene.vo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.device.entity.DeviceInstance;
import org.jeecg.modules.device.entity.DeviceModel;
import com.zhouwr.common.enums.DeviceInstanceState;
import org.jeecg.modules.device.enums.DeviceType;
import org.jeecg.modules.scene.entity.Scene;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: jeecg-boot-iotcp
 * @description: 场景及设备部署配置类
 * @author: zhouwr
 * @create: 2020-11-03 13:14
 * @version：1.0
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@NoArgsConstructor
@AllArgsConstructor
public class DeployConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String parentBy;
    private String name;
    private String code;
    private DeviceType type;
    private String schemeId;
    private String schemeName;
    private DeviceInstanceState state;
    private String modelId;
    private String modelName;
    private String modelFile;
    private String modelScale;
    private String modelZoom;
    private String latlon;
    private String address;
    @JsonIgnore
    private String modelAttribute;  // 模型属性str
    private ModelAttribute attribute;  // 模型属性
    private List<InstanceDataStructure> instanceDatas;  // 数据节点

    private List<DeployConfig> deviceInstances = new ArrayList<>();

    // 方案统计
    private List<SchemeCountVo> schemes = new ArrayList<>();

    /**
     * 模型类型
     */
    private JSONArray modelTypes = new JSONArray();


    public DeployConfig(Scene scene, List<SchemeCountVo> schemeCounts) {
        this.id = scene.getId();
        this.name = scene.getName();
        this.code = scene.getCode();
        this.type = DeviceType.SCENE;
        this.modelFile = scene.getModelFiles();
        this.modelScale = scene.getModelScale();
        this.modelZoom = scene.getSoftwareZoom();
        this.latlon = scene.getLatLon();
        this.address = scene.getAddress();
        /* 如果场景配置了模型文件，还没有做场景部署时，场景的模型属性是没有的，所以需要判断空 */
        if (oConvertUtils.isNotEmpty(scene.getModelAttribute())) {
            this.attribute = new ModelAttribute(scene.getModelAttribute());
        }
//        this.modelTypes = DeviceType.toArray();

        this.schemes = schemeCounts;
    }

    /**
     * 追加模型对象
     *
     * @param device         设备实例
     * @param model          设备模型
     * @param dataStructures 设备数据节点
     * @return 模型对象
     */
    public DeployConfig appendObject(DeviceInstance device, DeviceModel model, List<InstanceDataStructure> dataStructures) {
        DeployConfig object = new DeployConfig();
        object.id = device.getId();
        object.name = device.getName();
        object.code = device.getCode();
        object.schemeId = device.getSceneSchemeBy();
        object.state = device.getStatus();
//        object.modelId = model.getId();
        object.modelFile = model.getThreeModelFile();
//        object.type = model.getType();
        object.attribute = new ModelAttribute(device.getModelAttribute());

        object.instanceDatas = dataStructures;

        this.deviceInstances.add(object);

        return this;
    }

    /**
     * @return
     * @JsonIgnore 在json序列化时将java bean中的一些属性忽略掉，序列化和反序列化都受影响
     */
    @JsonIgnore
    public String getSceneAttribute() {
        JSONObject sceneAttr = new JSONObject();
        sceneAttr.put("position", this.attribute.getPosition());
        sceneAttr.put("rotation", this.attribute.getRotation());
        sceneAttr.put("scale", this.attribute.getScale());
        sceneAttr.put("ambientLight", this.attribute.getAmbientLight());
        sceneAttr.put("directionalLight", this.attribute.getDirectionalLight());
        return sceneAttr.toJSONString();
    }

    @JsonIgnore
    public String getDeviceAttribute() {
        JSONObject deviceAttr = new JSONObject();
        deviceAttr.put("position", this.attribute.getPosition());
        deviceAttr.put("rotation", this.attribute.getRotation());
        deviceAttr.put("scale", this.attribute.getScale());
        return deviceAttr.toJSONString();
    }

    public ModelAttribute getAttribute() {
        if (this.attribute == null)
            return new ModelAttribute(this.modelAttribute);
        else {
            return this.attribute;
        }
    }
}
