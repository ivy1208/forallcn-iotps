package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import com.zhouwr.common.network.DeviceMessageCodec;
import org.jeecg.modules.device.entity.*;
import org.jeecg.modules.device.vo.label.ViewDataVo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 模型服务i系设备之
 *
 * @author zhouwenrong
 * @Description: 设备模型
 * @Author: zhouwenrong
 * @Version: V1.0
 * @date 2022/01/06
 */
public interface IDeviceModelService extends IService<DeviceModel> {

    /**
     * 救主
     * 添加一对多
     *
     * @param model              模型
     * @param deviceDataList     设备数据表
     * @param deviceFunctionList 设备功能列表
     * @param deviceEventList    设备事件列表
     * @param deviceLabelList    设备标签列表
     */
    public void saveMain(DeviceModel model, List<DeviceData> deviceDataList, List<DeviceFunction> deviceFunctionList, List<DeviceEvent> deviceEventList, List<DeviceLabel> deviceLabelList);

    /**
     * 更新主要
     * 修改一对多
     *
     * @param model              模型
     * @param deviceDataList     设备数据表
     * @param deviceFunctionList 设备功能列表
     * @param deviceEventList    设备事件列表
     * @param deviceLabelList    设备标签列表
     */
    public void updateMain(DeviceModel model, List<DeviceData> deviceDataList, List<DeviceFunction> deviceFunctionList, List<DeviceEvent> deviceEventList, List<DeviceLabel> deviceLabelList);

    /**
     * 德尔主要
     * 删除一对多
     *
     * @param id id
     */
    public void delMain(String id);

    /**
     * 德尔批主要
     * 批量删除一对多
     *
     * @param idList id列表
     */
    public void delBatchMain(Collection<? extends Serializable> idList);

    /**
     * 获得协议
     * 根据模型id，获取模型通讯协议
     *
     * @param modelId 模型id
     * @return {@link DeviceMessageCodec}
     */
    DeviceMessageCodec getProtocol(String modelId);

    /**
     * 克隆
     *
     * @param id id
     */
    void clone(String id);

    /**
     * 通过id获取属性
     *
     * @param modelId    模型id
     * @param viewDataVo 显示数据vo {@link ViewDataVo}
     * @return {@link String}
     */
    String getAttributeById(String modelId, ViewDataVo viewDataVo);

    /**
     * 获取功能参数配置
     * @param modelId
     * @return
     */
    List<ModelFunctionVo> getFunctionParams(String modelId);

    /**
     * 获取功能参数配置
     * @param instanceId
     * @return
     */
    ModelFunctionVo getFunctionParam(String instanceId);
}
