package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.alarm.entity.ActuatorNotification;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-17 17:23
 * @version：1.0
 **/
public interface ActuatorNotificationMapper extends BaseMapper<ActuatorNotification> {

}
