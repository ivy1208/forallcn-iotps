package org.jeecg.modules.alarm.controller;

import com.zhouwr.common.enums.MessageEventType;
import com.zhouwr.common.enums.RuleCondition;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.alarm.service.IAlarmConfigService;
import org.jeecg.modules.alarm.service.IAlarmTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 告警配置
 * @Author: jeecg-boot
 * @Date: 2021-01-11
 * @Version: V1.0
 */
@Api(tags = "告警配置")
@RestController
@RequestMapping("/alarm/trigger")
@Slf4j
public class AlarmTriggerController {
    @Autowired
    private IAlarmConfigService alarmConfigService;
    @Autowired
    private IAlarmTriggerService alarmTriggerService;

    @ApiOperation(value = "告警配置-列出规则条件", notes = "告警配置-列出规则条件")
    @GetMapping(value = "/listRuleCondition")
    public Result<?> listRuleCondition() {
        return Result.ok(RuleCondition.toJSONArray());
    }

    @ApiOperation(value = "告警配置-列表消息事件类型", notes = "告警配置-列表消息事件类型")
    @GetMapping(value = "/listMessageEventType")
    public Result<?> listMessageEventType() {
        return Result.ok(MessageEventType.toJSONArray());
    }
}
