package org.jeecg.modules.network.network.mqtt;

import com.zhouwr.common.enums.NetworkType;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.network.entity.NetworkService;
import com.zhouwr.common.network.Network;

/**
 * @author zhouwenrong
 */
@Slf4j
public class NettyMqttSubscriber implements Network {

    private EventLoopGroup mainGroup;
    private EventLoopGroup workerGroup;
    private ServerBootstrap server;
    private ChannelFuture future;
    private NetworkService properies;

    public NettyMqttSubscriber() {
        super();
    }

    public NettyMqttSubscriber(NetworkService properies) {
        super();
        this.properies = properies;
    }

    public void initMqttSubscriber() {
        log.info("mqtt subscriber starting...");
        String serviceId = properies.getId();
        int servicePort = properies.getPort();

    }

    @Override
    public String getId() {
        return this.properies.getId();
    }

    @Override
    public NetworkType getType() {
        return NetworkType.TCP_SERVER;
    }

    @Override
    public void shutdown() {
        boolean isStoped = true;


        log.info("tcp-server[{}]服务已经停止...", properies.getPort());
    }

    @Override
    public boolean isAlive() {
        return this.server != null;
    }

    @Override
    public boolean isAutoReload() {
        return false;
    }

}
