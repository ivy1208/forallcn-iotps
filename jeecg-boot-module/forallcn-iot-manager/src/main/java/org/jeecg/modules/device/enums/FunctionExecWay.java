package org.jeecg.modules.device.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
@JSONType(serializeEnumAsJavaBean = true)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FunctionExecWay implements IDictEnum<String> {
    /**
     *
     */
    AUTO("auto", "自动执行"),
    FOUND("found", "前端操作"),
    REQUEST("trigger", "数据触发");

    @JsonCreator
    public static FunctionExecWay of(String value) {
        for (FunctionExecWay functionType : FunctionExecWay.values()) {
            if (functionType.code.equals(value)) {
                return functionType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    @EnumValue
    private final String code;
    private final String name;

    public static JSONArray toArray() {
        return  Arrays.stream(FunctionExecWay.values()).map(codecType -> {
            JSONObject json = new JSONObject();
            json.put("value", codecType.getValue());
            json.put("text", codecType.getText());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }

    @Override
    public String getValue() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.name;
    }
}
