package org.jeecg.modules.alarm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.alarm.entity.AlarmConfig;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;

import java.util.List;

/**
 * @Description: 告警配置
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface AlarmConfigMapper extends BaseMapper<AlarmConfig> {

    /**
     * 根据实例id、数据节点id，获取触发过滤器
     * @param instanceId
     * @param dataId
     * @return
     */
    @Select("SELECT act.* FROM iot_alarm_config ac, iot_alarm_config_trigger act" +
            " WHERE 1=1 AND ac.id=act.alarm_config_id" +
            " AND ac.instance_id=#{instanceId}" +
            " AND act.data_id=#{dataId}")
    List<AlarmConfigTrigger> selectFilter(@Param("instanceId") String instanceId, @Param("dataId") String dataId);
}
