package org.jeecg.modules.device.listener.event;

import com.zhouwr.common.enums.MessageEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jeecg.modules.device.listener.MessageEvent;

/**
 * @program: forallcn-iotps
 * @description: 设备下线事件
 * @author: zhouwr
 * @create: 2020-12-09 21:58
 * @version：1.0
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstanceNotActionEvent extends MessageEvent {
    private String sceneId;
    private String instanceId;

    @Override
    public MessageEventType getEventType() {
        return MessageEventType.DEVICE_OFFLINE;
    }
}
