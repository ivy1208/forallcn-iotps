package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhouwr.common.enums.InstanceParamType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description: 设备实例参数
 * @author: zhouwr
 * @create: 2021-01-01 14:52
 * @version：1.0
 **/
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("iot_device_instance_param")
@ApiModel(value = "iot_device_instance_param对象", description = "设备实例参数")
public class DeviceInstanceParam implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 参数类型：input-输入参数；output-输出参数；event-事件规则值
     */
    private InstanceParamType type;
    private String instanceId;
    private String functionId;
    private String dataId;
    private Object value;

    public DeviceInstanceParam(InstanceParamType paramType, String instanceId, String functionId, String dataId, Object value) {
        this.type = paramType;
        this.instanceId = instanceId;
        this.functionId = functionId;
        this.dataId = dataId;
        this.value = value;
    }
}
