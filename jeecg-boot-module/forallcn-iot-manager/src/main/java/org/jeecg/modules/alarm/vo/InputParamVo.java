package org.jeecg.modules.alarm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-17 18:00
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public
class InputParamVo {
    private String dataId;
    private Object value;
}
