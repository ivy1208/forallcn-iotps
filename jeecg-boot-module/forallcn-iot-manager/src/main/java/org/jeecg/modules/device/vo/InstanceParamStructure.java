package org.jeecg.modules.device.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import com.zhouwr.common.enums.DeviceInstanceState;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @program: forallcn-iotps
 * @description: 实例及参数数据接收类
 * @author: zhouwr
 * @create: 2021-01-04 16:30
 * @version：1.0
 **/
@Data
@ToString
@NoArgsConstructor
public class InstanceParamStructure {
    private String id;
    private String parentBy;
    private String code;
    private String name;
    private String modelBy;
    private String sceneBy;
    private String sceneSchemeBy;
    private String sysOrgCode;
    private DeviceInstanceState status;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date statusUpdateTime;
    private String createBy;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String updateBy;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private String description;
    private List<InstanceFunctionVo> extendParams;
}
