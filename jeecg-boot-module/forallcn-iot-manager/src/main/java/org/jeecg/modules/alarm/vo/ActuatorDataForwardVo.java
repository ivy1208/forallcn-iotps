package org.jeecg.modules.alarm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-16 21:46
 * @version：1.0
 **/

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ActuatorDataForwardVo {
    private String id;
    private String networkServiceId;
    private String forwardReceivers;
}
