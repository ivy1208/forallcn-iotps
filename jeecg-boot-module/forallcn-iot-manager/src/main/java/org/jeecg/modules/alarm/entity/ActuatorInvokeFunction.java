package org.jeecg.modules.alarm.entity;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.alarm.vo.ActuatorInvokeFunctionVo;
import org.jeecg.modules.alarm.vo.InputParamVo;

import java.io.Serializable;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-15 21:21
 * @version：1.0
 **/
@ApiModel(value = "ActuatorInvokeFunction对象", description = "执行器调用功能")
@Data
@NoArgsConstructor
@TableName("iot_actuator_invoke_function")
public class ActuatorInvokeFunction implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 执行设备实例Id
     */
    @ApiModelProperty(value = "实例id")
    private String instanceId;
    /**
     * 功能Id
     */
    @ApiModelProperty(value = "功能id")
    private String functionId;
    /**
     * 功能输入参数
     */
    @ApiModelProperty(value = "功能输入参数")
    private String inputParam;


    public ActuatorInvokeFunction(ActuatorInvokeFunctionVo invokeFunctionVo) {
        this();
        this.setInstanceId(invokeFunctionVo.getInstanceId());
        this.setFunctionId(invokeFunctionVo.getFunctionId());
        this.setInputParam(JSONArray.toJSONString(invokeFunctionVo.getParams()));
    }

    public ActuatorInvokeFunction(String id, ActuatorInvokeFunctionVo invokeFunctionVo) {
        this(invokeFunctionVo);
        this.id = id;
    }

    public ActuatorInvokeFunctionVo vo(){
        ActuatorInvokeFunctionVo vo = new ActuatorInvokeFunctionVo();
        vo.setId(this.id);
        vo.setInstanceId(this.instanceId);
        vo.setFunctionId(this.functionId);
        vo.setParams(JSONArray.parseArray(this.inputParam, InputParamVo.class));
        return vo;
    }
}
