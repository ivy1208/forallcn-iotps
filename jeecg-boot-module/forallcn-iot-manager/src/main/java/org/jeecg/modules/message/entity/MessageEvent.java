package org.jeecg.modules.message.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zhouwr.common.enums.MessageEventType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 消息事件
 * @Author: jeecg-boot
 * @Date: 2020-12-09
 * @Version: V1.0
 */
@Data
@TableName("iot_message_event")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "iot_message_event对象", description = "消息事件")
@AllArgsConstructor
@NoArgsConstructor
public class MessageEvent implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 事件主体
     */
    @Excel(name = "事件主体", width = 15)
    @ApiModelProperty(value = "事件主体")
    private String bodyId;

    /**
     * 事件标识
     */
    @Excel(name = "事件标识", width = 15)
    @ApiModelProperty(value = "事件标识")
    private String code;
    /**
     * 事件名称
     */
    @Excel(name = "事件名称", width = 15)
    @ApiModelProperty(value = "事件名称")
    private String message;
    /**
     * 事件级别
     */
    @Excel(name = "事件级别", width = 15)
    @ApiModelProperty(value = "事件级别")
    private Integer level;
    /**
     * 触发时间
     */
    @Excel(name = "触发时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "触发时间")
    private java.util.Date createTime;
    /**
     * 是否处理
     */
    @Excel(name = "是否处理", width = 15)
    @ApiModelProperty(value = "是否处理")
    private Integer isDeal;

    public MessageEvent(String instanceId, MessageEventType eventType, int isDeal) {
        this.bodyId = instanceId;
        this.message = eventType.getName();
        this.level = eventType.getLevel();
        this.code = eventType.getCode();
        this.createTime = new Date();
        this.isDeal = eventType.getLevel() > 2 ? 1 : 0;
    }

    public MessageEvent(String instanceId, String instanceName, MessageEventType eventType, int isDeal) {
        this.bodyId = instanceId;

        this.message = new StringBuffer(eventType.getName()).insert(2, "["+instanceName+"]").toString();
        this.level = eventType.getLevel();
        this.code = eventType.getCode();
        this.createTime = new Date();
        this.isDeal = eventType.getLevel() > 2 ? 1 : 0;
    }
}
