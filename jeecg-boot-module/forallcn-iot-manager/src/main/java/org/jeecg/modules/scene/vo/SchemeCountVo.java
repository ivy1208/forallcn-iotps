package org.jeecg.modules.scene.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.modules.scene.entity.SceneScheme;

/**
 * @description: 场景方案下设备统计
 * @author: zhouwenrong
 * @create: 2020-11-12 15:42
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchemeCountVo {
    String id;
    String name;
    Long total;
    Long online;
    Long offline;
    Long noAction;

    public SchemeCountVo(SceneScheme scheme) {
        this.id = scheme.getId();
        this.name = scheme.getName();
    }
}
