package org.jeecg.modules.message.service.impl;

import org.jeecg.modules.message.entity.MessageEvent;
import org.jeecg.modules.message.mapper.MessageEventMapper;
import org.jeecg.modules.message.service.IMessageEventService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 消息事件
 * @Author: jeecg-boot
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@Service
public class MessageEventServiceImpl extends ServiceImpl<MessageEventMapper, MessageEvent> implements IMessageEventService {

}
