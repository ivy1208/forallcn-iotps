package org.jeecg.modules.device.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.device.entity.DeviceLabel;
import org.jeecg.modules.device.enums.DeviceLabelCloseMethod;
import org.jeecg.modules.device.enums.DeviceLabelTrigger;
import org.jeecg.modules.device.mapper.DeviceLabelMapper;
import org.jeecg.modules.device.service.IDeviceLabelService;
import org.jeecg.modules.device.vo.label.DeviceLabelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 设备标签
 * @Author: jeecg-boot
 * @Date: 2021-12-07
 * @Version: V1.0
 */
@Api(tags = "设备标签")
@RestController
@RequestMapping("/device/label")
@Slf4j
public class DeviceLabelController extends JeecgController<DeviceLabel, IDeviceLabelService> {
    @Autowired
    private IDeviceLabelService deviceLabelService;
    @Autowired
    private DeviceLabelMapper labelMapper;

    /**
     * 分页列表查询
     *
     * @param deviceLabel
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "设备标签-分页列表查询")
    @ApiOperation(value = "设备标签-分页列表查询", notes = "设备标签-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(DeviceLabel deviceLabel, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
        QueryWrapper<DeviceLabel> queryWrapper = QueryGenerator.initQueryWrapper(deviceLabel, req.getParameterMap());
        Page<DeviceLabel> page = new Page<DeviceLabel>(pageNo, pageSize);
        IPage<DeviceLabel> pageList = deviceLabelService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    @ApiOperation(value = "设备模型：获取设备标签触发方式")
    @GetMapping(value = "/getLabelTrigger")
    public Result<?> getLabelTrigger() {
        return Result.ok(DeviceLabelTrigger.toArray());
    }

    @ApiOperation(value = "设备模型：获取设备标签关闭类型")
    @GetMapping(value = "/getLabelCloseMethod")
    public Result<?> getLabelCloseMethod() {
        return Result.ok(DeviceLabelCloseMethod.toArray());
    }

    /**
     * 添加
     *
     * @param deviceLabel
     * @return
     */
    @AutoLog(value = "设备标签-添加")
    @ApiOperation(value = "设备标签-添加", notes = "设备标签-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody DeviceLabelVo deviceLabel) {
        deviceLabelService.saveMain(deviceLabel);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param deviceLabelVo
     * @return
     */
    @AutoLog(value = "设备标签-编辑")
    @ApiOperation(value = "设备标签-编辑", notes = "设备标签-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody DeviceLabelVo deviceLabelVo) {
        deviceLabelService.updateByMain(deviceLabelVo);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备标签-通过id删除")
    @ApiOperation(value = "设备标签-通过id删除", notes = "设备标签-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        deviceLabelService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "设备标签-批量删除")
    @ApiOperation(value = "设备标签-批量删除", notes = "设备标签-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.deviceLabelService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备标签-通过id查询")
    @ApiOperation(value = "设备标签-通过id查询", notes = "设备标签-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        DeviceLabelVo deviceLabel = deviceLabelService.getMainById(id);
        if (deviceLabel == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(deviceLabel);
    }

    @AutoLog(value = "设备标签-通过modelId查询")
    @ApiOperation(value = "设备标签-通过modelId查询")
    @GetMapping(value = "/queryByModelId")
    public Result<?> queryByModelId(@RequestParam(name = "modelId", required = true) String modelId) {
        return Result.ok(deviceLabelService.selectByMainId(modelId));
    }

    @AutoLog(value = "触发方式验证重复")
    @ApiOperation(value = "触发方式验证重复")
    @GetMapping(value = "/validateDuplicateDefaultTrigger")
    public Result<?> validateDuplicateDefaultTrigger(
            @RequestParam(name = "id", required = false) String id,
            @RequestParam(name = "deviceModelBy", required = true) String deviceModelBy,
            @RequestParam(name = "defaultTrigger", required = true) String defaultTrigger) {
        final List<DeviceLabel> list = deviceLabelService.lambdaQuery()
                .eq(DeviceLabel::getDefaultTrigger, defaultTrigger)
                .eq(DeviceLabel::getDeviceModelBy, deviceModelBy)
                .ne(StringUtils.isNotBlank(id), DeviceLabel::getId, id)
                .list();
        list.forEach(label -> log.info("{}-{}-{}", label.getTitle(), label.getDefaultTrigger(), label.getDeviceModelBy()));
        int count = list.size();
        for (DeviceLabel label : list) {
            if ("onload".equals(label.getDefaultTrigger().getValue())) {
                count = 1;
                break;
            } else {
                count = 0;
            }
        }
        return Result.ok(count);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param deviceLabel
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DeviceLabel deviceLabel) {
        return super.exportXls(request, deviceLabel, DeviceLabel.class, "设备标签");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, DeviceLabel.class);
    }

}