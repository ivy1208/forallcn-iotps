package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.device.entity.DeviceFunctionParam;

/**
 * @Description: 功能定义
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
public interface DeviceFunctionParamMapper extends BaseMapper<DeviceFunctionParam> {

}
