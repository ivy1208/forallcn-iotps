package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.device.entity.DeviceInstanceParam;

public interface DeviceInstanceParamMapper extends BaseMapper<DeviceInstanceParam> {
}
