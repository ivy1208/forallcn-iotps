package org.jeecg.modules.device.listener.event;

import com.zhouwr.common.enums.MessageEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jeecg.modules.device.listener.MessageEvent;

/**
 * @program: forallcn-iotps
 * @description: 设备实例不存在
 * @author: zhouwr
 * @create: 2020-12-22 15:04
 * @version：1.0
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstanceNotFindEvent extends MessageEvent {
    private String sceneId;
    private String instanceId;

    public InstanceNotFindEvent(String instanceId) {
        super();
        this.instanceId = instanceId;
    }

    @Override
    public MessageEventType getEventType() {
        return MessageEventType.DEVICE_NOT_FIND;
    }
}
