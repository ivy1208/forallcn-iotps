package org.jeecg.modules.network.network.tcp;

import com.zhouwr.common.utils.HexConvertUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

public class SocketTest {

    public static void main(String[] args) {
        String host = "192.168.1.100";
        int port = 8888;
        boolean running = true;
        String msg = "" +
                "01 03 f4 00 c3 00 c1 00 c2 00 c0 00 bf 00 c2 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 fc 19 00 c3 00 00 66 a3";
        try {
            //创建一个Socket,跟本机的8080端口连接
            Socket socket = new Socket();
            socket.bind(new InetSocketAddress(23));//绑定本地端口
            socket.connect(new InetSocketAddress(host, port));//连接远程服务端接口

            //使用Socket创建PrintWriter和BufferedReader进行读写数据
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            do {
                //接收数据
//                String line = is.readLine();
//                System.out.println("received from server:" + line);

                //发送数据
                System.out.println(Arrays.toString(HexConvertUtil.hexStringToBytes(msg)));
                pw.println(HexConvertUtil.hexStringToBytes(msg));
                pw.flush();

                Thread.sleep(2000);
            } while (true);
            //关闭资源
            // pw.close();
            // is.close();
            // socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
