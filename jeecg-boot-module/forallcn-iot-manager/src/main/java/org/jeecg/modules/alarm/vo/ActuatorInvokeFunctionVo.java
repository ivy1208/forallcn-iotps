package org.jeecg.modules.alarm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-16 21:46
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ActuatorInvokeFunctionVo {
    private String id;
    private String instanceId;
    private String functionId;
    private List<InputParamVo> params;
}
