package org.jeecg.modules.scene.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2020-12-18 12:27
 * @version：1.0
 **/
public interface DisplayerMapper {
    public List<Map<String, Long>> countMessageEvent(@Param("sceneId") String sceneId);
}
