package org.jeecg.modules.alarm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhouwr.common.enums.NoticeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-16 21:46
 * @version：1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ActuatorNotificationVo {
    private String id;
    private NoticeType noticeType;
    private String noticeConfigId;
    private String noticeTemplateCode;

}
