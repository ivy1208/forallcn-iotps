package org.jeecg.modules.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.device.entity.DeviceData;

import java.util.List;

/**
 * @Description: 数据节点
 * @Author: jeecg-boot
 * @Date: 2020-04-08
 * @Version: V1.0
 */
public interface DeviceDataMapper extends BaseMapper<DeviceData> {

    public boolean deleteByMainId(@Param("mainId") String mainId);

    public List<DeviceData> selectByMainId(@Param("mainId") String mainId);

    /**
     * 根据设备实例id，获取设备的数据节点
     *
     * @param instanceId
     * @return
     */
    List<DeviceData> selectByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 根据实例id、数据节点code，获取数据节点
     *
     * @param instanceId
     * @param dataCode
     * @return
     */
    @Select("SELECT d.* FROM iot_device_data d, iot_device_model m, iot_device_instance i " +
            "WHERE 1 = 1 AND i.id=#{instanceId} AND d.code=#{dataCode} AND i.model_by=m.id AND d.device_model_by=m.id")
    List<DeviceData> select(@Param("instanceId") String instanceId, @Param("dataCode") String dataCode);

    InstanceDataStructure selectLastDataStructure(@Param("instanceId") String instanceId, @Param("propertyCode") String propertyCode);

    List<InstanceDataStructure> selectLastDataStructure(@Param("instanceId") String instanceId);
}