package org.jeecg.modules.device.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-01-07 16:13
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DataReportCountVo {
    public String deviceId;
    public String deviceName;
    public Long dataCount;
    public Date countTime;

    public List<InstanceDataCountVo> dataCounts = new ArrayList<>();

}
