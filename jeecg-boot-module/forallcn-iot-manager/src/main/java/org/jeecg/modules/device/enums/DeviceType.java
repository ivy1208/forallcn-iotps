package org.jeecg.modules.device.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author zhouwenrong
 */

@AllArgsConstructor
@Getter
public enum DeviceType implements IDictEnum<String> {
    /**
     *
     */
    SCENE("场景", "scene", "global"),
    GATEWAY_TERMINAL("网关子设备", "gatewayTerminal", "cluster"),
    DIRECT_TERMINAL("直连设备", "directTerminal", "thunderbolt"),
    GATEWAY("网关设备", "gateway", "cloud-upload"),
    OTHER("其它", "other", "codepen");

    private final String text;
    @EnumValue
    private final String value;
    private final String icon;

    @JsonCreator
    public static DeviceType of(String value) {
        for (DeviceType deviceType : DeviceType.values()) {
            if (deviceType.value.equals(value)) {
                return deviceType;
            }
        }
        throw new UnsupportedOperationException("不支持的数据类型:" + value);
    }

    public static JSONArray toArray() {
        return  Arrays.stream(DeviceType.values()).map(deviceType -> {
            JSONObject json = new JSONObject();
            json.put("value", deviceType.getValue());
            json.put("text", deviceType.getText());
            json.put("icon", deviceType.getIcon());
            return json;
        }).collect(Collectors.toCollection(JSONArray::new));
    }
}
