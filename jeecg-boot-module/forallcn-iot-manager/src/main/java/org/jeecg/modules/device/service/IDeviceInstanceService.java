package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.device.vo.InstanceDataStructure;
import com.zhouwr.common.device.vo.InternetDevice.InternetDeviceVo;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import com.zhouwr.common.device.vo.function.ModelFunctionVo;
import io.netty.channel.ChannelHandlerContext;
import org.jeecg.modules.device.entity.DeviceData;
import org.jeecg.modules.device.entity.DeviceInstance;
import com.zhouwr.common.enums.DeviceInstanceState;
import org.jeecg.modules.device.vo.InstanceParamStructure;
import org.jeecg.modules.device.vo.label.ViewDataVo;
import org.jeecg.modules.scene.entity.Scene;
import org.jeecg.modules.scene.vo.DeployConfig;

import java.util.Date;
import java.util.List;

/**
 * @Description: 设备实例
 * @Author: jeecg-boot
 * @Date: 2020-04-11
 * @Version: V1.0
 */
public interface IDeviceInstanceService extends IService<DeviceInstance> {

    List<DeviceInstance> listInstanceDeviceByModelType(String modelType);

    List<InternetDeviceVo> listInternetDevice();

    /**
     * 动态创建数据表，根据实例id
     *
     * @param instanceId
     * @return
     */
    Boolean createTable(String instanceId);

    /**
     * 根据TCP地址(IPL:PORT)获取设备实例
     *
     * @param address <p>IP:PORT</p>
     * @return 设备实例
     */
    DeviceInstance getInstanceByAddress(String address);

    /**
     * 根据设备模型id，获取设备数据节点
     *
     * @param modelId
     * @return
     */
    List<DeviceData> getDeviceDatasByModelId(String modelId);

    /**
     * 根据设备实例id，获取设备数据节点
     *
     * @param instanceId
     * @return
     */
    List<DeviceData> getDeviceDatasByInstanceId(String instanceId);

    /**
     * 根据设备实例id，获取网络连接通道
     * @param instanceId
     * @return
     * @throws Exception
     */
    ChannelHandlerContext getNetworkChannel(String instanceId) throws Exception;

    /**
     * 获取设备实例的父级网关设备实例
     *
     * @param instanceId
     * @return
     */
    DeviceInstance getParentGateway(String instanceId);

    /**
     * 获取父级实例
     *
     * @param instanceId
     * @return
     */
    DeviceInstance getParentInstance(String instanceId);

    /**
     * 获取设备的网络状态
     *
     * @param instanceId
     * @return DeviceInstanceState
     */
    DeviceInstanceState getNetworkState(String instanceId);

    /**
     * 递归获取父类
     *
     * @param instanceId
     * @param instances
     * @return
     */
    List<DeviceInstance> getParentInstances(String instanceId, List<DeviceInstance> instances);

    /**
     * 设置实例设备的功能执行配置信息
     *
     * @param execStructure
     */
    void setFuncExecConf(InstanceFunctionVo execStructure);

    /**
     * 删除执行任务
     *
     * @param execStructure
     */
    void deleteExecJob(InstanceFunctionVo execStructure);

    /**
     * 添加执行任务
     *
     * @param execStructure
     * @throws Exception
     */
    void addExecJob(InstanceFunctionVo execStructure) throws Exception;

    /**
     * 获取子实例列表，根据实例id
     *
     * @param id
     * @param withSelf
     * @return
     */
    List<DeviceInstance> getInstanceChildren(String id, Boolean withSelf);

    /**
     * 模型扩展参数，带数值
     * 新增实例时，无instanceId，根据modelId查询功能配置，无功能输入参数
     * 编辑实例时，有instanceId，根据modelId查询功能配置，根据instanceId查询输入参数
     * @param instanceId
     * @param modelId
     * @return
     * @throws Exception
     */
    List<ModelFunctionVo> getModelExtendParams(String modelId, String instanceId);

    /**
     * 实例扩展参数，带数值
     * @param instanceId
     * @return
     * @throws Exception
     */
    List<InstanceFunctionVo> getInstanceExtendParams(String instanceId) throws Exception;

    /**
     * 构建功能结构
     *
     * @param instanceId 设备实例
     * @return 设备实例扩展参数（功能及输入参数）
     */
    public List<InstanceFunctionVo> buildFunctionStructures(String instanceId);

    /**
     * 列出实例历史数据
     * @param instanceId
     * @param startTime
     * @param endTime
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<InstanceDataStructure> listInstanceDataHistory(String instanceId, Date startTime, Date endTime, int pageIndex, int pageSize);

    List<InstanceDataStructure> listInstanceDataHistory(String instanceId, Date begin, Date now);

    /**
     * 构建数据节点，数据值取最新一条数据值
     *
     * @param instanceId 实例id
     * @return List<InstanceDataStructure>
     */
    List<InstanceDataStructure> listInstanceDataLatest(String instanceId);

    /**
     * 批量更新实例状态
     * @param instanceId
     * @param status
     */
    void batchUpdateInstanceState(String instanceId, DeviceInstanceState status);

    /**
     * 更新实例及参数
     * @param structure
     */
    void updateInstanceParams(InstanceParamStructure structure);

    /**
     * 手动下线设备
     * @param instanceId
     */
    void manuallyOffline(String instanceId);

    /**
     * 根据实例id，获取实例所属场景
     * @param instanceId
     * @return
     */
    Scene getScene(String instanceId);

    /**
     * 根据实例id，获取实例所属场景id
     *
     * @param instanceId
     * @return
     */
    String getSceneId(String instanceId);

    /**
     * 按ID查询相同级别
     *
     * @param instanceId
     * @return
     */
    List<DeviceInstance> querySameLevelById(String instanceId);


    List<DeployConfig> listDeployDevice();

    /**
     * 通过id获取属性
     *
     * @param modelId    模型id
     * @param viewDataVo 显示数据vo {@link ViewDataVo}
     * @return {@link String}
     */
    String getAttributeById(String modelId, ViewDataVo viewDataVo);

    List<InstanceFunctionVo> getInstanceFunctionExecute(String instanceId);

    /**
     * 删除实例
     * @param instanceId
     * @return
     */
    boolean deleteInstance(String instanceId);
}