package org.jeecg.modules.alarm.handler.impl;

import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.alarm.handler.IActuatorHandle;
import org.jeecg.modules.alarm.service.IActuatorFunctionService;
import org.jeecg.modules.device.service.IDeviceFunctionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-06-17 09:24
 * @version：1.0
 **/
public class FunctionInvokeHandle implements IActuatorHandle {
    @Autowired
    private IDeviceFunctionService functionsService;
    @Autowired
    private IActuatorFunctionService actuatorFunctionService;

    public FunctionInvokeHandle() {
        this.functionsService = (IDeviceFunctionService) SpringContextUtils.getBean(IDeviceFunctionService.class);
        this.actuatorFunctionService = (IActuatorFunctionService) SpringContextUtils.getBean(IActuatorFunctionService.class);
    }

    @Override
    public boolean invoke(String actuatorId, Map<String, Object> dataMap) {
        try {
            InstanceFunctionVo executeConfig = actuatorFunctionService.getExecuteConfig(actuatorId);
            Boolean b = functionsService.invokeFunctionAsync(executeConfig);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
