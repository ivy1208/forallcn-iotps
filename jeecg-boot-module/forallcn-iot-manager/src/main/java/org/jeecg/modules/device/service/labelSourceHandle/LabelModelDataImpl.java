package org.jeecg.modules.device.service.labelSourceHandle;

import com.zhouwr.common.utils.EnumUtil;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.device.service.IDeviceModelService;
import org.jeecg.modules.device.service.impl.DeviceModelServiceImpl;
import org.jeecg.modules.device.vo.label.ViewDataVo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 标签模型数据impl
 *
 * @author zhouwenrong
 * @date 2022/01/06
 */
@Slf4j
public class LabelModelDataImpl implements ILabelSourceData {
    /**
     * 模型服务
     */
    @Autowired
    private IDeviceModelService modelService;

    /**
     * 标签模型数据impl
     */
    public LabelModelDataImpl() {
        this.modelService = SpringContextUtils.getBean(DeviceModelServiceImpl.class);
    }

    /**
     * 获得价值
     *
     * @param modelId    模型id
     * @param viewDataVo 视图数据签证官
     * @return {@link String}
     * @throws ClassNotFoundException 类没有发现异常
     */
    @Override
    public String getValue(String modelId, String instanceId, ViewDataVo viewDataVo) throws ClassNotFoundException {
        String value = modelService.getAttributeById(modelId, viewDataVo);
        if ("enum".equals(viewDataVo.getType())) {
            value = (String) EnumUtil.getEnumProperty(value,
                    viewDataVo.getConvert().getDictTable(),
                    viewDataVo.getConvert().getDictKey(),
                    viewDataVo.getConvert().getDictValue());
        }
        return value;
    }
}
