package org.jeecg.modules.device.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @program: forallcn-iotps
 * @description: 实例数据节点下的数据个数统计
 * @author: zhouwr
 * @create: 2021-01-07 22:10
 * @version：1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InstanceDataCountVo {
    private String dataId;
    private String dataCode;
    private String dataName;
    private String dataCount;

}
