package org.jeecg.modules.message.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouwr.common.enums.EventLevel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.message.entity.MessageEvent;
import org.jeecg.modules.message.service.IMessageEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 消息事件
 * @Author: jeecg-boot
 * @Date: 2020-12-09
 * @Version: V1.0
 */
@Api(tags = "消息事件")
@RestController
@RequestMapping("/message/event")
@Slf4j
public class MessageEventController extends JeecgController<MessageEvent, IMessageEventService> {
    @Autowired
    private IMessageEventService messageEventService;

    /**
     * 分页列表查询
     *
     * @param messageEvent
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "消息事件-分页列表查询")
    @ApiOperation(value = "消息事件-分页列表查询", notes = "消息事件-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MessageEvent messageEvent,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MessageEvent> queryWrapper = QueryGenerator.initQueryWrapper(messageEvent, req.getParameterMap());
        Page<MessageEvent> page = new Page<MessageEvent>(pageNo, pageSize);
        IPage<MessageEvent> pageList = messageEventService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    @AutoLog(value = "消息事件-获取事件等级")
    @ApiOperation(value = "消息事件-获取事件等级", notes = "消息事件-获取事件等级")
    @GetMapping(value = "/listLevel")
    public Result<?> listLevel() {
        return Result.ok(EventLevel.toJSONArray());
    }


    /**
     * 添加
     *
     * @param messageEvent
     * @return
     */
    @AutoLog(value = "消息事件-添加")
    @ApiOperation(value = "消息事件-添加", notes = "消息事件-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MessageEvent messageEvent) {
        messageEventService.save(messageEvent);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param messageEvent
     * @return
     */
    @AutoLog(value = "消息事件-编辑")
    @ApiOperation(value = "消息事件-编辑", notes = "消息事件-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MessageEvent messageEvent) {
        messageEventService.updateById(messageEvent);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "消息事件-通过id删除")
    @ApiOperation(value = "消息事件-通过id删除", notes = "消息事件-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        messageEventService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "消息事件-批量删除")
    @ApiOperation(value = "消息事件-批量删除", notes = "消息事件-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.messageEventService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "消息事件-通过id查询")
    @ApiOperation(value = "消息事件-通过id查询", notes = "消息事件-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MessageEvent messageEvent = messageEventService.getById(id);
        if (messageEvent == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(messageEvent);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param messageEvent
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MessageEvent messageEvent) {
        return super.exportXls(request, messageEvent, MessageEvent.class, "消息事件");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MessageEvent.class);
    }

}
