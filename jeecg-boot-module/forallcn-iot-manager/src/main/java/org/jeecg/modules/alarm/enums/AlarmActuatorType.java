package org.jeecg.modules.alarm.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @program: forallcn-iotps
 * @description: 报警执行器类型
 * @author: zhouwr
 * @create: 2021-01-14 12:10
 * @version：1.0
 **/
@AllArgsConstructor
@Getter
public enum AlarmActuatorType implements IDictEnum<Byte> {
    /**
     *
     */
    NOTIFICATION((byte) 0x01, "消息通知", "org.jeecg.modules.alarm.handler.impl.NotificationHandle"),
    DEVICE_FUNCTION((byte) 0x02, "功能调用", "org.jeecg.modules.alarm.handler.impl.FunctionInvokeHandle"),
    DATA_FORWARD((byte) 0x03, "数据转发", "org.jeecg.modules.alarm.handler.impl.DataForwardHandle");

    @EnumValue
    private final Byte value;
    private final String text;
    private final String actuatorClass;

    @JsonCreator
    public static AlarmActuatorType of(Byte value) {
        final List<AlarmActuatorType> actuatorTypes = Arrays.stream(AlarmActuatorType.values())
                .filter(type -> Objects.equals(type.value, value))
                .collect(Collectors.toList());
        if (actuatorTypes.size() > 0) {
            return actuatorTypes.get(0);
        }
        return null;
    }

    /**
     * @return
     */
    public static JSONArray toJSONArray() {
        return Arrays.stream(AlarmActuatorType.values())
                .map(type -> {
                    JSONObject json = new JSONObject();
                    json.put("text", type.text);
                    json.put("value", type.value);
                    return json;
                }).collect(Collectors.toCollection(JSONArray::new));
    }
}
