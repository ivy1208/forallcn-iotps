package org.jeecg.modules.scene.vo;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.zhouwr.common.utils.Vector3;

/**
 * @program: jeecg-boot-iotcp
 * @description: 模型属性
 * @author: zhouwr
 * @create: 2020-11-03 13:34
 * @version：1.0
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class ModelAttribute {
    private Vector3 position;
    private Vector3 rotation;
    private Vector3 scale;
    private LightAttribute ambientLight;
    private LightAttribute directionalLight;

    public ModelAttribute(String attribute) {
        if (org.jeecg.common.util.oConvertUtils.isNotEmpty(attribute)) {
            JSONObject json = JSONObject.parseObject(attribute);
            this.position = new Vector3(json.getJSONObject("position"));
            this.rotation = new Vector3(json.getJSONObject("rotation"));
            this.scale = new Vector3(json.getJSONObject("scale"));
            this.ambientLight = new LightAttribute(json.getJSONObject("ambientLight"));
            this.directionalLight = new LightAttribute(json.getJSONObject("directionalLight"));
        }
    }

    public ModelAttribute(Vector3 position, Vector3 rotation, Vector3 scale) {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    public ModelAttribute(Vector3 position, LightAttribute ambientLight, LightAttribute directionalLight) {
        this.position = position;
        this.ambientLight = ambientLight;
        this.directionalLight = directionalLight;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("position", this.position);
        json.put("rotation", this.rotation);
        json.put("scale", this.scale);
        json.put("ambientLight", this.ambientLight);
        json.put("directionalLight", this.directionalLight);
        return json.toJSONString();
    }

    public static void main(String[] args) {
        String str = "{position:{x:1,y:2,z:3},rotation:{x:1,y:2,z:3},scale:{x:1,y:2,z:3}}";
        ModelAttribute attribute = new ModelAttribute(str);
        System.out.println(attribute.getPosition());
        System.out.println(attribute.toString());
    }
}
