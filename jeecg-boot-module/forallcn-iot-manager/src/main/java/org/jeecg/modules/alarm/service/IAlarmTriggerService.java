package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.alarm.entity.AlarmConfigTrigger;

import java.util.List;

/**
 * @Description: 告警触发器
 * @Author: jeecg-boot
 * @Date:   2021-01-11
 * @Version: V1.0
 */
public interface IAlarmTriggerService extends IService<AlarmConfigTrigger> {

	/**
	 * 根据配置id查询
	 * @param configId
	 * @return
	 */
	public List<AlarmConfigTrigger> selectByMainId(String configId);
}
