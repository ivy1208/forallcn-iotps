package org.jeecg.modules.alarm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouwr.common.device.vo.function.InstanceFunctionVo;
import org.jeecg.modules.alarm.entity.ActuatorInvokeFunction;

/**
 * @author zhouwenrong
 */
public interface IActuatorFunctionService extends IService<ActuatorInvokeFunction> {
    /**
     * 获取发送消息
     *
     * @param actuatorId
     * @return
     */
    InstanceFunctionVo getExecuteConfig(String actuatorId);
}
