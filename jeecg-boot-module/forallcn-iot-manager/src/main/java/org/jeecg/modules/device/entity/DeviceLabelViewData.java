package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.modules.device.enums.DeviceLabelSource;
import org.jeecg.modules.device.vo.label.ViewDataVo;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 设备标签显示数据
 * @Author: zhouwenrong
 * @Date: 2022-01-06
 * @Version: V1.0
 */
@ApiModel(value = "device_label_view_data对象", description = "设备标签显示数据")
@Data
@TableName("iot_device_label_view_data")
public class DeviceLabelViewData implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;

    /**
     * 设备标签
     */
    @ApiModelProperty(value = "设备标签")
    private String deviceLabelBy;

    /**
     * 标识
     */
    @Excel(name = "标识", width = 15)
    @ApiModelProperty(value = "标识")
    private String code;

    /**
     * 名称
     */
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 数据类型
     */
    @Excel(name = "数据类型", width = 15)
    @ApiModelProperty(value = "数据类型")
    private String type;

    /**
     * 数据来源
     */
    @Excel(name = "数据来源", width = 15)
    @ApiModelProperty(value = "数据来源")
    private DeviceLabelSource source;

    /**
     * 数据转换
     */
    @Excel(name = "数据长度", width = 15)
    @ApiModelProperty(value = "数据长度")
    private Integer length;

    @Excel(name = "日期时间格式", width = 15)
    @ApiModelProperty(value = "日期时间格式")
    private String dateformat;

    @Excel(name = "字典表", width = 15)
    @ApiModelProperty(value = "字典表")
    private String dictTable;

    @Excel(name = "字典键", width = 15)
    @ApiModelProperty(value = "字典键")
    private String dictKey;

    @Excel(name = "字典值", width = 15)
    @ApiModelProperty(value = "字典值")
    private String dictValue;

    public ViewDataVo vo() {
        return new ViewDataVo(this);
    }
}
