package org.jeecg.modules.network.network.mqtt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @program: forallcn-iotps
 * @description: Mqtt消息处理
 * @author: zhouwr
 * @create: 2020-12-22 11:23
 * @version：1.0
 **/
@Slf4j
@Service
public class MqttCallbackHandle {
    @Resource
    private IMqttSender iMqttSender;

    public void handle(String topic, String payload){
        log.info("MqttCallbackHandle:" + topic + "---"+ payload);
        // 根据topic分别进行消息处理。
        if (topic.equalsIgnoreCase("testTopic")){
            // 业务逻辑
        }
    }
}


