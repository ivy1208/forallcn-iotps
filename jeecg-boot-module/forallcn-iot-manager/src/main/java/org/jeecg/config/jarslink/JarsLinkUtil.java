package org.jeecg.config.jarslink;

import com.alipay.jarslink.api.Module;
import com.alipay.jarslink.api.ModuleConfig;
import com.alipay.jarslink.api.ModuleLoader;
import com.alipay.jarslink.api.ModuleManager;

import javax.annotation.Resource;
import java.net.URL;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-02-25 10:25
 * @version：1.0
 **/
public class JarsLinkUtil {
    @Resource
    private ModuleLoader moduleLoader;
    @Resource
    private ModuleManager moduleManager;

    public void load(String jarUrl) {
        Module module = moduleLoader.load(createModuleConfig(jarUrl));
        String result = module.doAction("hello-world-action", "rabbit");
    }

    public ModuleConfig createModuleConfig(String jarUrl) {
        // 加载模块jar包，可以在任意路径下
        URL modelUrl = Thread.currentThread().getContextClassLoader().getResource(jarUrl);
        ModuleConfig moduleConfig = new ModuleConfig();
        moduleConfig.addModuleUrl(modelUrl);
        return moduleConfig;
    }

    public static void main(String[] args) {
        JarsLinkUtil util = new JarsLinkUtil();
        util.load("/Users/zhouwenrong/IdeaProjects/forallcn-iotps/jeecg-boot-module/forallcn-iot-manager/src/main/java/org/jeecg/config/jarslink/HelloWorldAction.java");
    }
}
