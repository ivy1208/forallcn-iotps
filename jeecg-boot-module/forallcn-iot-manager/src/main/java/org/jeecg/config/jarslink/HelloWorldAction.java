package org.jeecg.config.jarslink;

import com.alipay.jarslink.api.Action;

/**
 * @program: forallcn-iotps
 * @description:
 * @author: zhouwr
 * @create: 2021-02-25 11:01
 * @version：1.0
 **/
public class HelloWorldAction implements Action<String, String> {

    @Override
    public String execute(String s) {
        return s + ":hello world";
    }

    @Override
    public String getActionName() {
        return "hello-world-action";
    }


}
