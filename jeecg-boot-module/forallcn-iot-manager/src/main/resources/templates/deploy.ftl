<#assign base=springMacroRequestContext.getContextUrl("")>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <script>
      window.location.href = "${base}/deploy/index.html?mode=display&baseUrl=${base}&username=${username}&token=${token}";
    </script>
</head>
<body>
</body>
</html>
