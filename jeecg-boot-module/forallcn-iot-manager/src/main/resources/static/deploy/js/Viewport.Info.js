/**
 * @author mrdoob / http://mrdoob.com/
 */

import {UIBreak, UIPanel, UIText} from '../../js/threejs/libs/ui.js'

var ViewportInfo = function (editor) {
  var signals = editor.signals
  var strings = editor.strings

  var container = new UIPanel()
  container.setId('info')
  container.setPosition('absolute')
  container.setLeft('10px')
  container.setBottom('10px')
  container.setFontSize('12px')
  container.setColor('#fff')

  const objectsText = new UIText('0').setMarginLeft('6px');
  const directTerminalText = new UIText('0').setMarginLeft('6px');
  const gatewayTerminalText = new UIText('0').setMarginLeft('6px');
  const gatewaysText = new UIText('0').setMarginLeft('6px');

  const frametimeText = new UIText('0').setMarginLeft('6px');

  container.add(new UIText('直连设备'))
  container.add(directTerminalText, new UIBreak())
  container.add(new UIText('网关子设备'))
  container.add(gatewayTerminalText, new UIBreak())
  container.add(new UIText('网关设备'))
  container.add(gatewaysText, new UIBreak())
  container.add(new UIText(strings.getKey('viewport/info/objects')).setTextTransform('lowercase'))
  container.add(objectsText, new UIBreak())
  container.add(new UIText(strings.getKey('viewport/info/frametime')).setTextTransform('lowercase'))
  container.add(frametimeText, new UIBreak())

  signals.objectAdded.add(update)
  signals.objectRemoved.add(update)
  signals.geometryChanged.add(update)

  //

  function update() {

    let directTerminal = 0
    let gatewayTerminal = 0
    let gateways = 0

    let objects = editor.scene.children;
    for (let obj of objects) {
      if (obj.userData.type === 'directTerminal')
        directTerminal++
      else if (obj.userData.type === 'gatewayTerminal')
        gatewayTerminal++
      else if (obj.userData.type === 'gateway')
        gateways++
    }

    directTerminalText.setValue(directTerminal)
    gatewayTerminalText.setValue(gatewayTerminal)
    gatewaysText.setValue(gateways)
    objectsText.setValue(objects.length)
  }

  signals.sceneRendered.add(updateFrametime)

  function updateFrametime(frametime) {
    frametimeText.setValue(Number(frametime).toFixed(2) + ' ms')
  }

  return container
}

export {ViewportInfo}
