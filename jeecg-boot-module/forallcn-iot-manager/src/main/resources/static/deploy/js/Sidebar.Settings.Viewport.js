/**
 * @author mrdoob / http://mrdoob.com/
 */

import {UIDiv, UIRow, UIText} from '../../js/threejs/libs/ui.js'
import {UIBoolean} from '../../js/threejs/libs/ui.three.js'

var SidebarSettingsViewport = function (editor) {
  var signals = editor.signals
  var strings = editor.strings

  var container = new UIDiv()

  // helpers
  // zhouwr 去掉grid相关，增加helpers相关
  var showHelpersRow = new UIRow()

  showHelpersRow.add(new UIText(strings.getKey('sidebar/settings/viewport/helpers')).setWidth('90px'))

  var showHelpers = new UIBoolean(true).onChange(function () {
    signals.showHelpersChanged.dispatch(showHelpers.getValue())
  })
  showHelpersRow.add(showHelpers)
  container.add(showHelpersRow)

  return container
}

export {SidebarSettingsViewport}
