/**
 * @author mrdoob / http://mrdoob.com/
 */

import {UIPanel, UIRow} from '../../js/threejs/libs/ui.js'

var MenubarView = function (editor) {
  var container = new UIPanel()
  container.setClass('menu')

  var title = new UIPanel()
  title.setClass('title')
  title.setTextContent('View')
  container.add(title)

  var options = new UIPanel()
  options.setClass('options')
  container.add(options)

  // zhouwr del VR mode
  return container
}

export {MenubarView}
