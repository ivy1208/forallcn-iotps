// r117

const assets = [
  // './../',

  '../build/three.module.js',

  '../../manifest.json',
  '../images/favicon.ico',
  '../images/icon.png',

  '../libs/codemirror/codemirror.css',
  '../libs/codemirror/theme/monokai.css',

  '../libs/codemirror/codemirror.js',
  '../libs/codemirror/mode/javascript.js',
  '../libs/codemirror/mode/glsl.js',

  '../libs/esprima.js',
  '../libs/jsonlint.js',
  '../libs/glslprep.min.js',

  '../libs/codemirror/addon/dialog.css',
  '../libs/codemirror/addon/show-hint.css',
  '../libs/codemirror/addon/tern.css',

  '../libs/codemirror/addon/dialog.js',
  '../libs/codemirror/addon/show-hint.js',
  '../libs/codemirror/addon/tern.js',
  '../libs/acorn/acorn.js',
  '../libs/acorn/acorn_loose.js',
  '../libs/acorn/walk.js',
  '../libs/ternjs/polyfill.js',
  '../libs/ternjs/signal.js',
  '../libs/ternjs/tern.js',
  '../libs/ternjs/def.js',
  '../libs/ternjs/comment.js',
  '../libs/ternjs/infer.js',
  '../libs/ternjs/doc_comment.js',
  '../libs/tern-threejs/threejs.js',

  '../libs/signals.min.js',
  '../libs/ui.js',
  '../libs/ui.three.js',

  '../libs/app.js',
  '../Script.js',

  //. '..s/main.css',

  '../EditorControls.js',
  '../Storage.js',

  //./js/Editor.js',
  //./js/Config.js',
  '../History.js',
  '../Loader.js',
  '../LoaderUtils.js',
  '../Menubar.js',
  '../Menubar.File.js',
  '../Menubar.Edit.js',
  '../Menubar.Add.js',
  '../Menubar.Status.js',
  '../Sidebar.js',
  '../Sidebar.Scene.js',
  '../Sidebar.Project.js',
  '../Sidebar.Settings.js',
  '../Sidebar.Settings.Shortcuts.js',
  '../Sidebar.Settings.Viewport.js',
  '../Sidebar.Properties.js',
  '../Sidebar.Object.js',
  '../Sidebar.Geometry.js',
  '../Sidebar.Geometry.Geometry.js',
  '../Sidebar.Geometry.BufferGeometry.js',
  '../Sidebar.Geometry.Modifiers.js',
  '../Sidebar.Geometry.BoxGeometry.js',
  '../Sidebar.Geometry.CircleGeometry.js',
  '../Sidebar.Geometry.CylinderGeometry.js',
  '../Sidebar.Geometry.DodecahedronGeometry.js',
  '../Sidebar.Geometry.ExtrudeGeometry.js',
  '../Sidebar.Geometry.IcosahedronGeometry.js',
  '../Sidebar.Geometry.OctahedronGeometry.js',
  '../Sidebar.Geometry.PlaneGeometry.js',
  '../Sidebar.Geometry.RingGeometry.js',
  '../Sidebar.Geometry.SphereGeometry.js',
  '../Sidebar.Geometry.ShapeGeometry.js',
  '../Sidebar.Geometry.TetrahedronGeometry.js',
  '../Sidebar.Geometry.TorusGeometry.js',
  '../Sidebar.Geometry.TorusKnotGeometry.js',
  '../Sidebar.Geometry.TubeGeometry.js',
  '../Sidebar.Geometry.TeapotBufferGeometry.js',
  '../Sidebar.Geometry.LatheGeometry.js',
  '../Sidebar.Material.js',
  '../Sidebar.Animation.js',
  '../Sidebar.Script.js',
  '../Sidebar.History.js',
  '../Strings.js',
  '../Toolbar.js',
  '../Viewport.js',
  '../Viewport.Camera.js',
  '../Viewport.Info.js',

  '../Command.js',
  '../commands/AddObjectCommand.js',
  '../commands/RemoveObjectCommand.js',
  '../commands/MoveObjectCommand.js',
  '../commands/SetPositionCommand.js',
  '../commands/SetRotationCommand.js',
  '../commands/SetScaleCommand.js',
  '../commands/SetValueCommand.js',
  '../commands/SetUuidCommand.js',
  '../commands/SetColorCommand.js',
  '../commands/SetGeometryCommand.js',
  '../commands/SetGeometryValueCommand.js',
  '../commands/MultiCmdsCommand.js',
  '../commands/AddScriptCommand.js',
  '../commands/RemoveScriptCommand.js',
  '../commands/SetScriptValueCommand.js',
  '../commands/SetMaterialCommand.js',
  '../commands/SetMaterialColorCommand.js',
  '../commands/SetMaterialMapCommand.js',
  '../commands/SetMaterialValueCommand.js',
  '../commands/SetMaterialVectorCommand.js',
  '../commands/SetSceneCommand.js',
  '../commands/Commands.js',
]

self.addEventListener('install', async function () {
  const cache = await caches.open('threejs-editor')

  // assets.forEach( function ( asset ) {
  //
  // 	cache.add( asset ).catch( function () {
  //
  // 		console.error( '[SW] Cound\'t cache:', asset );
  //
  // 	} );
  //
  // } );
})

self.addEventListener('fetch', async function (event) {
  const request = event.request
  event.respondWith(cacheFirst(request))
})

async function cacheFirst(request) {
  const cachedResponse = await caches.match(request)

  if (cachedResponse === undefined) {
    console.error('[SW] Not cached:', request.url)
    return fetch(request)
  }

  return cachedResponse
}
