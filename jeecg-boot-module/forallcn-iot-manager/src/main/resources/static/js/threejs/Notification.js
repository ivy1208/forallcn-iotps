const Notification = function (editor) {
  window.addEventListener('load', function () {
    // 检查浏览器是否支持 Notification
    if (!('Notification' in window)) {
      alert('你的不支持 Notification!  TAT')
    }

    // 检查用户是否已经允许使用通知
    else if (Notification.permission === 'granted') {
      // 创建 Notification
      new Notification('欢迎使用3D物联网平台!')
    }

    // 重新发起请求，让用户同意使用通知
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function (permission) {
        // 用户同意使用通知
        if (!('permission' in Notification)) {
          Notification.permission = permission
        }

        if (permission === 'granted') {
          // 创建 Notification
          new Notification('欢迎使用3D物联网平台!')
        }
      })
    }
  })
}

Notification.prototype = {
  message: function (title, options) {
    title = title || '新的消息'
    options = options || {
      body: '默认消息',
      icon: ``,
    }
    return new Notification(title, options)
  },
}

export {Notification}
