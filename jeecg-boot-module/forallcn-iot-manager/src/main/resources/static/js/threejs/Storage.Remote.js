var StorageRemote = function (editor) {
  this.request = editor.request
  this.sceneId = editor.SCENE_ID
  this.username = editor.USERNAME
  this.objects = editor.scene.children
  this.signals = editor.signals
}

StorageRemote.prototype = {
  /**
   * @callback
    */
  getDeployConfig: function () {
    const start = performance.now();
    return new Promise((resolve, reject) => {
      this.request.getAction('/display/getDeployConfig', {id: this.sceneId, username: this.username}).then((res) => {
        if (res.success) {
          console.log('部署配置数据：', res.result)
          this.signals.deployConfigLoaded.dispatch(res.result)
          console.log('[' + /\d\d\:\d\d\:\d\d/.exec(new Date())[0] + ']', 'getDeployConfig. ' + (performance.now() - start).toFixed(2) + 'ms');
          return resolve(res.result)
        } else {
          return reject(res.message)
        }
      })
    })
  },
  buildConfig: function () {
    let deployConf = {
      id: '', attribute: {
        position: {}, rotation: {}, scale: {}, ambientLight: {
          color: '', intensity: 1, position: {},
        }, directionalLight: {
          color: '', intensity: 1, position: {},
        },
      }, deviceInstances: [],
    }

    for (const obj of this.objects) {
      if (obj.userData.type === 'scene') {
        // 场景部署信息
        deployConf.id = obj.uuid
        deployConf.attribute = {
          position: {x: obj.position.x, y: obj.position.y, z: obj.position.z},
          rotation: {x: obj.rotation._x, y: obj.rotation._y, z: obj.rotation._z},
          scale: {x: obj.scale.x, y: obj.scale.y, z: obj.scale.z},
        }
      } else {
        // 所有设备配置信息
        // 灯光
        if (obj.type === 'AmbientLight') {
          //
          deployConf.attribute.ambientLight = {
            color: obj.color,
          }
        } else if (obj.type === 'DirectionalLight') {
          deployConf.attribute.ambientLight = {
            color: obj.color,
            intensity: obj.intensity,
            position: {x: obj.position.x, y: obj.position.y, z: obj.position.z},
          }
        } else {
          // 设备
          // 校验设备
          deployConf.deviceInstances.push({
            id: obj.uuid, attribute: {
              position: {x: obj.position.x, y: obj.position.y, z: obj.position.z},
              rotation: {x: obj.rotation._x, y: obj.rotation._y, z: obj.rotation._z},
              scale: {x: obj.scale.x, y: obj.scale.y, z: obj.scale.z},
            },
          })
        }
      }
    }
    console.log(deployConf)
    return deployConf
  },

  save: function () {
    console.log('>>>>>>> update scene config <<<<<<<<<')
    let deployConf = this.buildConfig()
    this.request.postAction('/scene/manage/setDeployConfig', deployConf).then((res) => {
      console.log(res)
      alert(res.message)
    })
  },
}

export {StorageRemote}
