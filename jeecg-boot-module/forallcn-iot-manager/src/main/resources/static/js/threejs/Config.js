/**
 * @author mrdoob / http://mrdoob.com/
 */

var Config = function () {

  var name = 'forllcn-iotcp-threejs';

  var storage = {

    'language': 'zh',
    'exportPrecision': 3,

    'autosave': false,
    'isDetail': false,

    'project/title': '物联网3D部署',
    'project/editable': false,
    'project/vr': false,

    'project/renderer/antialias': false,
    'project/renderer/shadows': false,
    'project/renderer/shadowType': 1, // PCF
    'project/renderer/physicallyCorrectLights': false,
    'project/renderer/toneMapping': 0, // NoToneMapping
    'project/renderer/toneMappingExposure': 1,
    'project/renderer/toneMappingWhitePoint': 1,

    'settings/history': false,

    'settings/shortcuts/translate': 'w',
    'settings/shortcuts/rotate': 'e',
    'settings/shortcuts/scale': 'r',
    'settings/shortcuts/undo': 'z',
    'settings/shortcuts/focus': 'f',

    'rightClicked': ''
  };

  if (window.localStorage[name] === undefined) {

    window.localStorage[name] = JSON.stringify(storage);

  } else {

    var data = JSON.parse(window.localStorage[name]);

    for (var key in data) {

      storage[key] = data[key];

    }

  }

  return {

    getKey: function (key) {

      return storage[key];

    },

    setKey: function () { // key, value, key, value ...
      let log
      for (var i = 0, l = arguments.length; i < l; i += 2) {
        storage[arguments[i]] = arguments[i + 1];
        log = `${arguments[i]} = ${arguments[i + 1]}`
      }

      window.localStorage[name] = JSON.stringify(storage);

      console.log('[' + /\d\d\:\d\d\:\d\d/.exec(new Date())[0] + ']', `将配置[${log}]保存到本地存储`);

    },

    clear: function () {

      delete window.localStorage[name];

    }

  };

};

export {Config};