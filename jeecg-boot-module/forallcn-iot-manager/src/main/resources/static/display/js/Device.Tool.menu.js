import {UIElement, UIPanel} from "../../js/threejs/libs/ui.js";
import DeviceToolTopology from "./Device.Tool.Topology.js";
import DeviceToolLabel from "./Device.Tool.Label.js";
import DeviceToolFunction from "./Device.Tool.Function.js";

export default class DeviceToolMenu {

  constructor(displayer) {
    let scope = this

    let signals = displayer.signals;
    let config = displayer.config

    let container = new UIPanel()
    container.setId('device-menu-panel')
    container.dom.addEventListener('mousedown', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })
    container.dom.addEventListener('click', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })
    container.dom.addEventListener('dblclick', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })
    this.container = container

    let menuTitle = new UIPanel()
    menuTitle.setClass('title')
    menuTitle.setTextContent('设备名称')
    menuTitle.dom.title = '点击查看设备信息'
    container.add(menuTitle)

    // 按钮面板
    let buttonPanel = new UIPanel()
    buttonPanel.setClass('menu-button-panel')
    container.add(buttonPanel)
    // 按钮-设备居中
    this.createCenterBtn(buttonPanel)
    // 按钮-设备拓扑
    this.createTopologyBtn(buttonPanel)
    // 按钮-删除当前设备标签
    this.createRemoveLabelBtn(buttonPanel)

    // 下拉菜单面板
    let dropMenuPanel = new UIPanel()
    dropMenuPanel.setClass('menu-drop-panel')
    this.container.add(dropMenuPanel)
    // 按钮菜单-设备标签
    dropMenuPanel.add(new DeviceToolLabel(displayer))
    // 分割线
    dropMenuPanel.add(this.createSplit())
    // TODO 按钮菜单-设备功能
    dropMenuPanel.add(new DeviceToolFunction(displayer))

    signals.deviceMenued.add((object) => scope.createDeviceMenu(menuTitle, object))

    let timer
    signals.cameraChanged.add(function () {
      container.setStyle('display', ['none'])
      let object = displayer.selected
      if (object && object.userData.rightMenu) {
        clearTimeout(timer)
        timer = setTimeout(() => {
          container.setStyle('display', ['block'])
          let p = displayer.transPosition(object.position)
          let menuP = scope.menuPosition(p)
          container.setStyle('left', [menuP.x + 'px'])
          container.setStyle('top', [menuP.y + 'px'])
        }, 200)
      }
    })
  }

  /**
   * 创建设备右键菜单
   * @param titlePanel 菜单标题
   * @param object 右键设备对象
   */
  createDeviceMenu = (titlePanel, object) => {
    if (object === null) {
      displayer.selected = null
      this.container.setStyle('display', ['none'])
    } else {
      this.container.setStyle('display', ['block'])
      let p = displayer.transPosition(object.position)
      let menuP = this.menuPosition(p)
      this.container.setStyle('left', [menuP.x + 'px'])
      this.container.setStyle('top', [menuP.y + 'px'])
      this.yyds()
      titlePanel.setTextContent(object.name)
    }
  }

  /**
   * 创建居中按钮
   * @param buttonPanel
   */
  createCenterBtn = (buttonPanel) => {
    const centerSVG = (function () {
      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
      svg.setAttribute('width', 24)
      svg.setAttribute('height', 24)
      svg.setAttribute('viewBox', '0 0 1024 1024')
      let path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      path.setAttribute('d', 'M874.048 533.333333C863.424 716.629333 716.629333 863.424 533.333333 874.048V917.333333a21.333333 21.333333 0 0 1-42.666666 0v-43.285333C307.370667 863.424 160.576 716.629333 149.952 533.333333H106.666667a21.333333 21.333333 0 0 1 0-42.666666h43.285333C160.576 307.370667 307.370667 160.576 490.666667 149.952V106.666667a21.333333 21.333333 0 0 1 42.666666 0v43.285333c183.296 10.624 330.090667 157.418667 340.714667 340.714667h42.816a21.333333 21.333333 0 1 1 0 42.666666H874.026667z m-42.752 0h-127.786667a21.333333 21.333333 0 0 1 0-42.666666h127.786667C820.778667 330.922667 693.056 203.221333 533.333333 192.704V320a21.333333 21.333333 0 0 1-42.666666 0V192.704C330.922667 203.221333 203.221333 330.944 192.704 490.666667H320a21.333333 21.333333 0 0 1 0 42.666666H192.704c10.517333 159.744 138.24 287.445333 297.962667 297.962667V704a21.333333 21.333333 0 0 1 42.666666 0v127.296c159.744-10.517333 287.445333-138.24 297.962667-297.962667zM512 554.666667a42.666667 42.666667 0 1 1 0-85.333334 42.666667 42.666667 0 0 1 0 85.333334z')
      path.setAttribute('fill', '#00fcff')
      path.setAttribute('p-id', '2328')
      svg.appendChild(path)
      return svg
    })()

    let centerBtn = new UIElement(centerSVG)
    centerBtn.setCursor('pointer')
    let enterRoot = new UIPanel()
    enterRoot.dom.title = '设备居中'
    enterRoot.setClass('tool-button')
    enterRoot.add(centerBtn)
    buttonPanel.add(enterRoot)
    centerBtn.dom.addEventListener("click", (e) => {
      let object = displayer.selected
      displayer.signals.deviceMenued.dispatch(null)
      displayer.signals.objectFocused.dispatch(object)
    })
  }

  /**
   * 创建拓扑按钮
   * @param buttonPanel
   */
  createTopologyBtn = (buttonPanel) => {
    const topologySVG = (function () {
      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
      svg.setAttribute('width', 24)
      svg.setAttribute('height', 24)
      svg.setAttribute('viewBox', '0 0 1024 1024')
      let path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      path.setAttribute('d', 'M820.48 728.32a118.4 118.4 0 0 0-24.32 0h-5.12l-17.92 5.76h-3.2a117.76 117.76 0 0 0-17.92 10.24l-5.76 4.48L640 658.56a198.4 198.4 0 0 0-4.48-256L768 270.72a117.12 117.12 0 0 0 24.96 15.36 116.48 116.48 0 0 0 19.2 5.76h3.2a118.4 118.4 0 1 0-92.16-92.16v5.76a116.48 116.48 0 0 0 5.12 16.64l3.2 5.76a117.12 117.12 0 0 0 7.68 14.08l3.84 5.12L613.76 384a200.32 200.32 0 0 0-275.2 10.24l-56.96-48v-5.76A116.48 116.48 0 0 0 292.48 320c0-3.84 0-7.04 3.2-10.88s0-6.4 0-9.6a115.84 115.84 0 0 0 0-22.4 118.4 118.4 0 1 0-93.44 115.2A117.12 117.12 0 0 0 231.04 384 118.4 118.4 0 0 0 256 373.76l6.4-5.12 57.6 47.36a200.96 200.96 0 0 0 19.2 259.84l-64 72.32h-8.32l-14.72-7.04h-8.32l-12.16-3.2a114.56 114.56 0 0 0-21.76 0 118.4 118.4 0 1 0 115.2 93.44 117.12 117.12 0 0 0-5.12-33.28 117.76 117.76 0 0 0-12.16-19.84L298.24 768l64-72.96a199.68 199.68 0 0 0 256-14.08l104.32 96.64V785.28a117.12 117.12 0 0 0-8.32 16v7.68a99.84 99.84 0 0 0-6.4 35.84 118.4 118.4 0 1 0 118.4-118.4zM96 279.68a86.4 86.4 0 0 1 172.16 0 85.12 85.12 0 0 1-6.4 30.72 88.32 88.32 0 0 1-5.76 14.72 87.04 87.04 0 0 1-8.32 12.16l-6.4 6.4a85.76 85.76 0 0 1-21.76 14.72h-4.48a85.12 85.12 0 0 1-30.08 6.4A86.4 86.4 0 0 1 96 279.68z m745.6-188.8a86.4 86.4 0 1 1-64 147.2 87.68 87.68 0 0 1-5.12-7.68 86.4 86.4 0 0 1-13.44-19.84 86.4 86.4 0 0 1 79.36-119.68zM209.28 933.12a86.4 86.4 0 1 1 0-172.16 85.12 85.12 0 0 1 31.36 6.4 87.68 87.68 0 0 1 15.36 8.96 87.04 87.04 0 0 1 11.52 7.68l5.76 6.4a85.76 85.76 0 0 1 14.72 21.76v4.48a85.12 85.12 0 0 1 6.4 30.72 86.4 86.4 0 0 1-85.12 85.76z m272.64-260.48A138.88 138.88 0 1 1 620.8 533.76a138.88 138.88 0 0 1-138.24 138.88z m338.56 260.48a86.4 86.4 0 0 1-86.4-86.4 85.12 85.12 0 0 1 6.4-32 87.68 87.68 0 0 1 11.52-17.92 87.68 87.68 0 0 1 6.4-9.6l4.48-4.48a86.4 86.4 0 0 1 23.68-16 85.12 85.12 0 0 1 32-6.4 86.4 86.4 0 1 1 0 172.16z')
      path.setAttribute('fill', '#00fcff')
      path.setAttribute('p-id', '5136')
      svg.appendChild(path)
      return svg
    })()
    let topologyBtn = new UIElement(topologySVG)
    topologyBtn.setCursor('pointer')
    let topologyRoot = new UIPanel()
    topologyRoot.dom.title = '设备拓扑'
    topologyRoot.setClass('tool-button')
    topologyRoot.add(topologyBtn)
    buttonPanel.add(topologyRoot)
    topologyBtn.dom.addEventListener("click", (e) => {
      const object = displayer.selected
      displayer.signals.deviceMenued.dispatch(null)
      console.log('拓扑对象', object)
      let topology = new DeviceToolTopology(displayer)
      topology.deviceTopology(object)
    })
  }

  /**
   * 创建删除标签按钮
   * @param buttonPanel
   */
  createRemoveLabelBtn = (buttonPanel) => {
    let topologyRoot = new UIPanel()
    topologyRoot.setCursor('pointer')
    topologyRoot.dom.title = '删除标签'
    topologyRoot.setStyle('background', ['url(./images/biaoqian.png) center no-repeat'])
    topologyRoot.setStyle('background-size', ['24px'])
    topologyRoot.setClass('tool-button')
    buttonPanel.add(topologyRoot)
    topologyRoot.dom.addEventListener("click", (e) => {
      const object = displayer.selected
      displayer.signals.deviceMenued.dispatch(null)
      console.log('删除标签', object)
      displayer.labelObjects.forEach((value, index, array) => {
        if (value.labelDialog.dom.classList[2] === object.uuid) {
          array.splice(index, 1)
          displayer.signals.removeLabel.dispatch(value.labelDialog)
        }
      })
    })
  }

  /**
   * 计算menu的平面坐标
   * @param point 点击位置
   */
  menuPosition = (point) => {
    let menuPosition = {x: point.x, y: point.y}
    // menu的尺寸
    let menu = document.getElementById('device-menu-panel')
    let width = menu.offsetWidth
    let height = menu.offsetHeight
    console.log(width, height)

    if (point.x + width > displayer.VIEWPORT_WIDTH) {
      // 右侧超出
      menuPosition.x = displayer.VIEWPORT_WIDTH - width
    }
    if (point.y + height > displayer.VIEWPORT_HEIGHT) {
      // 下侧超出
      menuPosition.y = displayer.VIEWPORT_HEIGHT - height
    }
    if (point.x < 0) {
      // 左侧超出
      menuPosition.x = 0
    }
    if (point.y < 0) {
      // 上侧超出
      menuPosition.y = 0
    }
    return menuPosition;
  }


  yyds = () => {
    // 查询所有label dom
    const labels = document.getElementsByClassName('label-dialog') || []
    for (let label of labels) {
      if (label.style.display === 'block') {
        let isIntersect = displayer.checkIntersect(label, this.container.dom);
      }
    }
  }

  createSplit = () => {
    let split = new UIPanel()
    split.setClass('split')
    return split;
  }
}