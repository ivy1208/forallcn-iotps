import {UIElement, UIPanel} from "../../js/threejs/libs/ui.js";
import DeviceToolFunctionDialog from "./Device.Tool.Function.Dialog.js";

export default class DeviceToolFunction {
  #functionListDom
  #dropdownClose
  #dropdownOpen
  #signals

  constructor(displayer) {
    let scope = this
    this.#signals = displayer.signals;

    // 下拉列表-设备功能
    let functionRoot = new UIPanel()
    functionRoot.dom.title = '设备功能'
    functionRoot.setClass('function-root')
    functionRoot.setCursor('pointer')

    let functionContent = new UIPanel()
    functionContent.setClass('content')
    functionContent.setTextContent("设备功能")
    functionRoot.add(functionContent)

    let arrowBtn = new UIPanel()
    arrowBtn.setClass('to-down')
    functionRoot.add(arrowBtn)

    /* 下拉菜单展开 */
    let dropdown = new UIPanel()
    dropdown.setClass("dropdown")
    dropdown.setStyle('display', ['none'])
    functionRoot.add(dropdown)

    const dropdownOpen = this.#dropdownOpen = () => {
      arrowBtn.setClass('to-up')
      dropdown.setStyle('display', ['block'])
    }
    const dropdownClose = this.#dropdownClose = () => {
      arrowBtn.setClass('to-down')
      dropdown.setStyle('display', ['none'])
    }

    functionRoot.dom.addEventListener("mousedown", (e) => {
      arrowBtn.dom.className === 'to-down' ? dropdownOpen() : dropdownClose()
    })

    let arrow = new UIPanel()
    arrow.setClass("arrow")
    dropdown.add(arrow)

    let functionListDom = document.createElement("ul")
    this.#functionListDom = functionListDom
    let functionList = new UIElement(functionListDom)
    dropdown.add(functionList)

    /* 右键菜单点击显示功能 */
    this.#signals.deviceMenued.add((object) => {
      /* 显示菜单时关闭功能下拉菜单 */
      dropdownClose()
      if (object !== null) {
        scope.buildFunctionList(object)
      }
    })

    return functionRoot
  }

  /**
   * 获取后台功能配置
   * @returns {Promise}
   * @param instanceId
   */
  getFunctionList = (instanceId) => {
    return new Promise((resolve, reject) => {
      displayer.request.getAction('display/getDeviceFunction', {
        instanceId: instanceId
      }).then((data) => {
        if (data.success) {
          resolve(data.result)
        } else {
          resolve(data.message)
        }
      }).catch((error) => {
        reject(error)
      })
    })
  }

  /**
   * 建立功能列表
   * @param object
   */
  buildFunctionList = (object) => {
    const scope = this
    this.#functionListDom.innerHTML = ''
    /* 调取所有可用功能配置 */
    this.getFunctionList(object.uuid).then(functions => functions.map((func) => {
      let li = document.createElement('li')
      let a = document.createElement('a')
      a.href = `javascript:;`
      a.innerHTML = func.name
      a.addEventListener('mousedown', e => {
        scope.#signals.deviceMenued.dispatch(null)
        scope.openDialog(func, object.position)
        scope.#dropdownClose()
      })
      li.appendChild(a)
      this.#functionListDom.appendChild(li)
    }))
  }

  /**
   * 打开功能框
   * @param func 功能配置
   * @param position 位置
   */
  openDialog(func, position) {
    let functionDialog = new DeviceToolFunctionDialog(displayer);
    functionDialog.createFunctionDialog(func, position)
  }
}