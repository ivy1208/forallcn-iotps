import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentRightAlarmCount {
  constructor(displayer) {

    this.request = displayer.request
    let container = new UIPanel()
    container.setClass('panel-alarm-count')
    container.dom.innerHTML = `
        <div class="device-fault">
          <p>设备事件</p>
          <small><span id="device-fault">0</span> 条</small>
        </div>
        <div class="data-alarm">
          <p>数据报警</p>
          <small><span id="data-alarm">0</span> 条</small>
        </div>
    `
    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => this.getCount(deployConfig.id))

    displayer.signals.deviceOffline.add((instances) => document.getElementById('device-fault').innerText ++)
  }

  getCount(sceneId){
    this.request.getAction('/display/getMessageEventStat', {
      sceneId: sceneId
    }).then((res) => {
      if (res.success) {
        document.getElementById('device-fault').innerText = 0
        res.result.map(stat => {
          if (stat.code === 'data_alarm') {
            document.getElementById('data-alarm').innerText = stat.count
          } else {
            let old = document.getElementById('device-fault').innerText - 0
            document.getElementById('device-fault').innerText = old + stat.count
          }
        })
      }
    });
  }
}
