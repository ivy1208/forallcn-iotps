import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentLeftDeviceState {
  // 所有设备在线状态比
  // TODO 当选择某一方案时显示具体方案下的设备状态比
  #sceneId;
  #stateChart;
  #option;
  constructor(displayer) {
    this.request = displayer.request
    this.sceneId
    let container = new UIPanel()
    container.setClass('panel_state')
    container.dom.innerHTML = `
      <div class="title">
        <img src="images/icon01.png"  alt="🈳"/>
        设备在线情况
      </div>
      <div class="state" id="state"></div>
    `
    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => this.initContainer(deployConfig.id))

    displayer.signals.deviceOnline.add((instances) => {
      this.#option.series[0].data[0].value += instances.length
      this.#option.series[0].data[1].value -= instances.length
    })
    displayer.signals.deviceOffline.add((instances) => {
      this.#option.series[0].data[0].value -= instances.length
      this.#option.series[0].data[1].value += instances.length
    })
  }

  initContainer(sceneId) {
    const that = this
    this.#option = {
      color: ['#32A8E6', '#EE5196'],
      tooltip: {
        trigger: 'item',
        formatter: '{b} : {c} ({d}%)',
      },
      series: [
        {
          name: '在线状态',
          type: 'pie',
          radius: [30, 70],
          center: ['50%', '50%'],
          data: [],
          //饼状图的外围标签
          label: {
            normal: {
              textStyle: {
                color: '#00f6ff'
              }
            }
          },
          emphasis: {
            label: {
              show: true,
              fontSize: '26',
              fontWeight: 'bold'
            }
          },
        },
      ],
    }
    this.#stateChart = echarts.init(document.getElementById('state'))

    this.request.getAction('/display/getDeviceState', {
      sceneId: sceneId, schemeId: null
    }).then((res) => {
      if (res.success) {
        const state = res.result
        this.#option.series[0].data.push({
          name: '在线',
          value: state['online'],
        })
        this.#option.series[0].data.push({
          name: '离线',
          value: state['offline'],
        })
        this.#stateChart.setOption(this.#option)
      }
    })
  }
}
