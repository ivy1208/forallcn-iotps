import * as THREE from '../../js/threejs/build/three.module.js'
import {Viewport} from "./Viewport.js";
import DeviceToolSearch from "./Device.Tool.Search.js";
import {UIPanel} from "../../js/threejs/libs/ui.js";

class ContentCenterTop {
  currentRenderer = null
  currentPmremGenerator = null

  constructor(displayer) {
    let config = displayer.config

    let container = new UIPanel()
    container.setId('center_top')
    container.setClass('center_top')

    container.add(new Viewport(displayer))
    container.add(new DeviceToolSearch(displayer).container)

    this.container = container

    this.createRenderer(
      config.getKey('project/renderer/antialias'),
      config.getKey('project/renderer/shadows'),
      config.getKey('project/renderer/shadowType'),
      config.getKey('project/renderer/toneMapping'),
      config.getKey('project/renderer/physicallyCorrectLights')
    )

    displayer.signals.setReaderSized.dispatch(displayer.VIEWPORT_WIDTH, displayer.VIEWPORT_HEIGHT)
  }

  /**
   * 创建渲染器
   * @param antialias 抗锯齿
   * @param shadows 阴影
   * @param shadowType 阴影类型
   * @param toneMapping 色调映射
   * @param toneMappingExposure 色调映射曝光
   * @param toneMappingWhitePoint 色调映射白点
   * @param physicallyCorrectLights 物理上正确的灯
   */
  createRenderer(antialias, shadows, shadowType, toneMapping, toneMappingExposure, toneMappingWhitePoint, physicallyCorrectLights) {
    var parameters = {antialias: antialias, alpha: true}

    if (this.currentRenderer !== null) {
      this.currentRenderer.dispose()
      this.currentPmremGenerator.dispose()
    }

    this.currentRenderer = new THREE.WebGLRenderer(parameters)
    this.currentPmremGenerator = new THREE.PMREMGenerator(this.currentRenderer)
    this.currentPmremGenerator.compileCubemapShader()
    this.currentPmremGenerator.compileEquirectangularShader()

    if (shadows) {
      this.currentRenderer.shadowMap.enabled = true
      this.currentRenderer.shadowMap.type = parseFloat(shadowType)
    }

    this.currentRenderer.toneMapping = parseFloat(toneMapping)
    this.currentRenderer.toneMappingExposure = toneMappingExposure
    this.currentRenderer.toneMappingWhitePoint = toneMappingWhitePoint
    this.currentRenderer.physicallyCorrectLights = physicallyCorrectLights

    displayer.signals.rendererChanged.dispatch(this.currentRenderer, this.currentPmremGenerator)
  }
}

export default ContentCenterTop