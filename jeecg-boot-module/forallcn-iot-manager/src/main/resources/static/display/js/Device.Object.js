import * as THREE from "../../js/threejs/build/three.module.js"
import {Object3D} from "../../js/threejs/src/core/Object3D.js"

export default class DeviceObject extends Object3D{

  constructor(objectUUID) {
    super();
    this.object = displayer.scene.getObjectByProperty('uuid', objectUUID);
  }

  /**
   * 移动对象
   * @param position
   * @param type
   */
  move = (position = {x:0,y:0,z:0}, type = 0) => {
    console.log('移动对象', this.object, position, type)
    if (this.object !== null) {
      let newPosition = new THREE.Vector3(position.x, position.y, position.z)
      if(type === 1) {
        // 相对位置
        newPosition.set(
          this.object.position.x + position.x,
          this.object.position.y + position.y,
          this.object.position.z + position.z
        )
      }

      if (this.object.position.distanceTo(newPosition) >= 0.01) {
        this.object.position.copy(newPosition)
        this.object.updateMatrixWorld(true)
        displayer.signals.objectChanged.dispatch(this.object)
      }
    }
  }

  zoom = (scale = {x:0,y:0,z:0}, type = 0) => {
    console.log('缩放对象', this.object, scale, type)
    if (this.object !== null) {
      let newScale = new THREE.Vector3(scale.x, scale.y, scale.z)
      if(type === 1) {
        // 相对位置
        newScale.set(
          this.object.scale.x * scale.x,
          this.object.scale.y * scale.y,
          this.object.scale.z * scale.z
        )
      }

      if (this.object.scale.distanceTo(newScale) >= 0.01) {
        this.object.scale.copy(newScale)
        this.object.updateMatrixWorld(true)
        displayer.signals.objectChanged.dispatch(this.object)
      }
    }
  }

  rotate = (vector3 = {x:0,y:1,z:0}, angle=0, type = 0) => {
    console.log('旋转对象', this.object, vector3, angle, type)
    if (this.object !== null) {
      let axis = new THREE.Vector3(vector3.x, vector3.y, vector3.z)
      let newAngle = angle
      if(type === 1) {
        // 相对位置
        this.object.rotateOnAxis(axis, angle);//绕axis轴旋转π/8
      } else {
        // 绝对
        this.object.rotation.set(vector3.x, vector3.y, vector3.z) // 设置x、y、z位置信息
      }
      this.object.updateMatrixWorld(true)
      displayer.signals.objectChanged.dispatch(this.object)
    }
  }
}