import {UIElement, UIPanel, UITabbedPanel} from "../../js/threejs/libs/ui.js";
import DeviceDetailModel from "./Device.Detail.Model.js";
import DeviceDetailInstance from "./Device.Detail.Instance.js";
import DeviceDetailData from "./Device.Detail.Data.js";
import DeviceDetailFunction from "./Device.Detail.Function.js";

export default class DeviceDetail {
  constructor(displayer) {
    let scope = this
    let strings = displayer.strings

    let container = new UIPanel()
    container.setId('detail-panel')
    container.setPosition('relative')
    container.setBackgroundColor('rgb(14 29 59 / 50%)')
    container.setWidth('100%')
    container.setHeight('100%')
    container.setStyle('display', ['none'])
    container.dom.style.flexDirection = 'column'
    container.onKeyUp(ev => {
      if (ev && ev.code === 'Escape') {
        ev.stopPropagation()
        closeDetail()
      }
    })
    this.container = container

    let header = new UIPanel()
    container.add(header)

    const closeDetail = () => {
      displayer.config.setKey('deviceDetailId', null)
      container.setStyle('display', ['none'])
    }

    const buttonSVG = (function () {
      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
      svg.setAttribute('width', 32)
      svg.setAttribute('height', 32)
      let path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      path.setAttribute('d', 'M 12,12 L 22,22 M 22,12 12,22')
      path.setAttribute('stroke', '#00fcff')
      svg.appendChild(path)
      return svg
    })()

    let close = new UIElement(buttonSVG)
    close.setPosition('absolute')
    close.setRight('3px')
    close.setWidth('32px')
    close.setHeight('32px')
    close.setCursor('pointer')
    header.add(close)

    // 关闭按钮点击事件
    close.onClick(function () {
      closeDetail()
    })

    let detailTab = new UITabbedPanel()
    detailTab.setId('detail-tab')

    detailTab.addTab('model', strings.getKey('device/detail/model'), new DeviceDetailModel(displayer).container)
    detailTab.addTab('instance', strings.getKey('device/detail/instance'), new DeviceDetailInstance(displayer).container)
    detailTab.addTab('function', strings.getKey('device/detail/function'), new DeviceDetailFunction(displayer).container)
    detailTab.addTab('data', strings.getKey('device/detail/data'), new DeviceDetailData(displayer).container)
    container.add(detailTab)
    detailTab.select('instance')

    detailTab.remove()

    displayer.signals.deviceDetailed.add(function (object) {
      detailTab.select('model')
      displayer.config.setKey('deviceDetailId', object.uuid)
      container.setDisplay('flex')
    })
  }


}