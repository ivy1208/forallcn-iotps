import {UIPanel} from "../../js/threejs/libs/ui.js";
import {UIOutliner} from "../../js/threejs/libs/ui.three.js";

export default class ContentRightAlarmRecord {
  constructor(displayer) {

    this.request = displayer.request
    let container = new UIPanel()
    container.setClass('panel-alarm-record')
    container.dom.innerHTML = `<div class="title"><img src="images/icon05.png"  alt=""/>事件记录`

    let alarmList = new UIOutliner(displayer)
    alarmList.setClass('alarm-list')
    container.add(alarmList)

    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => {
      // this.getCount(deployConfig.id).then(opt => {
      //   alarmList.setOptions(opt)
      //   alarmList.selectIndex(opt.length-1)
      // })
    })

    displayer.signals.deviceOnline.add((msg) => appendNode('设备上线', msg))
    displayer.signals.deviceOffline.add((msg) => appendNode('设备离线', msg))

    let appendNode = (state, msg) => {
      let alarmOptions = document.getElementsByClassName('alarm-list')
      let option = document.createElement('div')
      option.className = 'option active'
      option.innerHTML = `${/\d\d:\d\d:\d\d/.exec(new Date(msg.createTime))} ${msg.message}`
      alarmOptions.item(0).appendChild(option)
      alarmList.selectIndex(alarmOptions.item(0).childElementCount-1)
    }
  }

  getCount(sceneId) {
    return new Promise((resolve, reject) => {
      this.request.getAction('/display/listMessageEvent', {
        sceneId: sceneId
      }).then((res) => {
        if (res.success) {
          let alarmOptions = res.result.map(msg => {
            let option = document.createElement('div')
            option.innerHTML = `${msg.createTime} ${msg.message}`
            return option
          })
          resolve(alarmOptions)
        } else{
          reject(res.msg);
        }
      });
    });
  }
}
