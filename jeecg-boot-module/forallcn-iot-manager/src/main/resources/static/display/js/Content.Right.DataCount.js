import {UIPanel} from "../../js/threejs/libs/ui.js";

let data = [
  {deviceId: "temperature-1", deviceName: "温度传感器-01", dataCount: 9479},
  {deviceId: "temperature-2", deviceName: "温度传感器-02", dataCount: 9480}
]
export default class ContentRightDataRecord {
  #countChart
  #option = {
    color: ['#32A8E6', '#EE5196'],
    tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'shadow | line' | 'shadow'
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: {
      type: 'value',
      axisLine: {
        show: true,
        lineStyle: {
          color: '#00f6ff'
        }
      },
    },
    yAxis: {
      type: 'category',
      data: [],
      axisLine: {
        show: true,
        lineStyle: {
          color: '#00f6ff'
        }
      },
    },
    series: [
      {
        name: '数据量',
        type: 'bar',
        data: [],
      }
    ]
  };

  constructor(displayer) {
    let scope = this

    this.request = displayer.request
    let container = new UIPanel()
    container.setClass('panel-data-count')
    container.dom.innerHTML = `<div class="title"><img src="images/icon06.png"  alt="" title="数据统计"/> 数据统计</div>`
    let countPanel = new UIPanel()
    countPanel.addClass('data-count')
    container.add(countPanel)

    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => this.getDataCount(deployConfig.id).then(dataCount => {
      console.log('getDataCount')
      dataCount.map(data => {
        scope.#option.yAxis.data.push(data.deviceName)
        scope.#option.series[0].data.push({
          deviceId: data.deviceId,
          value: data.dataCount
        })
      })
      scope.#countChart = echarts.init(countPanel.dom)
      scope.#countChart.setOption(scope.#option)
    }))

    displayer.signals.dataReceived.add(datas => {
      datas
        .filter(receive => receive['latestData']['value'])
        .map(receive => {
          scope.#option.series[0].data
            .filter(data => data.deviceId === receive.instanceId)
            .map(data => data.value++)
        })
      scope.#countChart.setOption(scope.#option)
    })
  }

  getDataCount(sceneId) {
    let scope = this
    scope.#option.yAxis.data = []
    return new Promise((resolve, reject) => {
      this.request.getAction('/display/getDataCount', {
        sceneId: sceneId
      }).then((res) => {
        if (res.success) {
          resolve(res.result)
        } else {
          reject(res.msg);
        }
      });
    });
  }
}
