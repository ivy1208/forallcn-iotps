import {UIPanel} from "../../js/threejs/libs/ui.js";
import ContentRightAlarmCount from "./Content.Right.AlarmCount.js";
import ContentRightAlarmRecord from "./Content.Right.AlarmRecord.js";
import ContentRightDataCount from "./Content.Right.DataCount.js"

export default class ContentRight {
  #inner = `
        
        <div class="data_box">
          
        </div>
    `

  constructor(displayer) {
    let alarmCount = new ContentRightAlarmCount(displayer)
    let alarmRecord = new ContentRightAlarmRecord(displayer)
    // let dataRecord = new ContentRightDataRecord(displayer)
    let dataCount = new ContentRightDataCount(displayer)

    let container = new UIPanel()
    container.setClass('content_right')
    container.add(alarmCount.container)
    container.add(alarmRecord.container)
    // container.add(dataRecord.container)
    container.add(dataCount.container)
    this.container = container
  }
}
