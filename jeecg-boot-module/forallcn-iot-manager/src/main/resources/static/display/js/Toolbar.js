import {UIElement, UIPanel} from "../../js/threejs/libs/ui.js";
import DeviceToolTopology from "./Device.Tool.Topology.js";

export default class Toolbar {
  constructor(displayer) {
    let scope = this
    let signals = displayer.signals;

    let container = new UIPanel()
    container.setId('toolbar')
    container.dom.addEventListener('mousedown', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })
    container.dom.addEventListener('click', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })
    container.dom.addEventListener('dblclick', ev => {
      ev.stopImmediatePropagation()
      ev.stopPropagation()
    })

    // 工具栏左侧功能按钮
    let toolHeard = document.createElement('a')
    toolHeard.id = 'tool-handle'
    toolHeard.href = 'javascript:;'
    toolHeard.title = '工具栏'
    container.add(new UIElement(toolHeard))

    // 工具栏主体
    let toolBody = new UIPanel()
    toolBody.setId('tool-body')
    toolBody.setDisplay('flex')
    container.add(toolBody)

    toolHeard.addEventListener('mouseup', function (event) {
      event.stopPropagation()
      if (scope.isHide(toolBody.dom)) {
        toolBody.setStyle('display', ['flex'])
      } else {
        toolBody.setStyle('display', ['none'])
      }
    })
    toolHeard.addEventListener('mousedown', function (event) {
      event.stopPropagation()
    })


    this.container = container

    // 设备搜索按钮
    let searchBtn = document.createElement('a')
    searchBtn.id = 'search-button'
    searchBtn.className = 'tool-button'
    searchBtn.href = 'javascript:;'
    searchBtn.title = '搜索'
    toolBody.add(new UIElement(searchBtn))

    searchBtn.addEventListener('mouseup', function (event) {
      event.stopPropagation()
      signals.searchStarted.dispatch()
    })
    searchBtn.addEventListener('mousedown', function (event) {
      event.stopPropagation()
    })

    // 设备拓扑关系按钮
    let topologyBtn = document.createElement('a')
    topologyBtn.id = 'topology-button'
    topologyBtn.className = 'tool-button'
    topologyBtn.href = 'javascript:;'
    topologyBtn.title = '拓扑关系'
    toolBody.add(new UIElement(topologyBtn))
    topologyBtn.addEventListener('click', function (event) {
      let deviceTopology = new DeviceToolTopology(displayer)
      deviceTopology.deviceTopologyAll()
    })

    // 设备标签关系按钮
    let labelRemoveBtn = document.createElement('a')
    labelRemoveBtn.id = 'label-remove-button'
    labelRemoveBtn.className = 'tool-button'
    labelRemoveBtn.href = 'javascript:;'
    labelRemoveBtn.title = '移除所有标签'
    toolBody.add(new UIElement(labelRemoveBtn))
    labelRemoveBtn.addEventListener('click', function (event) {
      while (displayer.labelObjects.length > 0) {
        displayer.labelObjects[0] = null
        displayer.labelObjects.splice(0, 1)
      }
      signals.removeLabel.dispatch()
    })

    // 部署重载按钮
    let deployReloadBtn = document.createElement('a')
    deployReloadBtn.id = 'deploy-reload-button'
    deployReloadBtn.className = 'tool-button'
    deployReloadBtn.href = 'javascript:;'
    deployReloadBtn.title = '部署重载'
    toolBody.add(new UIElement(deployReloadBtn))
    deployReloadBtn.addEventListener('click', () => signals.deployReload.dispatch())

    // 显示阴影按钮
    let showShadowBtn = document.createElement('a')
    showShadowBtn.id = 'show-shadow-button'
    showShadowBtn.className = 'tool-button'
    showShadowBtn.href = 'javascript:;'
    showShadowBtn.title = '显示阴影'
    toolBody.add(new UIElement(showShadowBtn))
    showShadowBtn.addEventListener('click', () => signals.showShadow.dispatch())

    // 场景复位按钮
    let sceneResetBtn = document.createElement('a')
    sceneResetBtn.id = 'scene-reset-button'
    sceneResetBtn.className = 'tool-button'
    sceneResetBtn.href = 'javascript:;'
    sceneResetBtn.title = '场景复位'
    toolBody.add(new UIElement(sceneResetBtn))
    sceneResetBtn.addEventListener('click', () => {
      displayer.signals.objectFocused.dispatch(sceneGroup)
    })
  }

  // 判断是否为隐藏(css)样式
  isHide(obj) {
    var ret = obj.style.display === "none" ||
        obj.style.display === "" ||
        (obj.currentStyle && obj.currentStyle === "none") ||
        (window.getComputedStyle && window.getComputedStyle(obj, null).display === "none")
    return ret;
  }
}