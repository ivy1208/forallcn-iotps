import {UIPanel, UIRow, UIText} from '../../js/threejs/libs/ui.js'

export default class DeviceDetailInstance {
  constructor(displayer) {
    let scope = this
    let request = displayer.request

    let container = new UIPanel()
    container.dom.style.padding = '20px'
    container.dom.style.overflowY = 'auto'
    container.dom.style.height = `${displayer.VIEWPORT_HEIGHT - 35 - 40}px`
    container.dom.style.color = '#00fcff'

    this.container = container

    let instCodeRow = new UIRow()
    let instCode = new UIText()
    instCodeRow.add(new UIText('实例标识：').setWidth('120px'))
    instCodeRow.add(instCode)
    container.add(instCodeRow)

    let instNameRow = new UIRow()
    let instName = new UIText()
    instNameRow.add(new UIText('实例名称：').setWidth('120px'))
    instNameRow.add(instName)
    container.add(instNameRow)

    let instRow = new UIRow()
    let instSceneSchemeBy = new UIText()
    instRow.add(new UIText('方案名称：').setWidth('120px'))
    instRow.add(instSceneSchemeBy)
    container.add(instRow)

    let instStatusRow = new UIRow()
    let instStatus = new UIText()
    instStatusRow.add(new UIText('在线状态：').setWidth('120px'))
    instStatusRow.add(instStatus)
    container.add(instStatusRow)

    let instOnlineTimeRow = new UIRow()
    let instOnlineTime = new UIText()
    instOnlineTimeRow.add(new UIText('在线时长：').setWidth('120px'))
    instOnlineTimeRow.add(instOnlineTime)
    container.add(instOnlineTimeRow)

    let instCreateByRow = new UIRow()
    let instCreateBy = new UIText()
    instCreateByRow.add(new UIText('创建者：').setWidth('120px'))
    instCreateByRow.add(instCreateBy)
    container.add(instCreateByRow)

    let instCreateTimeRow = new UIRow()
    let instCreateTime = new UIText()
    instCreateTimeRow.add(new UIText('创建时间：').setWidth('120px'))
    instCreateTimeRow.add(instCreateTime)
    container.add(instCreateTimeRow)

    let instDescriptionRow = new UIRow()
    let instDescription = new UIText()
    instDescriptionRow.add(new UIText('描述：').setWidth('200px'))
    instDescriptionRow.add(instDescription)
    container.add(instDescriptionRow)

    container.add(new UIRow().add(new UIText('扩展参数：').setWidth('120px')))

    let extendParamPanel = new UIPanel()
    container.add(extendParamPanel)

    let extendParamsPanel

    let updateUI = (instance) => {
      instCode.setValue(instance['code'])
      instName.setValue(instance['name'])
      instSceneSchemeBy.setValue(instance['scheme'])
      instStatus.setValue(instance['status'])
      instOnlineTime.setValue(/\d\d:\d\d:\d\d/.exec(String(new Date().getTime() - new Date(instance['statusUpdateTime']).getTime())))
      instCreateBy.setValue(instance['createUser'])
      instCreateTime.setValue(instance['createTime'])
      instDescription.setValue(instance['description'])

      // 1、先删除扩展参数
      if (extendParamsPanel !== undefined) {
        container.remove(extendParamsPanel)
      }
      // 2、再动态添加 扩展参数
      extendParamsPanel = this.buildExtendParams(instance['extentParams'] || []);
      container.add(extendParamsPanel)
    }

  }

  buildExtendParams(extentParams) {
    let extendParamsPanel = new UIPanel()
    for (const func of extentParams) {
      let funcPanel = new UIPanel()
      funcPanel.setClass('function-panel')
      funcPanel.dom.style.borderTop = '1px #0ffcff solid'
      funcPanel.dom.style.marginTop = '20px'
      funcPanel.dom.style.padding = '10px 10px 10px 20px' // 上 右 下 左
      funcPanel.dom.innerHTML = `<div style="padding: 10px; font-weight: bold">${func['funcName']}</div>`
      func['inputParams']
        .filter(inputParam => inputParam.inputMode === 'ins_input')
        .map(extendParam => {
          let extendParamRow = new UIRow()
          let value = new UIText(extendParam.value)
          extendParamRow.add(new UIText(extendParam.name).setWidth('120px'))
          extendParamRow.add(value)
          funcPanel.add(extendParamRow)
        })
      extendParamsPanel.add(funcPanel)
    }
    return extendParamsPanel
  }
}