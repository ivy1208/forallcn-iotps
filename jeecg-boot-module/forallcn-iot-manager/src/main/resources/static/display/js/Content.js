import ContentLeft from './Content.Left.js'
import ContentRight from './Content.Right.js'
import ContentCenter from './Content.Center.js'
import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class Content {
  constructor(displayer) {

    let container = new UIPanel()
    container.setId('content')
    container.add(new ContentLeft(displayer).container)
    container.add(new ContentCenter(displayer).container)
    container.add(new ContentRight(displayer).container)

    this.dom = container.dom
  }
}
