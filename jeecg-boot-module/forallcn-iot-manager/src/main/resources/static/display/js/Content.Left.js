import ContentLeftDeviceCount from './Content.Left.DeviceCount.js'
import ContentLeftDeviceState from './Content.Left.DeviceState.js'
import ContentLeftDeviceList from './Content.Left.DeviceList.js'
import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentLeft {
  constructor(displayer) {
    let count = new ContentLeftDeviceCount(displayer)
    let state = new ContentLeftDeviceState(displayer)
    let list = new ContentLeftDeviceList(displayer)

    let container = new UIPanel()
    container.setClass('content_left')
    container.add(count.container)
    container.add(state.container)
    container.add(list.container)
    this.container = container
  }
}