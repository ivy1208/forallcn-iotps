import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class Header {
  #inner = `
      <span>物联网平台数据统计</span>
      <span id="localtime" style=""></span>
  `

  constructor(displayer) {
    let container = new UIPanel()
    container.setId('header')
    container.setClass('header')
    container.dom.innerHTML = this.#inner

    this.dom = container.dom

    // 手动创建时间span
    let localtime = document.createElement('span')
    localtime.id = 'localtime'
    localtime.style = `
      font-size: 14px;
      position: absolute;
      right: 30px;
      top: -20px;
    `
    container.dom.appendChild(localtime)

    localtime.onload = window.setInterval(() => {
      localtime.innerHTML = this.showLocale(new Date())
    }, 1000)
  }

  showLocale(objD) {
    let str, colorhead, colorfoot
    let yy = objD.getYear()
    if (yy < 1900) yy = yy + 1900
    let MM = objD.getMonth() + 1
    if (MM < 10) MM = '0' + MM
    let dd = objD.getDate()
    if (dd < 10) dd = '0' + dd
    let hh = objD.getHours()
    if (hh < 10) hh = '0' + hh
    let mm = objD.getMinutes()
    if (mm < 10) mm = '0' + mm
    let ss = objD.getSeconds()
    if (ss < 10) ss = '0' + ss
    let ww = objD.getDay()
    if (ww === 0) colorhead = '<font color="#ffffff">'
    if (ww > 0 && ww < 6) colorhead = '<font color="#ffffff">'
    if (ww === 6) colorhead = `<font color="#ffffff">`
    if (ww === 0) ww = '星期日'
    if (ww === 1) ww = '星期一'
    if (ww === 2) ww = '星期二'
    if (ww === 3) ww = '星期三'
    if (ww === 4) ww = '星期四'
    if (ww === 5) ww = '星期五'
    if (ww === 6) ww = '星期六'
    colorfoot = '</font>'
    str = colorhead + yy + '-' + MM + '-' + dd + ' ' + hh + ':' + mm + ':' + ss + '  ' + ww + colorfoot
    return str
  }
}
