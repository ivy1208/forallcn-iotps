import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentCenterButton {
  #inner = `
      <table class="panel-table">
        <thead>
          <tr class="panel-table-title">
            <th colspan="7"><img src="images/icon04.png" alt="🈳" /> 方案列表</th>
          </tr>
          <tr class="panel-table-thead-column">
            <td>方案名称</td>
            <td>设备总数量</td>
            <td>在线数量</td>
            <td>离线数量</td>
            <td>未激活数量</td>
            <td>操作</td>
          </tr>
        </thead>
        <tbody id="scheme-list">
        </tbody>
      </table>
    `

  constructor(displayer) {
    let that = this
    let container = new UIPanel()
    container.setId('center_bot')
    container.setClass('center_bot')
    container.dom.innerHTML = this.#inner

    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => {
      console.log('getSceneSchemeCount')
      displayer.request.getAction('/display/getSceneSchemeCount', {sceneId: deployConfig.id})
          .then(res => {
            if (res.success) {
              that.buildSchemeRow(res.result)
            }
          });
    });

  }

  buildSchemeRow(schemes) {
    let rows = ''
    for (const scheme of schemes) {
      rows += `
        <tr class="panel-table-tbody">
          <td>${scheme.name}</td>
          <td>${scheme.total}</td>
          <td>${scheme.online}</td>
          <td>${scheme.offline}</td>
          <td>${scheme.noAction}</td>
          <td>
            <button class="scheme-list-b2">切换</button>
          </td>
        </tr>`
    }
    document.getElementById('scheme-list').innerHTML = rows
  }

  computTotalDevice() {
  }

  computOnlineDevice() {
  }
}