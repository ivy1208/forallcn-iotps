import {UIPanel, UIRow, UIText} from "../../js/threejs/libs/ui.js";

export default class DeviceDetailModel {
  constructor(displayer) {
    let scope = this

    let strings = displayer.strings
    let signals = displayer.signals
    let request = displayer.request

    let container = new UIPanel()
    container.dom.style.padding = '20px'
    container.dom.style.overflowY = 'auto'
    container.dom.style.height = `${displayer.VIEWPORT_HEIGHT - 35 - 40}px`
    container.dom.style.color = '#00fcff'

    // id
    let modelIdRow = new UIRow()
    let modelId = new UIText()
    modelIdRow.add(new UIText('模型主键：').setWidth('120px'))
    modelIdRow.add(modelId)
    container.add(modelIdRow)

    // code
    let modelCodeRow = new UIRow()
    let modelCode = new UIText()
    modelCodeRow.add(new UIText('模型标识：').setWidth('120px'))
    modelCodeRow.add(modelCode)
    container.add(modelCodeRow)

    // name
    let modelNameRow = new UIRow()
    let modelName = new UIText()
    modelNameRow.add(new UIText('模型名称：').setWidth('120px'))
    modelNameRow.add(modelName)
    container.add(modelNameRow)

    // type: "gateway"
    let modelTypeRow = new UIRow()
    let modelType = new UIText()
    modelTypeRow.add(new UIText('模型类型：').setWidth('120px'))
    modelTypeRow.add(modelType)
    container.add(modelTypeRow)

    // threeModelFile: "temp/USR-N540_1604727810067.json"
    let modelFileRow = new UIRow()
    let modelFile = new UIText()
    modelFileRow.add(new UIText('模型文件：').setWidth('120px'))
    modelFileRow.add(modelFile)
    container.add(modelFileRow)

    // state: 0
    let modelStateRow = new UIRow()
    let modelState = new UIText()
    modelStateRow.add(new UIText('模型状态：').setWidth('120px'))
    modelStateRow.add(modelState)
    container.add(modelStateRow)

    // linkProtocolBy: "1293442241930022914"
    let modelLinkProtocolRow = new UIRow()
    let modelLinkProtocol = new UIText()
    modelLinkProtocolRow.add(new UIText('链接方式：').setWidth('120px'))
    modelLinkProtocolRow.add(modelLinkProtocol)
    container.add(modelLinkProtocolRow)

    // dataProtocolBy: "1293444635560595458"
    let modelDataProtocolRow = new UIRow()
    let modelDataProtocol = new UIText()
    modelDataProtocolRow.add(new UIText('数据解析协议：').setWidth('120px'))
    modelDataProtocolRow.add(modelDataProtocol)
    container.add(modelDataProtocolRow)
    // createBy: "admin"
    let modelCreateUserRow = new UIRow()
    let modelCreateUser = new UIText()
    modelCreateUserRow.add(new UIText('创建者：').setWidth('120px'))
    modelCreateUserRow.add(modelCreateUser)
    container.add(modelCreateUserRow)

    // createTime: "2020-08-12 00:00:00"
    let modelCreateTimeRow = new UIRow()
    let modelCreateTime = new UIText()
    modelCreateTimeRow.add(new UIText('创建时间：').setWidth('120px'))
    modelCreateTimeRow.add(modelCreateTime)
    container.add(modelCreateTimeRow)

    // description: null
    let modelDescriptionRow = new UIRow()
    let modelDescription = new UIText().setWidth('200px')
    modelDescriptionRow.add(new UIText('描述：').setWidth('120px'))
    modelDescriptionRow.add(modelDescription)
    container.add(modelDescriptionRow)

    this.container = container

    let updateUI = (model) => {
      modelId.setValue(model['id'])
      modelCode.setValue(model['code'])
      modelName.setValue(model['name'])
      modelType.setValue(model['type'])
      modelFile.setValue(model['threeModelFile'])
      modelState.setValue(model['state'] === 1 ? '有效' : '无效')
      modelLinkProtocol.setValue(model['linkProtocolBy'])
      modelDataProtocol.setValue(model['dataProtocolBy'])
      modelCreateUser.setValue(model['createBy'])
      modelCreateTime.setValue(model['createTime'])
      modelDescription.setValue(model['description'])
    }

    displayer.signals.deviceDetailed.add(function (object) {
      request.getAction('/display/getModelById', {id: object.userData.model})
        .then(res => {
          console.log(res)
          if (res.success) {
            updateUI(res.result)
          }
        })
    })
  }


}