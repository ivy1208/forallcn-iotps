import {UIElement, UIInput, UIPanel} from "../../js/threejs/libs/ui.js";
import {UIOutliner} from "../../js/threejs/libs/ui.three.js";

export default class DeviceToolSearch {
  constructor(displayer) {
    let scope = this

    let container = new UIPanel()
    container.setId('search-panel')
    container.setPosition('relative')
    container.setBackgroundColor('rgb(14 29 59 / 50%)')
    container.setWidth('100%')
    container.setHeight('100%')
    container.setStyle('display', ['none'])
    container.onKeyUp(ev => {
      if (ev && ev.code === 'Escape') {
        ev.stopPropagation()
        closeSearch()
      }
    })
    this.container = container

    let header = new UIPanel()
    header.setPadding('10px')
    container.add(header)

    const closeSearch = () => {
      searchBox.dom.value = ''
      candidate.setOptions([])
      candidate.setStyle('display', ['none'])
      container.setStyle('display', ['none'])
    }

    const buttonSVG = (function () {
      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
      svg.setAttribute('width', 32)
      svg.setAttribute('height', 32)
      let path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      path.setAttribute('d', 'M 12,12 L 22,22 M 22,12 12,22')
      path.setAttribute('stroke', '#00fcff')
      svg.appendChild(path)
      return svg
    })()

    let close = new UIElement(buttonSVG)
    close.setPosition('absolute')
    close.setTop('3px')
    close.setRight('1px')
    close.setCursor('pointer')
    header.add(close)

    let searchBox = new UIInput('')
    searchBox.setId('search-box')
    searchBox.dom.addEventListener('blur', () => searchBox.dom.focus())
    container.add(searchBox)

    let candidate = new UIOutliner(displayer)
    candidate.setId('candidate')
    candidate.setBackgroundColor('rgb(14 29 59 / 50%)')
    candidate.setWidth('300px')
    candidate.setStyle('display', ['none'])
    candidate.onClick(function () {
      displayer.selectByUuid(candidate.getValue())
      displayer.focusByUuid(candidate.getValue())
      closeSearch()
    })
    container.add(candidate)

    searchBox.dom.addEventListener('input', function () {
      let options = []
      const value = this.value
      console.log(value)
      // 不输入，不显示
      if (value === undefined || value === '') {
        candidate.setStyle('display', ['none'])
      } else {
        candidate.setStyle('display', ['block'])
      }
      options = displayer.deployConfig.deviceInstances
        .filter(instance => instance.name.indexOf(value) !== -1)
        .map(instance => scope.buildOption(instance));
      candidate.setOptions(options)
    })

    // 关闭按钮点击事件
    close.onClick(function () {
      closeSearch()
    })

    displayer.signals.searchStarted.add(() => {
      container.setDisplay('flex')
      searchBox.dom.focus();
    })
    displayer.signals.searchEnded.add(() => {
      container.setStyle('display', ['none'])
    })
  }

  buildOption(instance) {
    let option = document.createElement('div')
    option.draggable = false
    option.innerHTML = `<a href="javascript:;">${instance.name}</a>`
    option.value = instance.id
    option.style.margin = '8px 0px'
    return option
  }

}