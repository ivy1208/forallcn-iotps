import {UIPanel} from "../../js/threejs/libs/ui.js";
import {UIOutliner} from "../../js/threejs/libs/ui.three.js";

export default class ContentRightDataRecord {
  constructor(displayer) {

    this.request = displayer.request
    let container = new UIPanel()
    container.setClass('panel-data-record')
    container.dom.innerHTML = `
        <div class="title"><img src="images/icon06.png"  alt="" title="24小时内的数据记录"/> 数据记录</div>
    `

    let dataList = new UIOutliner(displayer)
    dataList.setClass('data-list')
    container.add(dataList)

    this.container = container

    displayer.signals.deployConfigLoaded.add(deployConfig => this.getData(deployConfig.id).then(opt => {
        console.log('getDataRecord')
        dataList.setOptions(opt)
        dataList.selectIndex(opt.length - 1)
    }))

    displayer.signals.dataReceived.add(datas => {
      datas
        .filter(data => data.latestData['value'] !== undefined)
        .map(data => {
          let dataOptions = document.getElementsByClassName('data-list')
          let option = document.createElement('div')
          option.className = 'option active'
          option.innerHTML = `${/\d\d:\d\d:\d\d/.exec(new Date(data.latestData['reportTime']))} ${data.instanceName} ${data.name}：${data.latestData['value']}${data.metadata.unitSymbol}`
          dataOptions.item(0).appendChild(option)
          dataList.selectIndex(dataOptions.item(0).childElementCount - 1)
        })
    })
  }

  getData(sceneId) {
    return new Promise((resolve, reject) => {
      this.request.getAction('/display/listLatestReportData', {
        sceneId: sceneId
      }).then((res) => {
        if (res.success) {
          let alarmOptions = res.result
            .filter(data => data.latestData['value'] !== null && data.latestData['value'] !== undefined)
            .map(data => {
              let option = document.createElement('div')
              option.innerHTML = `${/\d\d:\d\d:\d\d/.exec(data.latestData['reportTime'])} ${data.instanceName} ${data.name}：${data.latestData['value']}${data.metadata.unitSymbol}`
              return option
            })
          resolve(alarmOptions)
        } else {
          reject(res.msg);
        }
      });
    });
  }
}
