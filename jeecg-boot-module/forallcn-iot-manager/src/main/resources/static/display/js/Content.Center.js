import ContentCenterTop from './Content.Center.Top.js'
import ContentCenterButton from './Content.Center.Button.js'
import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentCenter {
  constructor(displayer) {
    let top = new ContentCenterTop(displayer)
    let button = new ContentCenterButton(displayer)

    let container = new UIPanel()
    container.setClass('content_center')
    container.add(top.container)
    container.add(button.container)
    this.container = container
  }
}
