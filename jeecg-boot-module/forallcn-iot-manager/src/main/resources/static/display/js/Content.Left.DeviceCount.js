import {UIPanel} from "../../js/threejs/libs/ui.js";

export default class ContentLeftDeviceCount {
  // 所有设备数量统计比
  // TODO 当选择某一方案时显示当前方案下设备数量统计比

  constructor(displayer) {
    this.request = displayer.request
    let container = new UIPanel()
    container.setId('panel_count')
    container.setClass('panel_count')
    this.container = container
    displayer.signals.deployConfigLoaded.add(deployConfig => this.initContainer(deployConfig))
  }

  initContainer(deployConfig) {
    const that = this
    const option = {
      color: ['#32A8E6', '#FF954A', '#45E2CF', '#EE5196', '#Eaf'],
      tooltip: {
        trigger: 'item',
        formatter: '{b} : {c} ({d}%)',
      },
      series: [
        {
          name: '设备',
          type: 'pie',
          radius: [30, 60],
          center: ['50%', '50%'],
          data: [],
          //饼状图的外围标签
          label: {
            normal: {
              textStyle: {
                color: '#00f6ff'
              }
            }
          },
        },
      ],
    }
    let countChart = echarts.init(this.container.dom)
    this.request.getAction('/display/getDeviceCount', {
      sceneId: deployConfig.id, schemeId: null
    }).then((res) => {
      if (res.success) {
        const list = res.result
        for (const item of list) {
          option.series[0].data.push({
            name: item['modelName'],
            value: item['deviceCount'],
          })
        }
        countChart.setOption(option)
      }
    })
  }
}
