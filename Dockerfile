# 基础镜像
FROM java:8

# author
MAINTAINER zhouwr

# 设置日期
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 声明服务运行在8080端口
EXPOSE 8080

RUN mkdir -p /home/docker/iotps

WORKDIR /home/docker/iotps

# 复制当前电脑路径下jar文件：jeecg-boot-system-2.2.1.jar，到虚拟机路径下
ADD ./jeecg-boot-system/target/jeecg-boot-system-2.2.1.jar /home/docker/iotps/iotps-2.0.jar
#同步时间
RUN bash -c 'touch /home/docker/iotps/iotps-2.0.jar'

# 启动网关服务
ENTRYPOINT ["java","-jar","/home/docker/iotps/iotps-2.0.jar"]