Jeecg-Boot 低代码开发平台
===============

/Users/zhouwenrong/emqx/bin/emqx start

当前最新版本： 2.2.1（发布日期：20200713）

## 后端技术架构

- 基础框架：Spring Boot 2.1.3.RELEASE

- 持久层框架：Mybatis-plus_3.3.2

- 安全框架：Apache Shiro 1.4.0，Jwt_3.7.0

- 数据库连接池：阿里巴巴Druid 1.1.17

- 缓存框架：redis

- 日志打印：logback

- 3D框架：threejs

- 网络服务框架：netty

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。



## 开发环境

- 语言：Java 8

- IDE(JAVA)： Eclipse安装lombok插件 或者 IDEA

- 依赖管理：Maven

- 数据库：MySQL5.0  &  Oracle 11g

- 缓存：Redis


## 技术文档


- 在线演示 ：  [http://boot.jeecg.com](http://boot.jeecg.com)

- 在线文档：  [http://doc.jeecg.com/1273753](http://doc.jeecg.com/1273753)

- 常见问题：  [入门常见问题大全](http://bbs.jeecg.com/forum.php?mod=viewthread&tid=7816&extra=page%3D1)

- QQ交流群 ：  ①284271917、②769925425


## 专项文档

#### 一、查询过滤器用法

```
QueryWrapper<?> queryWrapper = QueryGenerator.initQueryWrapper(?, req.getParameterMap());
```

代码示例：

```

	@GetMapping(value = "/list")
	public Result<IPage<JeecgDemo>> list(JeecgDemo jeecgDemo, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, 
	                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			HttpServletRequest req) {
		Result<IPage<JeecgDemo>> result = new Result<IPage<JeecgDemo>>();
		
		//调用QueryGenerator的initQueryWrapper
		QueryWrapper<JeecgDemo> queryWrapper = QueryGenerator.initQueryWrapper(jeecgDemo, req.getParameterMap());
		
		Page<JeecgDemo> page = new Page<JeecgDemo>(pageNo, pageSize);
		IPage<JeecgDemo> pageList = jeecgDemoService.page(page, queryWrapper);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

```



- 查询规则 (本规则不适用于高级查询,高级查询有自己对应的查询类型可以选择 )

| 查询模式         | 用法                                                                                                                                                                                                                                                | 说明 |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- |
| 模糊查询         | 支持左右模糊和全模糊  需要在查询输入框内前或后带\*或是前后全部带\*                                                                                                                                                                                  |      |
| 取非查询         | 在查询输入框前面输入! 则查询该字段不等于输入值的数据(数值类型不支持此种查询,可以将数值字段定义为字符串类型的)                                                                                                                                       |      |
| \>  \>= < <=     | 同取非查询 在输入框前面输入对应特殊字符即表示走对应规则查询                                                                                                                                                                                         |      |
| in查询           | 若传入的数据带,(逗号) 则表示该查询为in查询                                                                                                                                                                                                          |      |
| 多选字段模糊查询 | 上述4 有一个特例，若某一查询字段前后都带逗号 则会将其视为走这种查询方式 ,该查询方式是将查询条件以逗号分割再遍历数组 将每个元素作like查询 用or拼接,例如 现在name传入值 ,a,b,c, 那么结果sql就是 name like '%a%' or name like '%b%' or name like '%c%' |      |


#### 二、AutoPoi(EXCEL工具类-EasyPOI衍变升级重构版本）
 
  [在线文档](https://github.com/zhangdaiscott/autopoi)
  


#### 三、代码生成器

> 功能说明：   一键生成的代码（包括：controller、service、dao、mapper、entity、vue）
 
 - 模板位置： src/main/resources/jeecg/code-template
 - 技术文档： http://doc.jeecg.com/1273927



#### 四、编码排重使用示例

重复校验效果：
![输入图片说明](https://static.oschina.net/uploads/img/201904/19191836_eGkQ.png "在这里输入图片标题")

1.引入排重接口,代码如下:  
 
```
import { duplicateCheck } from '@/api/api'
  ```
2.找到编码必填校验规则的前端代码,代码如下:  
  
```
<a-input placeholder="请输入编码" v-decorator="['code', validatorRules.code ]"/>

code: {
            rules: [
              { required: true, message: '请输入编码!' },
              {validator: this.validateCode}
            ]
          },
  ```
3.找到rules里validator对应的方法在哪里,然后使用第一步中引入的排重校验接口.  
  以用户online表单编码为示例,其中四个必传的参数有:  
    
```
  {tableName:表名,fieldName:字段名,fieldVal:字段值,dataId:表的主键},
  ```
 具体使用代码如下:  
 
```
    validateCode(rule, value, callback){
        let pattern = /^[a-z|A-Z][a-z|A-Z|\d|_|-]{0,}$/;
        if(!pattern.test(value)){
          callback('编码必须以字母开头，可包含数字、下划线、横杠');
        } else {
          var params = {
            tableName: "onl_cgreport_head",
            fieldName: "code",
            fieldVal: value,
            dataId: this.model.id
          };
          duplicateCheck(params).then((res)=>{
            if(res.success){
             callback();
            }else{
              callback(res.message);
            }
          })
        }
      },
```


## docker镜像用法
 ``` 
注意： 如果本地安装了mysql和redis,启动容器前先停掉本地服务，不然会端口冲突。
       net stop redis
       net stop mysql
 
# 1.修改项目配置文件 application.yml
    active: docker

# 2.先进JAVA项目根路径 maven打包
    mvn clean package
 

# 3.构建镜像__容器组（当你改变本地代码，也可重新构建镜像）
    docker-compose build

# 4.配置host

    127.0.0.1 zhouwr.iot

# 5.启动镜像__容器组（也可取代运行中的镜像）
    docker-compose up -d

# 6.访问后台项目（注意要开启swagger）
    http://localhost:8080/jeecg-boot/doc.html
``` 

# 物联网平台系统

## 快速安装

### 1、下载代码

```html
https://gitee.com/eruditeLoong/forallcn-iotps.git
```

### 2、导入idea中

```text
1、打开idea
2、文件->打开
3、修改配置文件
```

### 3、修改配置文件

```html
1、打开文件jeecg-boot-system/src/main/resources/application-dev.yml
2、修改数据库配置
    地址：spring.datasource.dynamic.datasource.master.url
    用户名：spring.datasource.dynamic.datasource.master.username
    密码：spring.datasource.dynamic.datasource.master.password
    驱动：spring.datasource.dynamic.datasource.master.driver-class-name
3、修改redis配置
    地址：spring.redis.host
    端口：spring.redis.port
4、mqtt服务配置
    mqtt.username
    mqtt.password
    mqtt.url
5、文件储存目录配置
    jeecg.path.upload
    jeecg.path.webapp
```

### 4、maven导出jar

#### 使用maven命令将项目打包

#### 在代码根目录执行

```shell
mvn clean package -Dmaven.test.skip=true
```

### 5、前端UI导出html

```shell
npm install
vue-cli-service build
```

### 6、docker安装部署

```
如果是Mac或windows下安装的docker，java服务程序需使用jar直接启动的方式，其它服务插件部署到docker下；
Linux系统下安装的docker，所有程序都部署到docker下。

一、Mac或windows
1、打开文件docker-compose.yml
2、注释掉services.jeecg-boot部分代码
3、修改mysql的端口、root密码、volumes映射路径，
  如mac
  - /Users/用户目录/docker/mysql/conf:/etc/mysql
  - /Users/用户目录/docker/mysql/data:/var/lib/mysql
  windows
  - D:/docker/mysql/conf:/etc/mysql
  - D:/docker/mysql/data:/var/lib/mysql
4、打开命令行终端程序，调转到docker-compose.yml所在的目录，执行docker-compise up -d命令，等待执行完成
5、本机运行jar程序

二、Linux
1、打开文件docker-compose.yml
2、修改mysql的端口、root密码、volumes映射路径，如
  - /home/docker/mysql/conf:/etc/mysql
  - /home/docker/mysql/data:/var/lib/mysql
4、打开命令行终端程序，调转到docker-compose.yml所在的目录，执行docker-compise up -d命令，等待执行完成
```

### 7、nginx配置

#### 1、复制UI文件

mac用户 复制dist目录下到文件到 /Users/用户目录/docker/iotps/nginx/html windows用户 复制dist目录下到文件到 D:/docker/iotps/nginx/html Linux用户
复制dist目录下到文件到 /home/docker/iotps/nginx/html

#### 2、添加nginx配置文件

```shell
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include       mime.types;
  default_type  application/octet-stream;
  sendfile        on;
  keepalive_timeout  65;

  server {
    listen       80;
    server_name  zhouwr.iot;

    location / {
      root   /home/iotps/html; #docker虚拟机前端页面文件的路径
      try_files $uri $uri/ /index.html;
      index  index.html index.htm;
      if (!-e $request_filename) {
          rewrite ^(.*)$ /index.html?s=$1 last;
          break;
      }
    }
  
    location /prod-api/{
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://192.168.17.1:8080/jeecg-boot000/;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
  }
}

```
