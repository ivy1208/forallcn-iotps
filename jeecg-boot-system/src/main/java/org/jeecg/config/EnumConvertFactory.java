package org.jeecg.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.system.base.enums.IDictEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.stereotype.Component;

/**
 * @program: forallcn-iotps
 * @description: 枚举工厂转换类
 * @author: zhouwr
 * @create: 2021-02-26 17:00
 * @version：1.0
 **/
@Slf4j
@Component
public class EnumConvertFactory implements ConverterFactory<String, IDictEnum> {
    @Override
    public <T extends IDictEnum> Converter<String, T> getConverter(Class<T> targetType) {
        return new StringToIEum<>(targetType);
    }

    @SuppressWarnings("all")
    private static class StringToIEum<T extends IDictEnum> implements Converter<String, T> {
        private Class<T> targerType;

        public StringToIEum(Class<T> targerType) {
            this.targerType = targerType;
        }

        @Override
        public T convert(String source) {
            System.out.println(">>>>>> getIEnum convert <<<<<<<<" + source);
            if (StringUtils.isEmpty(source)) {
                return null;
            }
            return (T) EnumConvertFactory.getIEnum(this.targerType, source);
        }
    }

    public static <T extends IDictEnum> Object getIEnum(Class<T> targerType, String source) {
        System.out.println(">>>>>> getIEnum <<<<<<<<");
        for (T enumObj : targerType.getEnumConstants()) {
            if (source.equals(String.valueOf(enumObj.getValue()))) {
                return enumObj;
            }
        }
        return null;
    }
}
