package org.jeecg.config.mybatis;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.enums.IDictEnum;

import java.io.IOException;

/**
 * @program: forallcn-iotps
 * @description: Dict Enum Web序列化器
 * @author: zhouwr
 * @create: 2021-01-03 20:48
 * @version：1.0
 **/
@Slf4j
public class DictEnumWebSerializer  extends JsonSerializer<IDictEnum> {
    @Override
    public void serialize(IDictEnum value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        if(value==null){
            jgen.writeNull();

        }
        assert value != null;
        assert jgen.getOutputContext().getCurrentName() != null;
        log.debug("DictEnumWebSerializer >>> {} {} - {} - {}", value.getClass(), jgen.getOutputContext().getCurrentName(), value.getValue(), getEnumDesc(value));
        jgen.writeObject(value.getValue());
        jgen.writeFieldName(jgen.getOutputContext().getCurrentName()+"Text");
        jgen.writeString(getEnumDesc(value));
    }

    @Override
    public Class handledType() {
        return IDictEnum.class;
    }

    private  String getEnumDesc(IDictEnum dictEnum){
        return dictEnum.getText();
    }

}
