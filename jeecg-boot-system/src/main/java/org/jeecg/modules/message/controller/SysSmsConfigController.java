package org.jeecg.modules.message.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.message.entity.SysSmsConfig;
import org.jeecg.modules.message.service.ISysSmsConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 消息配置
 * @Author: jeecg-boot
 * @Date: 2021-04-22
 * @Version: V1.0
 */
@Api(tags = "消息配置")
@RestController
@RequestMapping("/message/sysSmsConfig")
@Slf4j
public class SysSmsConfigController extends JeecgController<SysSmsConfig, ISysSmsConfigService> {
	@Autowired
	private ISysSmsConfigService sysSmsConfigService;

	/**
	 * 分页列表查询
	 *
	 * @param sysSmsConfig
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "消息配置-分页列表查询")
	@ApiOperation(value = "消息配置-分页列表查询", notes = "消息配置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SysSmsConfig sysSmsConfig,
								   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
								   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SysSmsConfig> queryWrapper = QueryGenerator.initQueryWrapper(sysSmsConfig, req.getParameterMap());
		Page<SysSmsConfig> page = new Page<SysSmsConfig>(pageNo, pageSize);
		IPage<SysSmsConfig> pageList = sysSmsConfigService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 * 添加
	 *
	 * @param sysSmsConfig
	 * @return
	 */
	@AutoLog(value = "消息配置-添加")
	@ApiOperation(value = "消息配置-添加", notes = "消息配置-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SysSmsConfig sysSmsConfig) {
		sysSmsConfigService.save(sysSmsConfig);
		return Result.ok("添加成功！");
	}

	/**
	 * 编辑
	 *
	 * @param sysSmsConfig
	 * @return
	 */
	@AutoLog(value = "消息配置-编辑")
	@ApiOperation(value = "消息配置-编辑", notes = "消息配置-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SysSmsConfig sysSmsConfig) {
		sysSmsConfigService.updateById(sysSmsConfig);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "消息配置-通过id删除")
	@ApiOperation(value = "消息配置-通过id删除", notes = "消息配置-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		sysSmsConfigService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "消息配置-批量删除")
	@ApiOperation(value = "消息配置-批量删除", notes = "消息配置-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		this.sysSmsConfigService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "消息配置-通过id查询")
	@ApiOperation(value = "消息配置-通过id查询", notes = "消息配置-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
		SysSmsConfig sysSmsConfig = sysSmsConfigService.getById(id);
		if (sysSmsConfig == null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(sysSmsConfig);
	}

	/**
	 * 导出excel
	 *
	 * @param request
	 * @param sysSmsConfig
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, SysSmsConfig sysSmsConfig) {
		return super.exportXls(request, sysSmsConfig, SysSmsConfig.class, "消息配置");
	}

	/**
	 * 通过excel导入数据
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, SysSmsConfig.class);
	}

}
