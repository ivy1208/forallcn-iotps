package org.jeecg.modules.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.message.entity.SysSmsConfig;
import org.jeecg.modules.message.mapper.SysSmsConfigMapper;
import org.jeecg.modules.message.service.ISysSmsConfigService;
import org.springframework.stereotype.Service;

/**
 * @Description: 消息配置
 * @Author: jeecg-boot
 * @Date: 2021-04-22
 * @Version: V1.0
 */
@Service
public class SysSmsConfigServiceImpl extends ServiceImpl<SysSmsConfigMapper, SysSmsConfig> implements ISysSmsConfigService {

}
