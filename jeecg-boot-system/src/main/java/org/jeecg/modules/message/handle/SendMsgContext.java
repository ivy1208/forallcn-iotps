package org.jeecg.modules.message.handle;

import org.jeecg.modules.message.handle.enums.SendMsgTypeEnum;

/**
 * @program: forallcn-iotps
 * @description: 定义策略上下文，根据msgType获取对象实例
 * @author: zhouwr
 * @create: 2021-05-12 15:38
 * @version：1.0
 **/
public class SendMsgContext {
    public static ISendMsgHandle getInstance(String msgType) {
        ISendMsgHandle sendMsgHandle = null;
        final SendMsgTypeEnum msgTypeEnum = SendMsgTypeEnum.getByType(msgType);
        try {
            if (msgTypeEnum != null) {
                sendMsgHandle = (ISendMsgHandle) Class.forName(msgTypeEnum.getImplClass()).newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sendMsgHandle;
    }
}
