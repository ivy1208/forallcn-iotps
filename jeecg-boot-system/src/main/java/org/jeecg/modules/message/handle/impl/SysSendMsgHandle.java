package org.jeecg.modules.message.handle.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.WebsocketConst;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.message.handle.ISendMsgHandle;
import org.jeecg.modules.message.websocket.WebSocket;
import org.jeecg.modules.system.entity.SysAnnouncement;
import org.jeecg.modules.system.entity.SysAnnouncementSend;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.mapper.SysAnnouncementMapper;
import org.jeecg.modules.system.mapper.SysAnnouncementSendMapper;
import org.jeecg.modules.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @program: forallcn-iotps
 * @description: SysSendMsgHandle
 * @author: zhouwr
 * @create: 2021-03-15 13:50
 * @version：1.0
 **/
@Slf4j
public class SysSendMsgHandle implements ISendMsgHandle {

    @Autowired
    private WebSocket webSocket;
    @Autowired
    private SysAnnouncementMapper sysAnnouncementMapper;
    @Autowired
    private SysAnnouncementSendMapper sysAnnouncementSendMapper;
    @Autowired
    private SysUserMapper userMapper;

    public SysSendMsgHandle() {
        this.webSocket = (WebSocket) SpringContextUtils.getBean(WebSocket.class);
        this.sysAnnouncementMapper = (SysAnnouncementMapper) SpringContextUtils.getBean(SysAnnouncementMapper.class);
        this.sysAnnouncementSendMapper = (SysAnnouncementSendMapper) SpringContextUtils.getBean(SysAnnouncementSendMapper.class);
        this.userMapper = (SysUserMapper) SpringContextUtils.getBean(SysUserMapper.class);
    }

    @Override
    public void SendMsg(String es_receiver, String es_title, String es_content) {

        log.info("es_receiver >>> {}", es_receiver);
        if (StringUtils.isBlank(es_receiver)) {
            throw new JeecgBootException("接收者为空！");
        }
        SysAnnouncement announcement = new SysAnnouncement();
        announcement.setTitile(es_title);
        announcement.setMsgContent(es_content);
        announcement.setSender("SYSTEM");
        announcement.setPriority(CommonConstant.PRIORITY_M);
        announcement.setMsgType(CommonConstant.MSG_TYPE_UESR);
        announcement.setSendStatus(CommonConstant.HAS_SEND);
        announcement.setSendTime(new Date());
        announcement.setMsgCategory(CommonConstant.MSG_CATEGORY_2);
        announcement.setDelFlag(String.valueOf(CommonConstant.DEL_FLAG_0));
        sysAnnouncementMapper.insert(announcement);

        String[] userIds = es_receiver.split(",");
        String anntId = announcement.getId();
        for (int i = 0; i < userIds.length; i++) {
            if (oConvertUtils.isNotEmpty(userIds[i])) {
                SysUser sysUser = userMapper.getUserByName(userIds[i]);
                if (sysUser == null) {
                    continue;
                }
                SysAnnouncementSend announcementSend = new SysAnnouncementSend();
                announcementSend.setAnntId(anntId);
                announcementSend.setUserId(sysUser.getId());
                announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
                sysAnnouncementSendMapper.insert(announcementSend);
                JSONObject obj = new JSONObject();
                obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
                obj.put(WebsocketConst.MSG_USER_ID, sysUser.getId());
                obj.put(WebsocketConst.MSG_ID, announcement.getId());
                obj.put(WebsocketConst.MSG_TXT, announcement.getTitile());
                webSocket.sendOneMessage(sysUser.getId(), obj.toJSONString());
            }
        }
    }
}
