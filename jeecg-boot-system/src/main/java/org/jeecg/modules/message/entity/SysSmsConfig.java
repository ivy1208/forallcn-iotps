package org.jeecg.modules.message.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.system.vo.SysSmsConfigVo;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 消息配置
 * @Author: jeecg-boot
 * @Date: 2021-04-22
 * @Version: V1.0
 */
@Data
@TableName("sys_sms_config")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "sys_sms_config对象", description = "消息配置")
public class SysSmsConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 名称
     */
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
    /**
     * 消息类型
     */
    @Excel(name = "消息类型", width = 15)
    @ApiModelProperty(value = "消息类型")
    private java.lang.Integer smsType;
    /**
     * 配置信息
     */
    @Excel(name = "配置信息", width = 15)
    @ApiModelProperty(value = "配置信息")
    private java.lang.String config;
    /**
     * 网络服务
     */
    @Excel(name = "网络服务", width = 15)
    @ApiModelProperty(value = "网络服务")
    private java.lang.String networkService;
    /**
     * 发送人
     */
    @Excel(name = "发送人", width = 15)
    @ApiModelProperty(value = "发送人")
    private java.lang.String sender;
    /**
     * 接收人
     */
    @Excel(name = "接收人", width = 15)
    @ApiModelProperty(value = "接收人")
    private java.lang.String receiver;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**
     * 更新人
     */
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
    /**
     * 更新日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**
     * 所属部门
     */
    @Dict(dictTable = "sys_depart", dicText = "depart_name", dicCode = "id")
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    /**
     * 备注
     */
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;

    public SysSmsConfigVo vo() {
        return new SysSmsConfigVo(
                id, name, smsType, config, networkService, sender, receiver, createBy, createTime, updateBy, updateTime, sysOrgCode, remark
        );
    }
}
