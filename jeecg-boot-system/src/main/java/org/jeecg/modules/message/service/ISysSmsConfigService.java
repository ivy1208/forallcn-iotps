package org.jeecg.modules.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.message.entity.SysSmsConfig;

/**
 * @Description: 消息配置
 * @Author: jeecg-boot
 * @Date: 2021-04-22
 * @Version: V1.0
 */
public interface ISysSmsConfigService extends IService<SysSmsConfig> {

}
