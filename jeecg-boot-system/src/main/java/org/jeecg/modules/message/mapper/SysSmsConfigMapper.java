package org.jeecg.modules.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.message.entity.SysSmsConfig;

/**
 * @Description: 消息配置
 * @Author: jeecg-boot
 * @Date: 2021-04-22
 * @Version: V1.0
 */
public interface SysSmsConfigMapper extends BaseMapper<SysSmsConfig> {

}
